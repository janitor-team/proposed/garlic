/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				extract_int.c

Purpose:
	Extract integer from string. Start reading after the colon, if
	present.

Input:
	Input string pointer.

Output:
	Return value.

Return value:
	(1) Integer read from string, on success.
	(2) Zero if there are no digits.

========includes:============================================================*/

#include <stdio.h>
#include <string.h>
#include <ctype.h>

/*======extract integer from a string:=======================================*/

int ExtractInteger_ (char *sP)
{
char		*P0, *P1;
int		n;

/* Colon should be separator: */
if ((P0 = strstr (sP, ":")) == NULL) P0 = sP;

/* Replace each non-numeric character except minus sign with space: */
P1 = P0;
while ((n = *P1++) != '\0') if (!isdigit (n) && (n != '-')) *(P1 - 1) = ' ';

/* Try to read one integer: */
if (sscanf (P0, "%d", &n) != 1) return 0;

/* If everything worked fine, return the extracted integer: */
return n;
}

/*===========================================================================*/


