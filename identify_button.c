/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				identify_button.c

Purpose:
	Identify (dummy) button at which the ButtonPress event occured.
	If called for the first time,  initialize the array of  ButtonS
	structures,  i.e. initialize edge coordinates  for each button.

Input:
	(1) Pointer to XButtonEvent structure.
	(2) Pointer to GUIS structure.

Output:
	Return value.

Return value:
	(1) Zero if called for the first time.
	(2) Button index (positive or zero), on success.
	(3) Negative on failure (no button pressed). 

Notes:
	(1) Indices are assigned as follows:
	     0 = KP_0
	     1 = KP_1
	     2 = KP_2
	     3 = KP_3
	     4 = KP_4
	     5 = KP_5
	     6 = KP_6
	     7 = KP_7
	     8 = KP_8
	     9 = KP_9
	    10 = KP_Decimal
	    11 = KP_Enter
	    12 = KP_Add
	    13 = KP_Subtract
	    14 = KP_Multiply
	    15 = KP_Divide
	    16 = F1
	    17 = F2
	    18 = F3
	    19 = F4

========includes:============================================================*/

#include <stdio.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

int		InitializeButtons_ (ButtonS *);

/*======identify (dummy) button:=============================================*/

int IdentifyButton_ (XButtonEvent *button_eventSP, GUIS *guiSP)
{
static int		called_beforeF;
static ButtonS		buttonSA[20];
int			x, y;
int			buttonI;
int			i;

/* If this function is called for the first time, */
/* initialize  edge coordinates  for all buttons: */
if (called_beforeF == 0)
	{
	InitializeButtons_ (buttonSA);
	called_beforeF = 1;
	return 0;
	}

/* The exact position where the click occured: */
x = button_eventSP->x - guiSP->control_winS.x0 -
			guiSP->control_winS.border_width;
y = button_eventSP->y - guiSP->control_winS.y0 -
			guiSP->control_winS.border_width;

/* Identify the button: */
buttonI = -1;
for (i = 0; i < 20; i++)
	{
	/** Check is x between left and right edge: **/
	if (x < buttonSA[i].left_edge)   continue;
	if (x > buttonSA[i].right_edge)  continue;

	/** Check is y between top and bottom edge: **/
	if (y < buttonSA[i].top_edge)    continue;
	if (y > buttonSA[i].bottom_edge) continue;

	/** If this point is reached, set buttonI and break from the loop: **/
	buttonI = i;
	break;
	}

/* Return the button index (negative if nothing was hit): */
return buttonI;
}

/*===========================================================================*/


