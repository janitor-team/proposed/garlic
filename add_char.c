/* Copyright (C) 2000, 2001 Damir Zucic */

/*=============================================================================

				add_char.c

Purpose:
	Add one character  to the command string;  copy the string  to the
	history buffer.

Input:
	(1) Pointer to RuntimeS structure.
	(2) Pointer to GUIS structure.
	(2) The new character.

Output:
	(1) One character added to the input string.
	(2) Return value.

Return value:
	(1) Positive on success.
	(2) Negative on failure.

Notes:
	(1) Only insert mode is available: it is not possible to overwrite
	    characters.

	(2) Input overflow is not allowed.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======add one character to command string:=================================*/

int AddCharToCommand_ (RuntimeS *runtimeSP, GUIS *guiSP, int new_char)
{
int			comm_length, old_comm_length, carriage_pos;
char			stringA[COMMSTRINGSIZE];
char			*currP;

/* Copy the command string length and carriage (keyboard cursor) position: */
comm_length = runtimeSP->command_length;
old_comm_length = comm_length;
carriage_pos = runtimeSP->carriage_position;

/* Check the command string length: */
if (comm_length >= COMMSTRINGSIZE - 2) return -1;

/* Append the character  to the end  of command */
/* string, if carriage is at the end of string: */
if (carriage_pos == comm_length)
	{
	runtimeSP->curr_commandA[comm_length] = new_char;
	runtimeSP->curr_commandA[comm_length + 1] = '\0';
	}

/* Insert character, if carriage is not at the end of command string: */
else
	{
	strncpy (stringA, runtimeSP->curr_commandA, carriage_pos);
	stringA[carriage_pos] = new_char;
	stringA[carriage_pos + 1] = '\0';
	strncat (stringA,
		 runtimeSP->curr_commandA + carriage_pos,
		 comm_length - carriage_pos);
	stringA[comm_length + 1] = '\0';
	strcpy (runtimeSP->curr_commandA, stringA);
	}

/* Update the command length and carriage position: */
comm_length++;
runtimeSP->command_length = comm_length;
carriage_pos++;
runtimeSP->carriage_position = carriage_pos;

/* Copy the command string to the history buffer: */
currP = runtimeSP->commandsP + runtimeSP->next_commandI * COMMSTRINGSIZE;
strcpy (currP, runtimeSP->curr_commandA);

/* Find and store the command string width: */
runtimeSP->left_part_widthA[old_comm_length] =
			XTextWidth (guiSP->main_winS.fontSP,
				    runtimeSP->curr_commandA,
				    runtimeSP->carriage_position);

/* Return positive value (success): */
return 1;
}

/*===========================================================================*/


