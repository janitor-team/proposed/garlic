/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				docking_resize.c

Purpose:
	Handle ConfigureNotify event for docking window.

Input:
	(1) Pointer to RuntimeS structure.
	(2) Pointer to GUIS structure.
	(3) Pointer to XConfigureEvent structure.

Output:
	(1) Two geometric parameters updated.
	(2) Return value.

Return value:
	(1) Positive if geometric parameters are updated.
	(2) Zero if nothing should be done.

========includes:============================================================*/

#include <stdio.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======handle ConfigureNotify event for docking window:=====================*/

int DockingResize_ (RuntimeS *runtimeSP, GUIS *guiSP,
		    XConfigureEvent *configure_eventSP)
{

/* If this event was not generated for docking window, do nothing: */
if (configure_eventSP->window != guiSP->docking_winS.ID) return 0;

/* New width and height of docking window: */
guiSP->docking_winS.width = configure_eventSP->width;
guiSP->docking_winS.height = configure_eventSP->height;

/* Return positive value: */
return 1;
}

/*===========================================================================*/


