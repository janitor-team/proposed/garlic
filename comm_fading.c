/* Copyright (C) 2000-2002 Damir Zucic */

/*=============================================================================

				comm_fading.c

Purpose:
	Execute fading command. Change fading mode or change the position of
	front or back fading surface.

Input:
	(1) Pointer to MolComplexS structure,  with macromol. complexes.
	(2) The number of macromolecular complexes.
	(3) Pointer to RuntimeS structure, with some runtime data.
	(4) Pointer to ConfigS structure, with configuration data.
	(5) Pointer to GUIS structure, with GUI data.
	(6) Pointer to NearestAtomS structure.
	(7) The number of pixels in the main window free area.
	(8) Pointer to refreshI.
	(9) String  which contains  fading surface specification  and fading
	    shift.

Output:
	(1) The fading mode changed for each caught macromolecular complex.
	(2) Return value.

Return value:
	(1) Positive (command) code on success.
	(2) Negative (error) code on failure.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "commands.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

char		*ExtractToken_ (char *, int, char *, char *);
int		ChangeFading_ (MolComplexS *, int, int);
void		MoveBackFading_ (MolComplexS *, int, ConfigS *, double);
void		MoveFrontFading_ (MolComplexS *, int, ConfigS *, double);
size_t		MainRefresh_ (MolComplexS *, int,
			      RuntimeS *, ConfigS *, GUIS *,
			      NearestAtomS *, size_t, unsigned int);
int		ControlRefresh_ (MolComplexS *, ConfigS *, GUIS *);

/*======execute fading command:================================================*/

int CommandFading_ (MolComplexS *mol_complexSP, int mol_complexesN,
		  RuntimeS *runtimeSP, ConfigS *configSP, GUIS *guiSP,
		  NearestAtomS *nearest_atomSP, size_t pixelsN,
		  unsigned int *refreshIP, char *stringP)
{
char		*remainderP;
char		tokenA[STRINGSIZE];
int		surfaceID;
double		shift;

/* Extract the first token: */
remainderP = ExtractToken_ (tokenA, STRINGSIZE, stringP, " \t\n");
if (!remainderP)
	{
	strcpy (runtimeSP->messageA, "Fading specification incomplete!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return ERROR_FADING;
	}

/* Switch fading off: */
if (strstr (tokenA, "OFF") == tokenA)
	{
	ChangeFading_ (mol_complexSP, mol_complexesN, 0);
	(*refreshIP)++;
	MainRefresh_ (mol_complexSP, mol_complexesN,
		      runtimeSP, configSP, guiSP,
		      nearest_atomSP, pixelsN, *refreshIP);
	ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI,
			 configSP, guiSP);
	return COMMAND_FADING;
	}

/* Change fading to planar: */
else if (strstr (tokenA, "PLA") == tokenA)
	{
	ChangeFading_ (mol_complexSP, mol_complexesN, 1);
	(*refreshIP)++;
	MainRefresh_ (mol_complexSP, mol_complexesN,
		      runtimeSP, configSP, guiSP,
		      nearest_atomSP, pixelsN, *refreshIP);
	ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI,
			 configSP, guiSP);
	return COMMAND_FADING;
	}

/* Change fading to spherical: */
else if (strstr (tokenA, "SPH") == tokenA)
	{
	ChangeFading_ (mol_complexSP, mol_complexesN, 2);
	(*refreshIP)++;
	MainRefresh_ (mol_complexSP, mol_complexesN,
		      runtimeSP, configSP, guiSP,
		      nearest_atomSP, pixelsN, *refreshIP);
	ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI,
			 configSP, guiSP);
	return COMMAND_FADING;
	}

/* Change fading to semi-spherical: */
else if ((strstr (tokenA, "HALF-SPH") == tokenA) ||
	 (strstr (tokenA, "HALF_SPH") == tokenA))
	{
	ChangeFading_ (mol_complexSP, mol_complexesN, 3);
	(*refreshIP)++;
	MainRefresh_ (mol_complexSP, mol_complexesN,
		      runtimeSP, configSP, guiSP,
		      nearest_atomSP, pixelsN, *refreshIP);
	ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI,
			 configSP, guiSP);
	return COMMAND_FADING;
	}

/* Change fading to cylindrical: */
else if (strstr (tokenA, "CYL") == tokenA)
	{
	ChangeFading_ (mol_complexSP, mol_complexesN, 4);
	(*refreshIP)++;
	MainRefresh_ (mol_complexSP, mol_complexesN,
		      runtimeSP, configSP, guiSP,
		      nearest_atomSP, pixelsN, *refreshIP);
	ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI,
			 configSP, guiSP);
	return COMMAND_FADING;
	}

/* Change fading to semi-cylindrical: */
else if ((strstr (tokenA, "HALF-CYL") == tokenA) ||
	 (strstr (tokenA, "HALF_CYL") == tokenA))
	{
	ChangeFading_ (mol_complexSP, mol_complexesN, 5);
	(*refreshIP)++;
	MainRefresh_ (mol_complexSP, mol_complexesN,
		      runtimeSP, configSP, guiSP,
		      nearest_atomSP, pixelsN, *refreshIP);
	ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI,
			 configSP, guiSP);
	return COMMAND_FADING;
	}

/* Identify the surface: */
else if (strstr (tokenA, "FRO") == tokenA) surfaceID = 1;
else if (strstr (tokenA, "BAC")  == tokenA) surfaceID = 2;
else
	{
	strcpy (runtimeSP->messageA,
		"Bad surface (valid keywords are front and back)!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return ERROR_BAD_SURFACE;
	}

/* Extract the token which contains the shift: */
remainderP = ExtractToken_ (tokenA, STRINGSIZE, remainderP, " \t\n");
if (!remainderP)
	{
	strcpy (runtimeSP->messageA, "Fading shift missing!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return ERROR_NO_SHIFT;
	}

/* Extract the shift: */
if (sscanf (tokenA, "%lf", &shift) != 1)
	{
	strcpy (runtimeSP->messageA,
		"Fading specification is bad (unknown keyword used)!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return ERROR_NO_SHIFT;
	}

/* Move the specified fading surface: */
if (surfaceID == 1)
	{
	MoveFrontFading_ (mol_complexSP, mol_complexesN, configSP, shift);
	}
else
	{
	MoveBackFading_ (mol_complexSP, mol_complexesN, configSP, shift);
	}

/* Refresh the main window: */
(*refreshIP)++;
MainRefresh_ (mol_complexSP, mol_complexesN,
	      runtimeSP, configSP, guiSP,
	      nearest_atomSP, pixelsN, *refreshIP);

/* Refresh the control window: */
ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI, configSP, guiSP);

/* Return positive value on success: */
return COMMAND_FADING;
}

/*===========================================================================*/


