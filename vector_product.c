/* Copyright (C) 2000, 2001 Damir Zucic */

/*=============================================================================

				vector_product.c

Purpose:
	Calculate the vector product of two vectors (v3 = v1 x v2).

Input:
	(1) Pointer to VectorS structure,  where the result  will be stored.
	(2) Pointer to VectorS structure, with the first vector.
	(3) Pointer to VectorS structure, with the second vector.

Output:
	(1) The vector product calculated and stored into VectorS structure.

Return value:
	No return value.

========includes:============================================================*/

#include <stdio.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======calculate vector product of two vectors:=============================*/

void VectorProduct_ (VectorS *vector3SP,
		     VectorS *vector1SP, VectorS *vector2SP)
{

vector3SP->x = vector1SP->y * vector2SP->z - vector1SP->z * vector2SP->y;
vector3SP->y = vector1SP->z * vector2SP->x - vector1SP->x * vector2SP->z;
vector3SP->z = vector1SP->x * vector2SP->y - vector1SP->y * vector2SP->x;

}

/*===========================================================================*/


