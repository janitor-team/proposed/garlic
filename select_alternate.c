/* Copyright (C) 2000-2002 Damir Zucic */

/*=============================================================================

				select_alternate.c

Purpose:
	Select atoms at alternate locations.

Input:
	(1) Pointer to MolComplexS structure, with macromol. complexes.
	(2) Number of macromolecular complexes.
	(3) Selection mode index (0 = overwrite, 1 = restrict, 2 = expand
	    previous selection).

Output:
	(1) The flag  selectedF set for all atoms at alternate positions,
	    in all currently caught macromolecular complexes.
	(2) Return value.

Return value:
	(1) The number of selected atoms (zero or positive value).

Notes:
	(1) The position_changedF is updated,  because some atoms may not
	    have the proper color.

========includes:============================================================*/

#include <stdio.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======select atoms at alternate positions:=================================*/

long SelectAlternate_ (MolComplexS *mol_complexSP, int mol_complexesN,
		       int selection_modeI)
{
long		selected_atomsN = 0;
int		mol_complexI;
MolComplexS	*curr_mol_complexSP;
size_t		atomsN, atomI;
AtomS		*curr_atomSP;
char		alt_location;
int		alternateF;

/* Check every macromolecular complex: */
for (mol_complexI = 0; mol_complexI < mol_complexesN; mol_complexI++)
	{
	/** Pointer to the current macromolecular complex: **/
	curr_mol_complexSP = mol_complexSP + mol_complexI;

	/** Check is the current macromolecular complex caught: **/
	if (curr_mol_complexSP->catchF == 0) continue;

	/** Number of atoms in a macromolecular complex: **/
	atomsN = curr_mol_complexSP->atomsN;

	/** Scan all atoms in the current complex: **/
	for (atomI = 0; atomI < atomsN; atomI++)
		{
		/** Pointer to the current atom: **/
		curr_atomSP = curr_mol_complexSP->atomSP + atomI;

		/** Check the alternate position indicator: **/
		alt_location = curr_atomSP->raw_atomS.alt_location;
		alternateF = 0;
		if ((alt_location != ' ') && (alt_location != 'A'))
			{
			alternateF = 1;
			}

		/** Set the selection flag for the current atom: **/
		switch (selection_modeI)
			{
			/*** Overwrite the previous selection: ***/
			case 0:
				curr_atomSP->selectedF = alternateF;
				break;

			/*** Restrict the previous selection: ***/
			case 1:
				curr_atomSP->selectedF &= alternateF;
				break;

			/*** Expand the previous selection: ***/
			case 2:
				curr_atomSP->selectedF |= alternateF;
				break;

			default:
				;
			}

		/** Check the selection flag; increase **/
		/** the count if flag is equal to one: **/
		if (curr_atomSP->selectedF) selected_atomsN++;
		}

	/** Update the position_changedF (some atoms may have bad color): **/
	curr_mol_complexSP->position_changedF = 1;
	}

/* Return the number of selected atoms: */
return selected_atomsN;
}

/*===========================================================================*/


