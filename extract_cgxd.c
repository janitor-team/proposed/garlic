/* Copyright (C) 2001, 2002 Damir Zucic */

/*=============================================================================

				extract_cgxd.c

Purpose:
	Extract CG and XD coordinates for a given residue. The symbol XD
        stands for  CD in ARG, GLN, GLU and LYS and for SD in MET.  This
	function is not able  to handle non-standard residues.

Input:
	(1) Pointer to VectorS structure, for CG coordinates.
	(2) Pointer to VectorS structure, for XD coordinates.
	(3) Pointer to AtomS structure, pointing to the first element of
	    the atomic array.
	(4) Index of the first atom of a given residue.
	(5) Index of the last atom of a given residue.

Output:
	(1) VectorS structures filled with data.
	(2) Return value.

Return value:
	(1) The number of successfully extracted vectors (at least zero,
	    at most two).

Notes:
	(1) Some files  contain  more  than  one entry  for  some atoms.
	    Garlic uses only the first entry and  discards other entries
	    for the same atom.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======extract CG and XD coordinates:=======================================*/

int ExtractCGXD_ (VectorS  *CG_vectorSP, VectorS *XD_vectorSP,
		  AtomS *atomSP, size_t atom_startI, size_t atom_endI)
{
int		vectors_extractedN = 0;
size_t		atomI;
AtomS		*curr_atomSP;
char		*atom_nameP;
int		CG_foundF = 0, XD_foundF = 0;

/* Scan the given residue: */
for (atomI = atom_startI; atomI <= atom_endI; atomI++)
	{
	/* Pointer to the current atom: */
	curr_atomSP = atomSP + atomI;

	/* Pointer to the purified atom name: */
	atom_nameP = curr_atomSP->raw_atomS.pure_atom_nameA;

	/* CG: */
	if (strcmp (atom_nameP, "CG") == 0)
		{
		if (CG_foundF) continue;
		CG_vectorSP->x = curr_atomSP->raw_atomS.x[0];
		CG_vectorSP->y = curr_atomSP->raw_atomS.y;
		CG_vectorSP->z = curr_atomSP->raw_atomS.z[0];
		vectors_extractedN++;
		CG_foundF = 1;
		}

	/* XD: */
	else if ((strcmp (atom_nameP, "CD") == 0) ||
		 (strcmp (atom_nameP, "SD") == 0))
		{
		if (XD_foundF) continue;
		XD_vectorSP->x = curr_atomSP->raw_atomS.x[0];
		XD_vectorSP->y = curr_atomSP->raw_atomS.y;
		XD_vectorSP->z = curr_atomSP->raw_atomS.z[0];
		vectors_extractedN++;
		XD_foundF = 1;
		}
	}

/* Return the number of extracted vectors: */
return vectors_extractedN;
}

/*===========================================================================*/


