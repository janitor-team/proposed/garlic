/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				reset_pos_flag.c

Purpose:
	Reset position_changedF in each macromolecular complex.  This flag
	is used to increase perfomance.
	
Input:
	(1) Pointer to MolComplexS structure, with macromol. complexes.
	(2) Number of macromolecular complexes.

Output:
	(1) In each macromolecular complex, position_changedF set to zero.

Return value:
	No return value.

========includes:============================================================*/

#include <stdio.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======reset position_changedF flag in each complex:========================*/

void ResetPositionFlag_ (MolComplexS *mol_complexSP, int mol_complexesN)
{
int		mol_complexI;

for (mol_complexI = 0; mol_complexI < mol_complexesN; mol_complexI++)
	(mol_complexSP + mol_complexI)->position_changedF = 0;
}

/*===========================================================================*/


