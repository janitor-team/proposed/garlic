/* Copyright (C) 2000-2006 Damir Zucic */

/*=============================================================================

				hydrophobicity.c

Purpose:
	This function  may be used to assign  the hydrophobicity to the
	specified residue or to retrieve minimal,  maximal,  average or
	threshold hydrophobicity value.
	
Input:
	(1) Pointer to residue name.
	(2) Hydrophobicity scale index.
	(3) Request index:  0 = return hydrophobicity,  1 = return min.
	    value, 2 = return max. value,  3 = return average value and
	    4 = return threshold value.

Output:
	Return value.

Return value:
	(1) Hydrophobicity of the specified residue, extreme hydrophob.
	    value,  average hydrophobicity or threshold hydrophobicity.
	    What will be returned is decided by the request index.

Notes:
	(1) The average hydrophobicity is assigned to unknown residues.

	(2) Threshold hydrophobicy  is used  to distinguish hydrophobic
	    and hydrophilic residues.  Residue which has hydrophobicity
	    value above the threshold should be treated as hydrophobic.
	    For Kyte-Doolittle scale and White scales,  positive values
	    are assigned to hydrophobic residues and threshold value is
	    equal to zero. For normalized Eisenberg scale, threshold is
	    different from zero. This is caused by normalization.

	(3) References:

	    ===========================================================
	    | Scale(s)        |  Reference                            |
	    ===========================================================
	    | S. White group  |  Biochemistry   35 (1996), p. 5109.   |
	    | Kyte-Doolitle   |  J. Mol. Biol. 157 (1982), p.  105.   |
	    | Eisenberg       |  J. Mol. Biol. 179 (1984), p.  125.   |
	    ===========================================================

	(4) Check init_runtime.c for default hydrophobicity scale!

	(5) Twenty standard residues are used  to calculate the average
	    hydrophobicity value: ASX, GLX and UNK are not used.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include "defines.h"

/*======return hydrophobicity or requested reference value:==================*/

double Hydrophobicity_ (char *residue_nameP, int scaleI, int requestI)
{
double		hydrophobicity;
static char	amino_acidAA[23][4] =
		 {"ALA","ARG","ASN","ASP","CYS","GLN","GLU","GLY","HIS","ILE",
		  "LEU","LYS","MET","PHE","PRO","SER","THR","TRP","TYR","VAL",
		  "ASX","GLX","UNK"};

static double	hyphob_scaleAA[SCALESN][23] =

		/* 0: White & Wimley octanol scale, OCT (was WHI before): */
		{{-0.50,-1.81,-0.85,-3.64, 0.02,-0.77,-3.63,-1.15,-0.11, 1.12,
		   1.25,-2.80, 0.67, 1.71,-0.14,-0.46,-0.25, 2.09, 0.71, 0.46,
		  -2.25,-2.20,-0.40}, /* Note: HIS is neutral! */

		/* 1: Kyte & Doolittle hydropathy scale, KD: */
		 { 1.80,-4.50,-3.50,-3.50, 2.50,-3.50,-3.50,-0.40,-3.20, 4.50,
		   3.80,-3.90, 1.90, 2.80,-1.60,-0.80,-0.70,-0.90,-1.30, 4.20,
		  -3.50,-3.50,-0.49},

		/* 2: Eisenberg normalized consensus scale, EIS: */
		 { 0.62,-2.53,-0.78,-0.90, 0.29,-0.85,-0.74, 0.48,-0.40, 1.38,
		   1.06,-1.50, 0.64, 1.19, 0.12,-0.18,-0.05, 0.81, 0.26, 1.08,
		  -0.84,-0.80, 0.00},

		/* 3: White & Wimley interface scale, HIS neutral, INT: */
		 {-0.17,-0.81,-0.42,-1.23, 0.24,-0.58,-2.02,-0.01,-0.17, 0.31,
		   0.56,-0.99, 0.23, 1.13,-0.45,-0.13,-0.14, 1.85, 0.94,-0.07,
		  -0.83,-1.30,-0.10}, /* Note: HIS is neutral! */

		/* 4: Interface scale, HIS, ASP and GLU neutral, IN2: */
		 {-0.17,-0.81,-0.42,-0.07,-0.24,-0.58, 0.01,-0.01,-0.17, 0.31,
		   0.56,-0.99, 0.23, 1.13,-0.45,-0.13,-0.14, 1.85, 0.94,-0.07,
		  -0.25,-0.30, 0.04}, /* Note: HIS, ASP and GLU are neutral! */

		/* 5: Differential scale, OCT-INT, HIS neutral, DIF: */
		 {-0.33,-1.00,-0.43,-2.41,-0.22,-0.19,-1.61,-1.14, 0.06, 0.81,
		   0.69,-1.81, 0.44, 0.58, 0.31,-0.33,-0.11, 0.24,-0.23, 0.53,
		  -1.38,-0.90,-0.31}};

		/*-----------------------------------*/
		/* OCT   KD    EIS   INT   IN2   DIF */
		/*-----------------------------------*/
		/*  0     1     2     3     4     5  */
		/*-----------------------------------*/
static double	min_hyphobA[SCALESN]       =
		 {-3.64,-4.50,-2.53,-2.02,-0.99,-2.41};
static double	max_hyphobA[SCALESN]       =
		 { 2.09, 4.50, 1.38, 1.85, 1.85, 0.81};
static double	average_hyphobA[SCALESN]   =
		 {-0.40,-0.49, 0.00,-0.10, 0.04,-0.31};
static double	threshold_hyphobA[SCALESN] =
		 { 0.00, 0.00,-0.16, 0.00, 0.00, 0.00};
int		i;

/* Check scale index: */
if (scaleI >= SCALESN) return 0.0;

/* If minimal hydrophobicity is requested: */
if (requestI == 1) return min_hyphobA[scaleI];

/* If maximal hydrophobicity is requiested: */
else if (requestI == 2) return max_hyphobA[scaleI];

/* If average hydrophobicity is requested: */
else if (requestI == 3) return average_hyphobA[scaleI];

/* If threshold hydrophobicity is requested: */
else if (requestI == 4) return threshold_hyphobA[scaleI];

/* If this point is reached, hydrophobicity */
/* of  the specified residue  is requested. */

/* Use average value as default; it will */
/* be returned  if identification fails: */
hydrophobicity = average_hyphobA[scaleI];

/* Compare the purified residue name with 23 standard names: */
for (i = 0; i < 23; i++)
	{
	/* If amino acid is recognized: */
	if (strcmp (residue_nameP, amino_acidAA[i]) == 0)
		{
		hydrophobicity = hyphob_scaleAA[scaleI][i];
		break;
		}
	}

/* Return hydrophobicity value: */
return hydrophobicity;
}

/*===========================================================================*/


