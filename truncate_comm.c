/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				truncate_comm.c

Purpose:
	Truncate the command string,  string length,  carriage position and
	array of text widths.

Input:
	(1) Pointer to RuntimeS structure.

Output:
	(1) Command string truncated, command string length reset, carriage
	    position reset and array of text widths reset.
	(2) Return value.

Return value:
	(1) Positive always (trivial).

========includes:============================================================*/

#include <stdio.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======truncate command string:=============================================*/

int TruncateCommand_ (RuntimeS *runtimeSP)
{
int			i;

/* Truncate the command string: */
runtimeSP->curr_commandA[0] = '\0';

/* Reset the command string length and carriage position: */
runtimeSP->command_length = 0;
runtimeSP->carriage_position = 0;

/* Reset the array of text widths: */
for (i = 0; i < COMMSTRINGSIZE; i++) runtimeSP->left_part_widthA[i] = 0;

/* Return positive value (success): */
return 1;
}

/*===========================================================================*/


