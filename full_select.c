/* Copyright (C) 2000-2003 Damir Zucic */

/*=============================================================================

				full_select.c

Purpose:
	Execute select command:  select the specified atoms  in all caught
	macromolecular complexes. In this function, the selection cryteria
	should be precise (three slashes expected). Three selections modes
	are available:  0 = overwrite,  1 = restrict,  2 = expand previous
	selection.  If this  function  failes  to interpret  the selection
	string, it will leave the job to other functions.

Input:
	(1) Pointer to MolComplexS structure, with macromol. complexes.
	(2) Number of macromolecular complexes.
	(3) The selection string.
	(4) Selection mode index  (0 = overwr., 1 = restrict, 2 = expand).

Output:
	(1) The flag selectedF will be equal to one for selected atoms and
	    equal to zero for all other atoms.
	(2) Return value.

Return value:
	(1) The number of  selected atoms,  if selection  is done  in this
	    function.
	(2) -1 if selection string is not suitable for this function.
	(3) Some other negative value on failure.

Notes:
	(1) This function will  not  handle  the selection string  if this
	    string does not contain three slashes.

========includes:============================================================*/

#include <stdio.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

int		Include_ (SelectS *, char *);
int		Exclude_ (SelectS *, char *);
long		ApplySelection_ (MolComplexS *, int,
				 SelectS *, SelectS *, int);

/*======execute select command (full selection string):======================*/

long FullSelect_ (MolComplexS *mol_complexSP, int mol_complexesN,
		  char *stringP, int selection_modeI)
{
long		selected_atomsN;
char		*P;
int		n;
int		slashesN = 0;
SelectS		include_selectS, exclude_selectS;

/* Count slashes: */
P = stringP;
while ((n = *P++) != '\0') if (n == '/') slashesN++;
if (slashesN != 3) return (long) -1;

/* Identify chains,  residue ranges,  residue */
/* names and atoms which have to be included: */
if (Include_ (&include_selectS, stringP) < 0) return (long) -2;

/* Identify chains,  residue ranges,  residue */
/* names and atoms which have to be excluded: */
if (Exclude_ (&exclude_selectS, stringP) < 0) return (long) -3;

/* Do the selection: */
selected_atomsN = ApplySelection_ (mol_complexSP, mol_complexesN,
				   &include_selectS, &exclude_selectS,
				   selection_modeI);

/* Return the number of selected atoms: */
return selected_atomsN;
}

/*===========================================================================*/


