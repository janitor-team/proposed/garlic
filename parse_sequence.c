/* Copyright (C) 2000-2003 Damir Zucic */

/*=============================================================================

				parse_sequence.c

Purpose:
	Parse the sequence string.

Input:
	(1) Pointer to RuntimeS structure.
	(2) Pointer to the string which contains the sequence.

Output:
	(1) Sequence stored to the sequence buffer.
	(2) Return value.

Return value:
	(1) Positive on success.
	(2) Negative on failure.

Notes:
	(1) The sequence is expected in three letters code. One letter
	    code may be missinterpreted as valid three letters code.

	(2) Space, comma, tab and semicolon may be used as separators.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "commands.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

char		*ExtractToken_ (char *, int, char *, char *);
void		InitHyphob_ (RuntimeS *);

/*======parse sequence string:===============================================*/

int ParseSequence_ (RuntimeS *runtimeSP, char *stringP)
{
int		max_length, i;
char		*remainderP;
char		tokenA[SHORTSTRINGSIZE];
char		*P;
size_t		residueI = 0;

/* The maximal residue name length: */
max_length = RESNAMESIZE - 1;

/* Reset the number of residues in sequence: */
runtimeSP->residuesN = 0;

/* Zero initialize the sequence buffer: */
for (i = 0; i < (int) runtimeSP->sequence_buffer_size; i++)
	{
	*(runtimeSP->sequenceP + i) = '\0';
	}

/* Parse the string: */
remainderP = stringP;
while ((remainderP = ExtractToken_ (tokenA, SHORTSTRINGSIZE,
				    remainderP, " ,;\t\n")) != NULL)
	{
	/* Check the residue name size: */
	if ((int) strlen (tokenA) > max_length)
		{
		sprintf (runtimeSP->messageA,
			 "Residue name %s too long!",
			 tokenA);
		runtimeSP->message_length = strlen (runtimeSP->messageA);
		return -1;
		}

	/* Copy the residue name to the sequence buffer: */
	P = runtimeSP->sequenceP + max_length * residueI;
	strcpy (P, tokenA);

	/* Update the residue index: */
	residueI++;
	}

/* Store the number of residues: */
runtimeSP->residuesN = residueI;

/* Reinitialize serial numbers: */
for (residueI = 0; residueI < runtimeSP->residuesN; residueI++)
	{
	*(runtimeSP->serialIP + residueI) = residueI + 1;
	}

/* Reinitialize disulfide flags: */
for (residueI = 0; residueI < runtimeSP->residuesN; residueI++)
	{
	*(runtimeSP->disulfideFP + residueI) = 0;
	}

/* Initialize hydrophobicity values: */
InitHyphob_ (runtimeSP);

/* Return positive value on success: */
return 1;
}

/*===========================================================================*/


