/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				ramachandran.c

Purpose:
	Execute ramachandran command: draw Ramachandran plot.

Input:
	(1) Pointer to MolComplexS structure.
	(2) The number of macromolecular complexes.
	(3) Pointer to RuntimeS structure.
	(4) Pointer to ConfigS structure.
	(5) Pointer to GUIS structure.
	(6) Pointer to NearestAtomS structure.
	(7) The number of pixels in the main window free area.
	(8) Pointer to refreshI.
	(9) Pointer to  the remainder of the command string.  This command
	    may be given without keyword, with keyword SEL or with keyword
	    OFF.

Output:
	(1) The main window mode changed to 1 (default is zero),  i.e. the
	    main window will be switched to Ramachandran plot.
	(2) Return value.

Return value:
	(1) Positive (command) code on success.
	(2) Negative (error) code on failure.

Notes:
	(1) This command reinitializes the  NearestAtomS array,  except if
	    the additional keyword is not recognized.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "commands.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

char		*ExtractToken_ (char *, int, char *, char *);
void		InitNearest_ (NearestAtomS *, size_t);
size_t		MainRefresh_ (MolComplexS *, int,
			      RuntimeS *, ConfigS *, GUIS *,
			      NearestAtomS *, size_t, unsigned int);
int		ControlRefresh_ (MolComplexS *, ConfigS *, GUIS *);

/*======execute ramachandran command:========================================*/

int Ramachandran_ (MolComplexS *mol_complexSP, int mol_complexesN,
		   RuntimeS *runtimeSP, ConfigS *configSP, GUIS *guiSP,
		   NearestAtomS *nearest_atomSP, size_t pixelsN,
		   unsigned int *refreshIP, char *stringP)
{
char		*remainderP;
char		tokenA[SHORTSTRINGSIZE];

/* Initialize / reset the rama_selectionF (described in typedefs.h): */
runtimeSP->rama_selectionF = 0;

/* Extract the first token: */
remainderP = ExtractToken_ (tokenA, STRINGSIZE, stringP, " \t\n");

/* If available, the first token should contain keyword OFF or SEL: */
if (remainderP)
	{
	/* If keyword OFF is present, switch to default drawing mode: */
	if (strstr (tokenA, "OFF") == tokenA)
		{
		/* Reset drawing mode index: */
		guiSP->main_window_modeI = 0;

		/* Reinitialize the NearestAtomS array: */
		InitNearest_ (nearest_atomSP, pixelsN);
		*refreshIP = 1;

		/* Refresh the main window: */
		(*refreshIP)++;
		MainRefresh_ (mol_complexSP, mol_complexesN,
			      runtimeSP, configSP, guiSP,
			      nearest_atomSP, pixelsN, *refreshIP);

		/* Refresh the control window: */
		ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI,
				 configSP, guiSP);

		/* Return the command code: */
		return COMMAND_RAMACHANDRAN;
		}

	/* If keyword  SEL  is present,  set the flag which signals that */
	/* Ramachandran plot should be drawn only for selected residues: */
	else if (strstr (tokenA, "SEL") == tokenA)
		{
		runtimeSP->rama_selectionF = 1;
		}

	/* If keyword recognition fails: */
	else
		{
		sprintf (runtimeSP->messageA,
			 "Keyword %s not recognized!", tokenA);
		runtimeSP->message_length = strlen (runtimeSP->messageA);
		return ERROR_RAMACHANDRAN;
		}
	}

/* Set the main window drawing mode index: */
guiSP->main_window_modeI = 1;

/* Reinitialize the NearestAtomS array and refresh index: */
InitNearest_ (nearest_atomSP, pixelsN);
*refreshIP = 1;

/* Refresh the main window: */
(*refreshIP)++;
MainRefresh_ (mol_complexSP, mol_complexesN,
	      runtimeSP, configSP, guiSP,
	      nearest_atomSP, pixelsN, *refreshIP);

/* Refresh the control window: */
ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI, configSP, guiSP);

/* Return the command code: */
return COMMAND_RAMACHANDRAN;
}

/*===========================================================================*/


