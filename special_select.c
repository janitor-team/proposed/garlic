/* Copyright (C) 2000-2006 Damir Zucic */

/*=============================================================================

				special_select.c

Purpose:
	Execute select command (the special version): try to interpret the
	selection string  as the special keyword.  Three selections  modes
	are available:  0 = overwrite,  1 = restrict,  2 = expand previous
	selection.  Special keywords  are:  ALL,  *,  POLAR,  HYDROPHOBIC,
	AROMATIC,  ALIPHATIC,  SMALL,  TINY,  POSITIVE,  NEGATIVE,  BASIC,
	ACIDIC, CHARGED, MAIN_CHAIN, SIDE_CHAINS, HETERO, CIS, TRANS, BAD,
	COMPLEMENT,  SEQUENCE,  SPHERE, ALTERNATE,  MODEL,  ABOVE,  BELOW,
	TRIPLET, TM, PATTERN, 2C3, 2CZ3, 2NZ3, 3C4, 3CZ4, 3NZ4, 3C5, 3CZ5,
	3NZ5, 3P5, 4C5, 4CZ5, 4NZ5, 3C6, 3CZ6, 3NZ6, 4C6, 4CZ6, 4NZ6, 4P6,
	5C6, 5CZ6, 5NZ6,  4C7, 4CZ7, 4NZ7, 5C7, 5CZ7, 5NZ7, 5P7, 6P7, 4C9,
	4CZ9, 4NZ9, 5C9, 5CZ9, 5NZ9 and  $1.  Most keywords may be abbrev.
	to 3 characters.  The exceptions are keywords HYDROPHOBIC  (it may
	be replaced by  PHO or shortened to  HYDROPHO),  2CZ3, 2NZ3, 3CZ4,
	3NZ4, 3CZ5, 3NZ5, 4CZ5, 4NZ5,  3CZ6, 3NZ6, 4CZ6, 4NZ6, 5CZ6, 5NZ6,
	4CZ7, 4NZ7, 5CZ7, 5NZ7, 4CZ9, 5CZ9 and  5NZ9.  The keyword $1 is a
	special selection keyword (not implemented yet, @@).

Input:
	(1) Pointer to MolComplexS structure, with macromol. complexes.
	(2) Number of macromolecular complexes.
	(3) Pointer to RuntimeS structure  (the sequence buffer is there).
	(4) The selection string.
	(5) Selection mode index  (0 = overwr., 1 = restrict, 2 = expand).

Output:
	(1) The flag selectedF will be equal to one for selected atoms and
	    equal to zero for all other atoms.
	(2) Return value.

Return value:
	(1) The number of  selected atoms,  if selection  is done  in this
	    function.
	(2) -1 if selection string is not suitable for this function.
	(3) Some other negative value on failure.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

char		*ExtractToken_ (char *, int, char *, char *);
int		ExtractIndex_ (char *);
long		SelectHetero_ (MolComplexS *, int, int);
long		SelectCisTrans_ (MolComplexS *, int, int, int);
long		SelectComplement_ (MolComplexS *, int, int);
long		SelectSequence_ (MolComplexS *, int, RuntimeS *, int);
long		SelectSphere_ (MolComplexS *, int, RuntimeS *, int);
long		SelectAlternate_ (MolComplexS *, int, int);
long		SelectModel_ (MolComplexS *, int, int, int);
long		SelectAbove_ (MolComplexS *, int, int);
long		SelectBelow_ (MolComplexS *, int, int);
long		SelectTriplet_ (MolComplexS *, int, int);
long		SelectTM_ (MolComplexS *, int, int);
long		SelectPattern_ (MolComplexS *, int, RuntimeS *, int);
long		Select2C3_ (MolComplexS *, int, int);
long		Select2CZ3_ (MolComplexS *, int, int);
long		Select2NZ3_ (MolComplexS *, int, int);
long		Select3C4_ (MolComplexS *, int, int);
long		Select3CZ4_ (MolComplexS *, int, int);
long		Select3NZ4_ (MolComplexS *, int, int);
long		Select3C5_ (MolComplexS *, int, int);
long		Select3CZ5_ (MolComplexS *, int, int);
long		Select3NZ5_ (MolComplexS *, int, int);
long		Select3P5_ (MolComplexS *, int, int);
long		Select4C5_ (MolComplexS *, int, int);
long		Select4CZ5_ (MolComplexS *, int, int);
long		Select4NZ5_ (MolComplexS *, int, int);
long		Select3C6_ (MolComplexS *, int, int);
long		Select3CZ6_ (MolComplexS *, int, int);
long		Select3NZ6_ (MolComplexS *, int, int);
long		Select4C6_ (MolComplexS *, int, int);
long		Select4CZ6_ (MolComplexS *, int, int);
long		Select4NZ6_ (MolComplexS *, int, int);
long		Select4P6_ (MolComplexS *, int, int);
long		Select5C6_ (MolComplexS *, int, int);
long		Select5CZ6_ (MolComplexS *, int, int);
long		Select5NZ6_ (MolComplexS *, int, int);
long		Select4C7_ (MolComplexS *, int, int);
long		Select4CZ7_ (MolComplexS *, int, int);
long		Select4NZ7_ (MolComplexS *, int, int);
long		Select5C7_ (MolComplexS *, int, int);
long		Select5CZ7_ (MolComplexS *, int, int);
long		Select5NZ7_ (MolComplexS *, int, int);
long		Select5P7_ (MolComplexS *, int, int);
long		Select6P7_ (MolComplexS *, int, int);
long		Select4C9_ (MolComplexS *, int, int);
long		Select4CZ9_ (MolComplexS *, int, int);
long		Select4NZ9_ (MolComplexS *, int, int);
long		Select5C9_ (MolComplexS *, int, int);
long		Select5CZ9_ (MolComplexS *, int, int);
long		Select5NZ9_ (MolComplexS *, int, int);
int		Include_ (SelectS *, char *);
int		Exclude_ (SelectS *, char *);
long		ApplySelection_ (MolComplexS *, int,
				 SelectS *, SelectS *, int);

/*======execute select command (special selection string):===================*/

long SpecialSelect_ (MolComplexS *mol_complexSP, int mol_complexesN,
		     RuntimeS *runtimeSP, char *stringP, int selection_modeI)
{
long		selected_atomsN;
char		tokenA[STRINGSIZE];
char		full_stringA[STRINGSIZE];
char		*P;
int		n;
int		slashesN = 0;
SelectS		include_selectS, exclude_selectS;
int		model_serialI;

/* Purify the selection string (remove the leading and trailing separators): */
if (!ExtractToken_ (tokenA, STRINGSIZE, stringP, " ,;\t\n")) return (long) -1;

/* Check the selection string: */

/** ALL: **/
if ((strstr (tokenA, "ALL") == tokenA) || (strstr (tokenA, "*") == tokenA))
	strcpy (full_stringA, "*/*/*/*");

/** POLAR: **/
else if (strstr (tokenA, "POL") == tokenA)
	strcpy (full_stringA,
		"*/*/ARG,LYS,HIS,ASP,GLU,ASN,GLN,SER,THR,TRP,TYR/*");

/** HYDROPHOBIC: **/
else if ((strstr (tokenA, "HYDROPHO") == tokenA) ||
	 (strstr (tokenA, "PHO") == tokenA))
	strcpy (full_stringA,
		"*/*/ALA,ILE,LEU,MET,PHE,TRP,VAL,TYR,CYS,GLY,HIS/*");

/** AROMATIC: **/
else if (strstr (tokenA, "ARO") == tokenA)
	strcpy (full_stringA, "*/*/PHE,TYR,TRP,HIS/*");

/** ALIPHATIC: **/
else if (strstr (tokenA, "ALI") == tokenA)
	strcpy (full_stringA, "*/*/ILE,LEU,VAL/*");

/** SMALL: **/
else if (strstr (tokenA, "SMA") == tokenA)
	strcpy (full_stringA, "*/*/GLY,ALA,SER,THR,CYS,VAL,PRO,ASP,ASN/*");

/** TINY: **/
else if (strstr (tokenA, "TIN") == tokenA)
	strcpy (full_stringA, "*/*/GLY,ALA,SER/*");

/** POSITIVE: **/
else if (strstr (tokenA, "POS") == tokenA)
	strcpy (full_stringA, "*/*/ARG,LYS,HIS/*");

/** NEGATIVE: **/
else if (strstr (tokenA, "NEG") == tokenA)
	strcpy (full_stringA, "*/*/GLU,ASP/*");

/** BASIC: **/
else if (strstr (tokenA, "BAS") == tokenA)
	strcpy (full_stringA, "*/*/ARG,LYS,HIS/*");

/** ACIDIC: **/
else if (strstr (tokenA, "ACI") == tokenA)
	strcpy (full_stringA, "*/*/GLU,ASP/*");

/** CHARGED: **/
else if (strstr (tokenA, "CHA") == tokenA)
	strcpy (full_stringA, "*/*/ARG,LYS,HIS,GLU,ASP/*");

/** MAIN_CHAIN: **/
else if (strstr (tokenA, "MAI") == tokenA)
	strcpy (full_stringA, "*/*/*/CA,C,N,O,H");

/** SIDE_CHAINS: **/
else if (strstr (tokenA, "SID") == tokenA)
	strcpy (full_stringA, "*/*/*/* EXCEPT CA,C,N,O,H");

/** HETERO: **/
else if (strstr (tokenA, "HET") == tokenA)
	{
	selected_atomsN = SelectHetero_ (mol_complexSP, mol_complexesN,
					 selection_modeI);
	return selected_atomsN;
	}

/** CIS: **/
else if (strstr (tokenA, "CIS") == tokenA)
	{
	selected_atomsN = SelectCisTrans_ (mol_complexSP, mol_complexesN,
					   selection_modeI, 2);
	return selected_atomsN;
	}

/** TRANS: **/
else if (strstr (tokenA, "TRA") == tokenA)
	{
	selected_atomsN = SelectCisTrans_ (mol_complexSP, mol_complexesN,
					   selection_modeI, 1);
	return selected_atomsN;
	}

/** BAD: **/
else if (strstr (tokenA, "BAD") == tokenA)
	{
	selected_atomsN = SelectCisTrans_ (mol_complexSP, mol_complexesN,
					   selection_modeI, 0);
	return selected_atomsN;
	}

/** COMPLEMENT: **/
else if (strstr (tokenA, "COM") == tokenA)
	{
	selected_atomsN = SelectComplement_ (mol_complexSP, mol_complexesN,
					     selection_modeI);
	return selected_atomsN;
	}

/** SEQUENCE: **/
else if (strstr (tokenA, "SEQ") == tokenA)
	{
	selected_atomsN = SelectSequence_ (mol_complexSP, mol_complexesN,
					   runtimeSP, selection_modeI);
	return selected_atomsN;
	}

/** SPHERE: **/
else if (strstr (tokenA, "SPH") == tokenA)
	{
	selected_atomsN = SelectSphere_ (mol_complexSP, mol_complexesN,
					 runtimeSP, selection_modeI);
	return selected_atomsN;
	}

/** ALTERNATE: **/
else if (strstr (tokenA, "ALT") == tokenA)
	{
	selected_atomsN = SelectAlternate_ (mol_complexSP, mol_complexesN,
					    selection_modeI);
	return selected_atomsN;
	}

/** MODEL: **/
else if (strstr (tokenA, "MOD") == tokenA)
	{
	model_serialI = ExtractIndex_ (stringP);
	selected_atomsN = SelectModel_ (mol_complexSP, mol_complexesN,
					selection_modeI, model_serialI);
	return selected_atomsN;
	}

/** ABOVE: **/
else if (strstr (tokenA, "ABO") == tokenA)
	{
	selected_atomsN = SelectAbove_ (mol_complexSP, mol_complexesN,
					selection_modeI);
	return selected_atomsN;
	}

/** BELOW: **/
else if (strstr (tokenA, "BEL") == tokenA)
	{
	selected_atomsN = SelectBelow_ (mol_complexSP, mol_complexesN,
					selection_modeI);
	return selected_atomsN;
	}

/** TRIPLET: **/
else if (strstr (tokenA, "TRI") == tokenA)
	{
	selected_atomsN = SelectTriplet_ (mol_complexSP, mol_complexesN,
					  selection_modeI);
	return selected_atomsN;
	}

/** TM (transmembrane part): **/
else if (strstr (tokenA, "TM") == tokenA)
	{
	selected_atomsN = SelectTM_ (mol_complexSP, mol_complexesN,
				     selection_modeI);
	return selected_atomsN;
	}

/** PATTERN (sequence pattern): **/
else if (strstr (tokenA, "PAT") == tokenA)
	{
	selected_atomsN = SelectPattern_ (mol_complexSP, mol_complexesN,
					  runtimeSP, selection_modeI);
	return selected_atomsN;
	}

/** 2C3 (two or more charged in a fragment of three residues): **/
else if (strstr (tokenA, "2C3") == tokenA)
	{
	selected_atomsN = Select2C3_ (mol_complexSP, mol_complexesN,
				      selection_modeI);
	return selected_atomsN;
	}

/** 2CZ3 (two or more charged or zwitterionic in a fragment of three): **/
else if (strstr (tokenA, "2CZ3") == tokenA)
	{
	selected_atomsN = Select2CZ3_ (mol_complexSP, mol_complexesN,
				       selection_modeI);
	return selected_atomsN;
	}

/** 2NZ3 (two or more negative or zwitterionic in a fragment of three): **/
else if (strstr (tokenA, "2NZ3") == tokenA)
	{
	selected_atomsN = Select2NZ3_ (mol_complexSP, mol_complexesN,
				       selection_modeI);
	return selected_atomsN;
	}

/** 3C4 (three or more charged in a fragment of four residues): **/
else if (strstr (tokenA, "3C4") == tokenA)
	{
	selected_atomsN = Select3C4_ (mol_complexSP, mol_complexesN,
				      selection_modeI);
	return selected_atomsN;
	}

/** 3CZ4 (three or more charged or zwitterionic in a fragment of four): **/
else if (strstr (tokenA, "3CZ4") == tokenA)
	{
	selected_atomsN = Select3CZ4_ (mol_complexSP, mol_complexesN,
				       selection_modeI);
	return selected_atomsN;
	}

/** 3NZ4 (three or more negative or zwitterionic in a fragment of four): **/
else if (strstr (tokenA, "3NZ4") == tokenA)
	{
	selected_atomsN = Select3NZ4_ (mol_complexSP, mol_complexesN,
				       selection_modeI);
	return selected_atomsN;
	}

/** 3C5 (three or more charged in a fragment of five residues): **/
else if (strstr (tokenA, "3C5") == tokenA)
	{
	selected_atomsN = Select3C5_ (mol_complexSP, mol_complexesN,
				      selection_modeI);
	return selected_atomsN;
	}

/** 3CZ5 (three or more charged or zwitterionic in a fragment of five): **/
else if (strstr (tokenA, "3CZ5") == tokenA)
	{
	selected_atomsN = Select3CZ5_ (mol_complexSP, mol_complexesN,
				       selection_modeI);
	return selected_atomsN;
	}

/** 3NZ5 (three or more negative or zwitterionic in a fragment of five): **/
else if (strstr (tokenA, "3NZ5") == tokenA)
	{
	selected_atomsN = Select3NZ5_ (mol_complexSP, mol_complexesN,
				       selection_modeI);
	return selected_atomsN;
	}

/** 4C5 (four or more charged in a fragment of five residues): **/
else if (strstr (tokenA, "4C5") == tokenA)
	{
	selected_atomsN = Select4C5_ (mol_complexSP, mol_complexesN,
				      selection_modeI);
	return selected_atomsN;
	}

/** 4CZ5 (four or more charged or zwitterionic in a fragment of five): **/
else if (strstr (tokenA, "4CZ5") == tokenA)
	{
	selected_atomsN = Select4CZ5_ (mol_complexSP, mol_complexesN,
				       selection_modeI);
	return selected_atomsN;
	}

/** 4NZ5 (four or more negative or zwitterionic in a fragment of five): **/
else if (strstr (tokenA, "4NZ5") == tokenA)
	{
	selected_atomsN = Select4NZ5_ (mol_complexSP, mol_complexesN,
				       selection_modeI);
	return selected_atomsN;
	}

/** 3C6 (three or more charged in a fragment of six residues): **/
else if (strstr (tokenA, "3C6") == tokenA)
	{
	selected_atomsN = Select3C6_ (mol_complexSP, mol_complexesN,
				      selection_modeI);
	return selected_atomsN;
	}

/** 3CZ6 (three or more charged or zwitterionic in a fragment of six): **/
else if (strstr (tokenA, "3CZ6") == tokenA)
	{
	selected_atomsN = Select3CZ6_ (mol_complexSP, mol_complexesN,
				       selection_modeI);
	return selected_atomsN;
	}

/** 3NZ6 (three or more negative or zwitterionic in a fragment of six): **/
else if (strstr (tokenA, "3NZ6") == tokenA)
	{
	selected_atomsN = Select3NZ6_ (mol_complexSP, mol_complexesN,
				       selection_modeI);
	return selected_atomsN;
	}

/** 4C6 (four or more charged in a fragment of six residues): **/
else if (strstr (tokenA, "4C6") == tokenA)
	{
	selected_atomsN = Select4C6_ (mol_complexSP, mol_complexesN,
				      selection_modeI);
	return selected_atomsN;
	}

/** 4CZ6 (four or more charged or zwitterionic in a fragment of six): **/
else if (strstr (tokenA, "4CZ6") == tokenA)
	{
	selected_atomsN = Select4CZ6_ (mol_complexSP, mol_complexesN,
				       selection_modeI);
	return selected_atomsN;
	}

/** 4NZ6 (four or more negative or zwitterionic in a fragment of six): **/
else if (strstr (tokenA, "4NZ6") == tokenA)
	{
	selected_atomsN = Select4NZ6_ (mol_complexSP, mol_complexesN,
				       selection_modeI);
	return selected_atomsN;
	}

/** 4P6 (four or more polar in a fragment of six): **/
else if (strstr (tokenA, "4P6") == tokenA)
	{
	selected_atomsN = Select4P6_ (mol_complexSP, mol_complexesN,
				      selection_modeI);
	return selected_atomsN;
	}

/** 5C6 (five or more charged in a fragment of six residues): **/
else if (strstr (tokenA, "5C6") == tokenA)
	{
	selected_atomsN = Select5C6_ (mol_complexSP, mol_complexesN,
				      selection_modeI);
	return selected_atomsN;
	}

/** 5CZ6 (five or more charged or zwitterionic in a fragment of six): **/
else if (strstr (tokenA, "5CZ6") == tokenA)
	{
	selected_atomsN = Select5CZ6_ (mol_complexSP, mol_complexesN,
				       selection_modeI);
	return selected_atomsN;
	}

/** 5NZ6 (five or more negative or zwitterionic in a fragment of six): **/
else if (strstr (tokenA, "5NZ6") == tokenA)
	{
	selected_atomsN = Select5NZ6_ (mol_complexSP, mol_complexesN,
				       selection_modeI);
	return selected_atomsN;
	}

/** 4C7 (four or more charged in a fragment of seven residues): **/
else if (strstr (tokenA, "4C7") == tokenA)
	{
	selected_atomsN = Select4C7_ (mol_complexSP, mol_complexesN,
				      selection_modeI);
	return selected_atomsN;
	}

/** 4CZ7 (four or more charged or zwitterionic in a fragment of seven): **/
else if (strstr (tokenA, "4CZ7") == tokenA)
	{
	selected_atomsN = Select4CZ7_ (mol_complexSP, mol_complexesN,
				       selection_modeI);
	return selected_atomsN;
	}

/** 4NZ7 (four or more negative or zwitterionic in a fragment of seven): **/
else if (strstr (tokenA, "4NZ7") == tokenA) 
	{
	selected_atomsN = Select4NZ7_ (mol_complexSP, mol_complexesN,
				       selection_modeI);
	return selected_atomsN;
	}

/** 5C7 (five or more charged in a fragment of seven residues): **/
else if (strstr (tokenA, "5C7") == tokenA) 
	{
	selected_atomsN = Select5C7_ (mol_complexSP, mol_complexesN,
				      selection_modeI);
	return selected_atomsN;
	}

/** 5CZ7 (five or more charged or zwitterionic in a fragment of seven): **/
else if (strstr (tokenA, "5CZ7") == tokenA)
	{
	selected_atomsN = Select5CZ7_ (mol_complexSP, mol_complexesN,
				       selection_modeI);
	return selected_atomsN;
	}

/** 5NZ7 (five or more negative or zwitterionic in a fragment of seven): **/
else if (strstr (tokenA, "5NZ7") == tokenA) 
	{       
	selected_atomsN = Select5NZ7_ (mol_complexSP, mol_complexesN,
				       selection_modeI);
	return selected_atomsN;
	}

/** 3P5 (three or more polar in a fragment of five residues): **/
else if (strstr (tokenA, "3P5") == tokenA)
	{
	selected_atomsN = Select3P5_ (mol_complexSP, mol_complexesN,
				      selection_modeI);
	return selected_atomsN;
	}

/** 5P7 (five or more polar in a fragment of seven residues): **/
else if (strstr (tokenA, "5P7") == tokenA)
        {
        selected_atomsN = Select5P7_ (mol_complexSP, mol_complexesN,
				      selection_modeI);
        return selected_atomsN;
        }

/** 6P7 (six or more polar in a fragment of seven residues): **/
else if (strstr (tokenA, "6P7") == tokenA)
	{
	selected_atomsN = Select6P7_ (mol_complexSP, mol_complexesN,
				      selection_modeI);
	return selected_atomsN;
	}

/** 4C9 (four or more charged in a fragment of nine residues): **/
else if (strstr (tokenA, "4C9") == tokenA)
	{
	selected_atomsN = Select4C9_ (mol_complexSP, mol_complexesN,
				      selection_modeI);
	return selected_atomsN;
	}

/** 4CZ9 (four or more charged or zwitterionic in a fragment of nine): **/
else if (strstr (tokenA, "4CZ9") == tokenA)
	{
	selected_atomsN = Select4CZ9_ (mol_complexSP, mol_complexesN,
				       selection_modeI);
	return selected_atomsN;
	}

/** 4NZ9 (four or more negative or zwitterionic in a fragment of nine): **/
else if (strstr (tokenA, "4NZ9") == tokenA)
	{
	selected_atomsN = Select4NZ9_ (mol_complexSP, mol_complexesN,
				       selection_modeI);
	return selected_atomsN;
	}

/** 5C9 (five or more charged in a fragment of nine residues): **/
else if (strstr (tokenA, "5C9") == tokenA) 
	{
	selected_atomsN = Select5C9_ (mol_complexSP, mol_complexesN,
				      selection_modeI);
	return selected_atomsN;
	}

/** 5CZ9 (five or more charged or zwitterionic in a fragment of nine): **/
else if (strstr (tokenA, "5CZ9") == tokenA)
	{
	selected_atomsN = Select5CZ9_ (mol_complexSP, mol_complexesN, 
				       selection_modeI);
	return selected_atomsN;
	}

/** 5NZ9 (five or more negative or zwitterionic in a fragment of nine): **/
else if (strstr (tokenA, "5NZ9") == tokenA)
	{
	selected_atomsN = Select5NZ9_ (mol_complexSP, mol_complexesN,
				       selection_modeI);
	return selected_atomsN;
	}

/** String not recognized: **/
else return (long) -1;		/* Must be -1! */

/* Count slashes: */
P = full_stringA;
while ((n = *P++) != '\0') if (n == '/') slashesN++;
if (slashesN != 3) return (long) -1;

/* Identify chains,  residue ranges,  residue */
/* names and atoms which have to be included: */
if (Include_ (&include_selectS, full_stringA) < 0) return (long) -2;

/* Identify chains,  residue ranges,  residue */
/* names and atoms which have to be excluded: */
if (Exclude_ (&exclude_selectS, full_stringA) < 0) return (long) -3;

/* Do the selection: */
selected_atomsN = ApplySelection_ (mol_complexSP, mol_complexesN,
				   &include_selectS, &exclude_selectS,
				   selection_modeI);

/* Return the number of selected atoms: */
return selected_atomsN;
}

/*===========================================================================*/


