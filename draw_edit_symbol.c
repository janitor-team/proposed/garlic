/* Copyright (C) 2001 Damir Zucic */

/*=============================================================================

			    draw_edit_symbol.c

Purpose:
	Draw the  edit symbol:  squid or seahorse.  The squid is drawn at
	the position of the first selected atom of default macromolecular
	complex.  The seahorse is drawn  in the middle of  the first bond
	from the set of bonds  which have  to be rotated.  If editing phi
	angle, the seahorse will be drawn in the middle of N-CA bond.  If
	editing psi, it will be drawn in the middle of CA-C bond.  In all
	other cases only one bond  is selected for editing.  The seahorse
	will be drawn in the middle of this bond.  There is no symbol for
	default editing mode (0 = no editing).

Input:
	(1) Pointer to MolComplexS structure.
	(2) Number of macromolecular complexes.
	(3) Pointer to RuntimeS structure.
	(4) Pointer to ConfigS structure.
	(5) Pointer to GUIS structure.
	(6) Pointer to NearestAtomS structure.
	(7) The number of pixels in the main window free area.
	(8) The refreshI,  used to check the NearestAtomS associated with
	    a given pixel.

Output:
	(1) The edit symbol drawn.
	(2) Return value.

Return value:
	(1) Zero if editing is off.
	(2) Negative if there are no atoms or no atom is selected.
	(3) Positive in all other cases.

=============================================================================*/

#include <stdio.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

int		DrawSquid_ (AtomS *, ConfigS *, GUIS *,
			    NearestAtomS *, size_t, unsigned int);
int		DrawSeahorse_ (MolComplexS *, RuntimeS *, ConfigS *, GUIS *,
			       NearestAtomS *, size_t, unsigned int);

/*======draw edit symbol:====================================================*/

int DrawEditSymbol_ (MolComplexS *mol_complexSP, int mol_complexesN,
		     RuntimeS *runtimeSP, ConfigS *configSP, GUIS *guiSP,
		     NearestAtomS *nearest_atomSP, size_t pixelsN,
		     unsigned int refreshI)
{
MolComplexS		*default_mol_complexSP;
size_t			atomsN, atomI;
int			something_is_selectedF;
static AtomS		*curr_atomSP;

/* Check editing mode: */
if (runtimeSP->edit_modeI == 0) return 0;

/* Prepare the pointer to default macromolecular complex: */
default_mol_complexSP = mol_complexSP + runtimeSP->default_complexI;

/* Check are there any atoms at all: */
atomsN = default_mol_complexSP->atomsN;
if (atomsN == 0) return -1;

/* Draw the proper symbol: */
switch (runtimeSP->edit_modeI)
	{
	/* Draw squid if editing atom or dimensions: */
	case   1:
	case 101:
		/* Check is there anything selected at all: */
		something_is_selectedF = 0;
		for (atomI = 0; atomI < atomsN; atomI++)
			{
			/* Pointer to the current atom: */
			curr_atomSP = default_mol_complexSP->atomSP + atomI;

			/* If the current atom is selected, set */
			/* the flag  and  break from  the loop: */
			if (curr_atomSP->selectedF)
				{
				something_is_selectedF = 1;
				break;
				}
			}
		if (!something_is_selectedF) return -2;

		/* Draw squid at the position of the first selected atom: */
		DrawSquid_ (curr_atomSP, configSP, guiSP,
			    nearest_atomSP, pixelsN, refreshI);
		break;

	/* Editing phi, psi, omega, bond, main chain or side chain: */
	case 2:
	case 3:
	case 4:
	case 5:
	case 6:
	case 7:
		DrawSeahorse_ (mol_complexSP, runtimeSP, configSP, guiSP,
			       nearest_atomSP, pixelsN, refreshI);
		break;

	/* Unsupported editing modes: */
	default:
		;
	}

/* Return positive value on success: */
return 1;
}

/*===========================================================================*/


