/* Copyright (C) 2000-2003 Damir Zucic */

/*=============================================================================

				draw_system.c

Purpose:
	Put small legend describing the coordinate system to the top left
	corner of  the main window.  Do not  create pixmap,  just use raw
	pixmap data. Red color is treated as transparent. Read system.xpm
	to check the pixmap!

Input:
	(1) Pointer to GUIS structure, with GUI data.
	(2) Pixmap file (included as header).

Output:
	(1) Small transparent pixmap drawn to  the top left corner of the
	    main window.
	(2) Return value.

Return value:
	(1) Positive on success.
	(2) Negative on failure.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"
#include "system.xpm"

/*======put coordinate system description to the top left corner:============*/

int DrawSystem_ (GUIS *guiSP)
{
static int		width = 50, height = 54, i, j, screen_x, screen_y;
int			current_char;

/* Prepare two graphics contexts (set colors): */
XSetForeground (guiSP->displaySP, guiSP->theGCA[0], guiSP->black_colorID);
XSetForeground (guiSP->displaySP, guiSP->theGCA[1], guiSP->white_colorID);

/* Draw the pixmap to the screen, leaving part of the background visible: */
for (i = 0; i < height; i++) /* i is line index */
	{
	for (j = 0; j < width; j++) /* j is column index */
		{
		/** Prepare the pixel position: **/
		screen_x = j;
		screen_y = i + guiSP->main_winS.font_height + 7;

		/** Check the current character: **/
		current_char = system_xpm[i + 4][j]; 
		switch (current_char)
			{
			case '#': /* Do nothing - transparent color (red)! */
				break;

			case 'a':
				XDrawPoint (guiSP->displaySP,
					    guiSP->main_winS.ID,
					    guiSP->theGCA[0],
					    screen_x, screen_y);
				break;

			case '.':
				XDrawPoint (guiSP->displaySP,
					    guiSP->main_winS.ID,
					    guiSP->theGCA[1],
					    screen_x, screen_y);
				break;

			default:
				;
			}
		}
	}

/* Return positive value on success: */
return 1;
}

/*===========================================================================*/


