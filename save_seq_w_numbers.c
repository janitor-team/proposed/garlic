/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				save_seq_w_numbers.c

Purpose:
	Save sequence to file. This function outputs three letters code
	accompained with serial numbers.

Input:
	(1) Pointer to RuntimeS structure.

Output:
	(1) Sequence stored to file.
	(2) Return value.

Return value:
	(1) Positive on success.
	(2) Negative on failure.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

char		*ExtractToken_ (char *, int, char *, char *);

/*======save sequence (with numbers) to file:================================*/

int SaveSequenceWithNumbers_ (RuntimeS *runtimeSP)
{

int		max_length;
char		lineA[STRINGSIZE];
char		*remainderP;
char		tokenA[STRINGSIZE];
FILE		*fileP;
size_t		residueI;
char		*nameP;
int		serialI;
char		residue_nameA[RESNAMESIZE];

/* The maximal residue name length: */
max_length = RESNAMESIZE - 1;

/* Copy the original command string: */
strncpy (lineA, runtimeSP->curr_commandA, STRINGSIZE - 1);
lineA[STRINGSIZE - 1] = '\0';

/* Skip two tokens: */
remainderP = ExtractToken_ (tokenA, STRINGSIZE, lineA, " \t\n");
if (!remainderP) return -1;
remainderP = ExtractToken_ (tokenA, STRINGSIZE, remainderP, " \t\n");
if (!remainderP) return -2;

/* The third token should contain the file name: */
remainderP = ExtractToken_ (tokenA, STRINGSIZE, remainderP, " \t\n");
if (!remainderP)
	{
	strcpy (runtimeSP->messageA, "File name missing!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return -3;
	}

/* Try to open file: */
fileP = fopen (tokenA, "w");
if (fileP == NULL)
	{
	strcpy (runtimeSP->messageA, "Failed to open file!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return -4;
	}

/* Write sequence to file: */
for (residueI = 0; residueI < runtimeSP->residuesN; residueI++)
	{
	/* Prepare the pointer to the current residue name: */
	nameP = runtimeSP->sequenceP + residueI * max_length;

	/* Copy serial number: */
	serialI = *(runtimeSP->serialIP + residueI);

	/* Copy residue name: */
	strncpy (residue_nameA, nameP, max_length);
	residue_nameA[max_length] = '\0';

	/* Print serial number and residue name: */
	fprintf (fileP, "%4d %-4s", serialI, residue_nameA);

	/* Add newline after every five residues: */
	if (((residueI + 1) % 5) == 0) fprintf (fileP, "\n");
	}

/* Add newline at the end of file: */
fprintf (fileP, "\n");

/* Close file: */
fclose (fileP);

/* Return positive value on success: */
return 1;
}

/*===========================================================================*/


