/* Copyright (C) 2000-2002 Damir Zucic */

/*=============================================================================

				prepare_cursor.c

Purpose:
	Prepare cursor. Use either default garlic cursor, or one of X11
	predefined cursors.  If cursor name is not recognized,  replace
	with "crosshair".

Input:
	(1) Pointer to GUIS structure.
	(2) Cursor name (based on /usr/include/X11/cursorfont.h).

Output:
	Return value.

Return value:
	Cursor ID.

========includes:============================================================*/

#include <stdio.h>
#include <string.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include <X11/cursorfont.h>

#include "defines.h"
#include "typedefs.h"

/*======defines:=============================================================*/

#define NAMESNUM 78

/*======prepare the main window cursor:======================================*/

Cursor PrepareCursor_ (GUIS *guiSP, char *nameP)
{
static unsigned int	cursor_shape_width = 16, cursor_shape_height = 16;
static unsigned char	cursor_shape_bits[32] = {0x80, 0x00, 0x80, 0x00,
						 0x80, 0x00, 0x80, 0x00,
						 0x80, 0x00, 0x00, 0x00,
						 0x00, 0x00, 0x1f, 0x7c,
						 0x00, 0x00, 0x00, 0x00,
						 0x80, 0x00, 0x80, 0x00,
						 0x80, 0x00, 0x80, 0x00,
						 0x80, 0x00, 0x00, 0x00};
Pixmap			shape_pixmapID;
static unsigned int	cursor_mask_width = 16, cursor_mask_height = 16;
static unsigned char	cursor_mask_bits[32]  = {0xc0, 0x01, 0xc0, 0x01,
						 0xc0, 0x01, 0xc0, 0x01,
						 0xc0, 0x01, 0x80, 0x00,
						 0x1f, 0x7c, 0x3f, 0x7e,
						 0x1f, 0x7c, 0x80, 0x00,
						 0xc0, 0x01, 0xc0, 0x01,
						 0xc0, 0x01, 0xc0, 0x01,
						 0xc0, 0x01, 0x00, 0x00};
Pixmap			mask_pixmapID;
XColor			fg_colorS, bg_colorS;
Cursor			cursorID;
int			i;

struct CursorNameS
	{
	char		nameA[20];
	unsigned	cursorID;
	};

struct CursorNameS	cnSA[NAMESNUM] = {
		{"X_cursor",            0}, {"arrow",                2},
		{"based_arrow_down",    4}, {"based_arrow_up",       6},
		{"boat",                8}, {"bogosity",            10},
		{"bottom_left_corner", 12}, {"bottom_right_corner", 14},
		{"bottom_side",        16}, {"bottom_tee",          18},
		{"box_spiral",         20}, {"center_ptr",          22},
		{"circle",             24}, {"clock",               26},
		{"coffee_mug",         28}, {"cross",               30},
		{"cross_reverse",      32}, {"crosshair",           34},
		{"diamond_cross",      36}, {"dot",                 38},
		{"dotbox",             40}, {"double_arrow",        42},
		{"draft_large",        44}, {"draft_small",         46},
		{"draped_box",         48}, {"exchange",            50},
		{"fleur",              52}, {"gobbler",             54},
		{"gumby",              56}, {"hand1",               58},
		{"hand2",              60}, {"heart",               62},
		{"icon",               64}, {"iron_cross",          66},
		{"left_ptr",           68}, {"left_side",           70},
		{"left_tee",           72}, {"leftbutton",          72},
		{"ll_angle",           76}, {"lr_angle",            78},
		{"man",                80}, {"middlebutton",        82},
		{"mouse",              84}, {"pencil",              86},
		{"pirate",             88}, {"plus",                90},
		{"question_arrow",     92}, {"right_ptr",           94},
		{"right_side",         96}, {"right_tee",           98},
		{"rightbutton",       100}, {"rtl_logo",           102},
		{"sailboat",          104}, {"sb_down_arrow",      106},
		{"sb_h_double_arrow", 108}, {"sb_left_arrow",      110},
		{"sb_right_arrow",    112}, {"sb_up_arrow",        114},
		{"sb_v_double_arrow", 116}, {"shuttle",            118},
		{"sizing",            120}, {"spider",             122},
		{"spraycan",          124}, {"star",               126},
		{"target",            128}, {"tcross",             130},
		{"top_left_arrow",    132}, {"top_left_corner",    134},
		{"top_right_corner",  136}, {"top_side",           138},
		{"top_tee",           140}, {"trek",               142},
		{"ul_angle",          144}, {"umbrella",           146},
		{"ur_angle",          148}, {"watch",              150},
		{"xterm",             152}, {"num_glyphs",         154}};

/* Check is default cursor required: */
if (strcmp (nameP, "default") == 0)
	{
	/** Prepare the shape pixmap: **/
	shape_pixmapID = XCreatePixmapFromBitmapData (
				guiSP->displaySP,
				RootWindow (guiSP->displaySP, guiSP->screenID),
				(char *) cursor_shape_bits,
				cursor_shape_width, cursor_shape_height,
				1, 0, 1);

	/** Prepare the mask pixmap: **/
	mask_pixmapID = XCreatePixmapFromBitmapData (
				guiSP->displaySP,
				RootWindow (guiSP->displaySP, guiSP->screenID),
				(char *) cursor_mask_bits,
				cursor_mask_width, cursor_mask_height,
				1, 0, 1);

	/** Prepare colors: **/
	XParseColor (guiSP->displaySP, guiSP->colormapID, "black", &fg_colorS);
	XParseColor (guiSP->displaySP, guiSP->colormapID, "white", &bg_colorS);

	/** Create the main window cursor: **/
	cursorID = XCreatePixmapCursor (guiSP->displaySP,
					shape_pixmapID, mask_pixmapID,
					&fg_colorS, &bg_colorS, 7, 7);

	/** Return the cursor ID: **/
	return cursorID;
	}

/* Default garlic cursor not required; compare */
/* the specified cursor name with X11 cursors: */
if (strlen (nameP) > 0)
	{
	/* Try to recognize cursor name; create cursor and return ID: */
	for (i = 0; i < NAMESNUM; i++)
		{
		if (strcmp (nameP, cnSA[i].nameA) == 0)
			{
			return XCreateFontCursor (guiSP->displaySP,
						  cnSA[i].cursorID);
			}
		}
	}

/* Return default (crosshair) cursor ID if cursor name not recognized: */
return XCreateFontCursor (guiSP->displaySP, XC_crosshair);
}

/*===========================================================================*/


