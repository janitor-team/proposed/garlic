/* Copyright (C) 2000-2002 Damir Zucic */

/*=============================================================================

				mix_colors.c

Purpose:
	Mix two color schemes to prepare colors for a given atom.

Input:
	(1) Pointer to AtomS structure.
	(2) Pointer to ColorSchemeS structure,  with the first color scheme.
	(3) Pointer to ColorSchemeS structure, with the second color scheme.
	(4) Scale factor (double).
	(5) Pointer to GUIS structure.

Output:
	(1) RGBS data set in AtomS structure.
	(1) Return value.

Return value:
	(1) Positive on success.
	(2) Negative on failure.

Notes:
	(1) The input color schemes should contain enough data to define two
	    color surfaces.

=============================================================================*/

#include <stdio.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======mix (combine) two color schemes:=====================================*/

int MixColors_ (AtomS *curr_atomSP,
		ColorSchemeS *scheme1SP, ColorSchemeS *scheme2SP,
		double scale_factor, GUIS *guiSP)
{
static double		color1, delta_color, color;

/* Check color schemes - both should contain data for two color surfaces: */
if (scheme1SP->surfacesN < 2) return -1;
if (scheme2SP->surfacesN < 2) return -2;

/* Colors at the surface 0 (front surface): */

/** Left color: **/

/*** Red component: ***/
color1 = (double) scheme1SP->left_rgbSA[0].red;
delta_color = (double) scheme2SP->left_rgbSA[0].red -
	      (double) scheme1SP->left_rgbSA[0].red;
color = color1 + scale_factor * delta_color;
curr_atomSP->left_rgbSA[0].red = (unsigned short) color;

/*** Green component: ***/
color1 = (double) scheme1SP->left_rgbSA[0].green;
delta_color = (double) scheme2SP->left_rgbSA[0].green -
	      (double) scheme1SP->left_rgbSA[0].green;
color = color1 + scale_factor * delta_color;
curr_atomSP->left_rgbSA[0].green = (unsigned short) color;

/*** Blue component: ***/
color1 = (double) scheme1SP->left_rgbSA[0].blue;
delta_color = (double) scheme2SP->left_rgbSA[0].blue -
	      (double) scheme1SP->left_rgbSA[0].blue;
color = color1 + scale_factor * delta_color;
curr_atomSP->left_rgbSA[0].blue = (unsigned short) color;

/** Middle color: **/

/*** Red component: ***/
color1 = (double) scheme1SP->middle_rgbSA[0].red;
delta_color = (double) scheme2SP->middle_rgbSA[0].red -
	      (double) scheme1SP->middle_rgbSA[0].red;
color = color1 + scale_factor * delta_color;
curr_atomSP->middle_rgbSA[0].red = (unsigned short) color;

/*** Green component: ***/
color1 = (double) scheme1SP->middle_rgbSA[0].green;
delta_color = (double) scheme2SP->middle_rgbSA[0].green -
	      (double) scheme1SP->middle_rgbSA[0].green;
color = color1 + scale_factor * delta_color;
curr_atomSP->middle_rgbSA[0].green = (unsigned short) color;

/*** Blue component: ***/
color1 = (double) scheme1SP->middle_rgbSA[0].blue;
delta_color = (double) scheme2SP->middle_rgbSA[0].blue -
	      (double) scheme1SP->middle_rgbSA[0].blue;
color = color1 + scale_factor * delta_color;
curr_atomSP->middle_rgbSA[0].blue = (unsigned short) color;

/** Right color: **/

/*** Red component: ***/
color1 = (double) scheme1SP->right_rgbSA[0].red;
delta_color = (double) scheme2SP->right_rgbSA[0].red -
	      (double) scheme1SP->right_rgbSA[0].red;
color = color1 + scale_factor * delta_color;
curr_atomSP->right_rgbSA[0].red = (unsigned short) color;

/*** Green component: ***/
color1 = (double) scheme1SP->right_rgbSA[0].green;
delta_color = (double) scheme2SP->right_rgbSA[0].green -
	      (double) scheme1SP->right_rgbSA[0].green;
color = color1 + scale_factor * delta_color;
curr_atomSP->right_rgbSA[0].green = (unsigned short) color;

/*** Blue component: ***/
color1 = (double) scheme1SP->right_rgbSA[0].blue;
delta_color = (double) scheme2SP->right_rgbSA[0].blue -
	      (double) scheme1SP->right_rgbSA[0].blue;
color = color1 + scale_factor * delta_color;
curr_atomSP->right_rgbSA[0].blue = (unsigned short) color;

/* Colors at the surface 1 (back surface): */

/** Left color: **/

/*** Red component: ***/
color1 = (double) scheme1SP->left_rgbSA[1].red;
delta_color = (double) scheme2SP->left_rgbSA[1].red -
	      (double) scheme1SP->left_rgbSA[1].red;
color = color1 + scale_factor * delta_color;
curr_atomSP->left_rgbSA[1].red = (unsigned short) color;

/*** Green component: ***/
color1 = (double) scheme1SP->left_rgbSA[1].green;
delta_color = (double) scheme2SP->left_rgbSA[1].green -
	      (double) scheme1SP->left_rgbSA[1].green;
color = color1 + scale_factor * delta_color;
curr_atomSP->left_rgbSA[1].green = (unsigned short) color;

/*** Blue component: ***/
color1 = (double) scheme1SP->left_rgbSA[1].blue;
delta_color = (double) scheme2SP->left_rgbSA[1].blue -
	      (double) scheme1SP->left_rgbSA[1].blue;
color = color1 + scale_factor * delta_color;
curr_atomSP->left_rgbSA[1].blue = (unsigned short) color;

/** Middle color: **/

/*** Red component: ***/
color1 = (double) scheme1SP->middle_rgbSA[1].red;
delta_color = (double) scheme2SP->middle_rgbSA[1].red -
	      (double) scheme1SP->middle_rgbSA[1].red;
color = color1 + scale_factor * delta_color;
curr_atomSP->middle_rgbSA[1].red = (unsigned short) color;

/*** Green component: ***/
color1 = (double) scheme1SP->middle_rgbSA[1].green;
delta_color = (double) scheme2SP->middle_rgbSA[1].green -
	      (double) scheme1SP->middle_rgbSA[1].green;
color = color1 + scale_factor * delta_color;
curr_atomSP->middle_rgbSA[1].green = (unsigned short) color;

/*** Blue component: ***/
color1 = (double) scheme1SP->middle_rgbSA[1].blue;
delta_color = (double) scheme2SP->middle_rgbSA[1].blue -
	      (double) scheme1SP->middle_rgbSA[1].blue;
color = color1 + scale_factor * delta_color;
curr_atomSP->middle_rgbSA[1].blue = (unsigned short) color;

/** Right color: **/

/*** Red component: ***/
color1 = (double) scheme1SP->right_rgbSA[1].red;
delta_color = (double) scheme2SP->right_rgbSA[1].red -
	      (double) scheme1SP->right_rgbSA[1].red;
color = color1 + scale_factor * delta_color;
curr_atomSP->right_rgbSA[1].red = (unsigned short) color;

/*** Green component: ***/
color1 = (double) scheme1SP->right_rgbSA[1].green;
delta_color = (double) scheme2SP->right_rgbSA[1].green -
	      (double) scheme1SP->right_rgbSA[1].green;
color = color1 + scale_factor * delta_color;
curr_atomSP->right_rgbSA[1].green = (unsigned short) color;

/*** Blue component: ***/
color1 = (double) scheme1SP->right_rgbSA[1].blue;
delta_color = (double) scheme2SP->right_rgbSA[1].blue -
	      (double) scheme1SP->right_rgbSA[1].blue;
color = color1 + scale_factor * delta_color;
curr_atomSP->right_rgbSA[1].blue = (unsigned short) color;

/* Return positive value on success: */
return 1;
}

/*===========================================================================*/


