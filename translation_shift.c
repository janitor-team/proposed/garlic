/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				translation_shift.c

Purpose:
	Prepare the translation shift. By default, the normal translation
	shift is copied  (translation_shiftA[2] from  ConfigS structure).
	See note 1 for combination of modifier keys. Modifier keys may be
	used to select the translation shift.

Input:
	(1) Pointer to ConfigS structure, with configuration data.
	(2) Pointer to GUIS structure, with GUI data.
	(3) The sign used to distinguish  positive and negative translat.
	    Value +1.0 is used with positive translation, while -1.0
	    defines negative translation.

Output:
	(1) Return value.

Return value:
	(1) Translation shift.

Notes:
	(1) The following  combinations  of modifier keys  may be used to
	    select the translation shift:
	    -------------------------------------------------------------
	    |  modifiers         translation shift    ConfigS member    |
	    |-----------------------------------------------------------|
	    | none               normal (default)  translation_stepA[2] |
	    | shift              large             translation_stepA[3] |
	    | alt+shift          very large        translation_stepA[4] |
	    | control            small             translation_stepA[1] |
	    | alt+control        very small        translation_stepA[0] |
	    | shift+control      very small        translation_stepA[2] |
	    | alt                normal            translation_stepA[2] |
	    | alt+shift+control  normal            translation_stepA[2] |
	    -------------------------------------------------------------
	    Note that some  modifier combinations are ignored  (alt alone
	    and alt+control+shift).

	(2) On some laptops the status of shift key may be changed before
	    the KeyPress event is send for the keys on  "numeric keypad".
 
========includes:============================================================*/

#include <stdio.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======prepare translation shift:===========================================*/

double TranslationShift_ (ConfigS *configSP, GUIS *guiSP, double sign)
{
double		translation_shift;
int		largeF, smallF, altF;

/* Copy the flags: */
largeF = guiSP->shift_pressedF;
smallF = guiSP->control_pressedF;
altF   = guiSP->alt_pressedF;

/* Select the requested translation shift: */
if (altF)
	{
	if (largeF && !smallF)
		{
		translation_shift = configSP->translation_stepA[4];
		}
	else if (smallF && !largeF)
		{
		translation_shift = configSP->translation_stepA[0];
		}
	else
		{
		translation_shift = configSP->translation_stepA[2];
		}
	}
else
	{
	if (largeF && !smallF)
		{
		translation_shift = configSP->translation_stepA[3];
		}
	else if (smallF && !largeF)
		{
		translation_shift = configSP->translation_stepA[1];
		}
	else if (smallF && largeF)
		{
		translation_shift = configSP->translation_stepA[0];
		}
	else
		{
		translation_shift = configSP->translation_stepA[2];
		}
	}

/* Take care for the sign: */
translation_shift *= sign;

/* Return the translation shift: */
return translation_shift;
}

/*===========================================================================*/


