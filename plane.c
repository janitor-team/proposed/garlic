/* Copyright (C) 2000, 2001 Damir Zucic */

/*=============================================================================

				plane.c

Purpose:
	Execute plane command.

Input:
	(1) Pointer to MolComplexS structure.
	(2) The number of macromolecular complexes.
	(3) Pointer to RuntimeS structure.
	(4) Pointer to ConfigS structure.
	(5) Pointer to GUIS structure.
	(6) Pointer to NearestAtomS structure.
	(7) The number of pixels in the main window free area.
	(8) Pointer to refreshI.
	(9) Pointer to the remainder of the command string. It may be empty
	    or it may contain the keyword  OFF,  TRA (TRANSPARENCY) or  RAD
	    (RADIUS).

Output:
	(1) Plane flag set,  transparency  changed  or radius set  for each
	    caught macromolecular complex.
	(2) Return value.

Return value:
	(1) Positive (command) code on success.
	(2) Negative (error) code on failure.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>
#include <ctype.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "commands.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

char		*ExtractToken_ (char *, int, char *, char *);
size_t		MainRefresh_ (MolComplexS *, int,
			      RuntimeS *, ConfigS *, GUIS *,
			      NearestAtomS *, size_t, unsigned int);
int		ControlRefresh_ (MolComplexS *, ConfigS *, GUIS *);

/*======execute plane command:===============================================*/

int Plane_ (MolComplexS *mol_complexSP, int mol_complexesN,
	    RuntimeS *runtimeSP, ConfigS *configSP, GUIS *guiSP,
	    NearestAtomS *nearest_atomSP, size_t pixelsN,
	    unsigned int *refreshIP, char *stringP)
{
char		*remainderP;
char		tokenA[STRINGSIZE];
int		mol_complexI;
MolComplexS	*curr_mol_complexSP;
char		*P;
int		n;
double		value;

/* Extract the first token, if present: */
remainderP = ExtractToken_ (tokenA, STRINGSIZE, stringP, " \t\n");

/* If there are no tokens in the remainder of */
/* the command string, make plane(s) visible: */
if (!remainderP)
	{
	/* For each caught macromolecular complex, make the plane visible: */
	for (mol_complexI = 0; mol_complexI < mol_complexesN; mol_complexI++)
		{
		/* Pointer to the current macromolecular complex: */
		curr_mol_complexSP = mol_complexSP + mol_complexI;

		/* Check is the current macromolecular complex caught: */
		if (curr_mol_complexSP->catchF == 0) continue;

		/* Make plane visible (set flag): */
		curr_mol_complexSP->planeS.hiddenF = 0;
		}
	}

/* If keyword OFF is present, hide plane(s): */
else if (strstr (tokenA, "OFF") == tokenA)
	{
	/* For each caught macromolecular complex, hide the plane: */
	for (mol_complexI = 0; mol_complexI < mol_complexesN; mol_complexI++)
		{
		/* Pointer to the current macromolecular complex: */
		curr_mol_complexSP = mol_complexSP + mol_complexI;

		/* Check is the current macromolecular complex caught: */
		if (curr_mol_complexSP->catchF == 0) continue;

		/* Hide plane (set flag): */
		curr_mol_complexSP->planeS.hiddenF = 1;
		}
	}

/* If keyword TRA (TRANSPARENCY) is present, change the plane transparency: */
else if (strstr (tokenA, "TRA") == tokenA)
	{
	/* Replace each non-numeric character (except */
	/* minus sign and  decimal point) with space: */
	P = stringP;
	while ((n = *P++) != '\0')
		{
		if (!isdigit (n) && (n != '-') && (n != '.')) *(P - 1) = ' ';
		}

	/* Try to extract the plane transparency: */
	if (sscanf (stringP, "%lf", &value) != 1)
		{
		strcpy (runtimeSP->messageA,
			"Failed to extract the plane transparency!");
		runtimeSP->message_length = strlen (runtimeSP->messageA);
		return ERROR_PLANE;
		}

	/* Check the value; it should be between 0 and 1: */
	if ((value < 0.0) || (value > 1.0))
		{
		strcpy (runtimeSP->messageA,
			"The value should be between 0 and 1!");
		runtimeSP->message_length = strlen (runtimeSP->messageA);
		return ERROR_PLANE;
		}

	/* If this point is reached, the plane transparency is good: */
	for (mol_complexI = 0; mol_complexI < mol_complexesN; mol_complexI++)
		{
		/* Pointer to the current macromolecular complex: */
		curr_mol_complexSP = mol_complexSP + mol_complexI;

		/* Check is the current macromolecular complex caught: */
		if (curr_mol_complexSP->catchF == 0) continue;

		/* Set the transparency: */
		curr_mol_complexSP->planeS.transparency = value;
		}
	}

/* If keyword RAD (RADIUS) is present, change the circle radius: */
else if (strstr (tokenA, "RAD") == tokenA)
	{
	/* Replace each non-numeric character (except */
	/* minus sign and  decimal point) with space: */
	P = stringP;
	while ((n = *P++) != '\0')
		{
		if (!isdigit (n) && (n != '-') && (n != '.')) *(P - 1) = ' ';
		}

	/* Try to extract the circle radius: */
	if (sscanf (stringP, "%lf", &value) != 1)
		{
		strcpy (runtimeSP->messageA,
			"Failed to extract the plane radius!");
		runtimeSP->message_length = strlen (runtimeSP->messageA);
		return ERROR_PLANE;
		}

	/* Check the value; it should be positive: */
	if (value < 0.0)
		{
		strcpy (runtimeSP->messageA,
			"The value should be positive!");
		runtimeSP->message_length = strlen (runtimeSP->messageA);
		return ERROR_PLANE;
		}

	/* If this point is reached, the plane radius is good: */
	for (mol_complexI = 0; mol_complexI < mol_complexesN; mol_complexI++)
		{
		/* Pointer to the current macromolecular complex: */
		curr_mol_complexSP = mol_complexSP + mol_complexI;

		/* Check is the current macromolecular complex caught: */
		if (curr_mol_complexSP->catchF == 0) continue;

		/* Set the plane radius: */
		curr_mol_complexSP->planeS.circle_radius = value;

		/* Set the position_changedF, to force projection: */
		curr_mol_complexSP->position_changedF = 1;
		}
	}

/* Keyword not recognized: */
else
	{
	strcpy (runtimeSP->messageA, "Keyword not recognized!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return ERROR_PLANE;
	}

/* Refresh the main window: */
(*refreshIP)++;
MainRefresh_ (mol_complexSP, mol_complexesN,
	      runtimeSP, configSP, guiSP,
	      nearest_atomSP, pixelsN, *refreshIP);

/* Refresh the control window: */
ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI,
		 configSP, guiSP);

/* Return the command code: */
return COMMAND_PLANE;
}

/*===========================================================================*/


