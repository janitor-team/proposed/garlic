/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				pdb_id.c

Purpose:
	If the input line contains the keyword HEADER, extract the unique
	PDB identifier.  HEADER line should be  the first line of the PDB
	entry,  but  be careful - if your file is stored in  HTML format,
	the keyword  HEADER  may be pushed from the first position in the
	first nonempty line.  The unique  PDB identifier may be extracted
	from characters 63-66 in the HEADER line,  if the first character
	is indexed  with index one.  In the C language,  this means  that
	characters 62-65 should be copied.

Input:
	(1) Pointer to MolComplexS structure.
	(2) Input line.

Output:
	(1) If the line  is identified as  HEADER  line,  the unique  PDB
	    identifier may be found and copied to  MolComplexS structure.
	    data about C-alpha atoms.
	(2) Return value.

Return value:
	(1) Positive on success.
	(2) Negative on failure.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

void		ErrorMessage_ (char *, char *, char *,
			       char *, char *, char *, char *);

/*======try to extract the unique PDB identifier:============================*/

int PDBIdentifier_ (MolComplexS *mol_complexSP, char *stringP)
{
char		*P;
int		record_length;
int		first_char_pos = 62, last_char_pos = 65;
static char	codeA[PDBCODESIZE] = "????";

/* If line does not contain keyword HEADER, return negative value: */
P = strstr (stringP, "HEADER");
if (P == NULL) return -1;

/* Check the length of the line, counting from */
/* the first character of  the keyword HEADER: */
record_length = strlen (P);
if (record_length < last_char_pos + 1) return -2;

/* Extract the unique PDB identifier: */
strncpy (codeA, P + first_char_pos, PDBCODESIZE - 1);
codeA[PDBCODESIZE - 1] = '\0';
strcpy (mol_complexSP->unique_PDB_codeA, codeA);

/* Return positive value on success: */
return 1;
}

/*===========================================================================*/


