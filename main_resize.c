/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				main_resize.c

Purpose:
	Handle ConfigureNotify event for the main window.

Input:
	(1) Pointer to pointer to NearestAtomS structure.
	(2) Pointer to the number of pixels in the main window free area.
	(3) Pointer to MolComplexS structure, with macromol. complexes.
	(4) Number of macromolecular complexes.
	(5) Pointer to ConfigS structure, with configuration data.
	(6) Pointer to GUIS structure, with GUI data.
	(7) Pointer to XConfigureEvent structure.

Output:
	(1) Geometric parameters updated, buffers resized.
	(2) Return value.

Return value:
	(1) Positive on success.
	(2) Zero if nothing should be done.
	(3) Negative on failure.

========includes:============================================================*/

#include <stdio.h>
#include <stdlib.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

int		CalculateParameters_ (ConfigS *, GUIS *);
NearestAtomS	*AllocateNearest_ (size_t *, GUIS *);
void		InitNearest_ (NearestAtomS *, size_t);

/*======handle ConfigureNotify event for the main window:====================*/

int MainResize_ (NearestAtomS **nearest_atomSPP, size_t *pixelsNP,
		 MolComplexS *mol_complexSP, int mol_complexesN,
		 ConfigS *configSP, GUIS *guiSP,
		 XConfigureEvent *configure_eventSP)
{
int		old_width, old_height, new_width, new_height;
int		delta_width, delta_height;
int		n;
int		mol_complexI;

/* If this event was not generated for the main window, do nothing: */
if (configure_eventSP->window != guiSP->main_winS.ID) return 0;

/* Old main window width and height: */
old_width  = guiSP->main_winS.width;
old_height = guiSP->main_winS.height;

/* New width and height of the main window: */
guiSP->main_winS.width = configure_eventSP->width;
guiSP->main_winS.height = configure_eventSP->height;
new_width = guiSP->main_winS.width;
new_height = guiSP->main_winS.height;

/* Calculate differences: */
delta_width  = new_width  - old_width;
delta_height = new_height - old_height;

/* If nothing has changed, do nothing and return zero: */
if ((delta_width == 0) && (delta_height == 0)) return 0;

/* Update positions and dimensions of child windows: */
n = (int) guiSP->control_winS.x0 + delta_width;
if (n < 0) n = 0;
guiSP->control_winS.x0 = n;
n = (int) guiSP->input_winS.y0 + delta_height;
if (n < 0) n = 0;
guiSP->input_winS.y0 = n;
n = (int) guiSP->input_winS.width + delta_width;
if (n < 100) n = 100;
guiSP->input_winS.width = (unsigned int) n;
n = (int) guiSP->output_winS.x0 + delta_width;
if (n < 0) n = 0;
guiSP->output_winS.x0 = n;
n = (int) guiSP->output_winS.height + delta_height;
if (n < 50) n = 50;
guiSP->output_winS.height = (unsigned int) n;

/* Main window free area width and height: */
n = (int) guiSP->main_win_free_area_width + delta_width;
if (n < 0) n = 0;
guiSP->main_win_free_area_width  = (unsigned int) n;
n = (int) guiSP->main_win_free_area_height + delta_height;
if (n < 0) n = 0;
guiSP->main_win_free_area_height = (unsigned int) n;

/* Refresh calculated parameters: */
if (CalculateParameters_ (configSP, guiSP) < 0) return -1;

/* Resize input and output window: */
XResizeWindow (guiSP->displaySP, guiSP->input_winS.ID,
	       guiSP->input_winS.width, guiSP->input_winS.height);
XResizeWindow (guiSP->displaySP, guiSP->output_winS.ID,
	       guiSP->output_winS.width, guiSP->output_winS.height);

/* Resize the main hidden pixmap: */
if ((guiSP->main_hidden_pixmapF) && (guiSP->main_win_free_area_height > 0))
	{
	XFreePixmap (guiSP->displaySP, guiSP->main_hidden_pixmapID);
	guiSP->main_hidden_pixmapID = XCreatePixmap (
					guiSP->displaySP,
					DefaultRootWindow (guiSP->displaySP),
					guiSP->main_win_free_area_width,
					guiSP->main_win_free_area_height,
					guiSP->depth);
	}

/* Resize the NearestAtomS array: */
if (*nearest_atomSPP)
	{
	free (*nearest_atomSPP);
	*nearest_atomSPP = AllocateNearest_ (pixelsNP, guiSP);
	InitNearest_ (*nearest_atomSPP, *pixelsNP);
	}

/* Set the position_changedF flag for each macromolecular complex: */
for (mol_complexI = 0; mol_complexI < mol_complexesN; mol_complexI++)
	{
	(mol_complexSP + mol_complexI)->position_changedF = 1;
	}

/* Return positive value: */
return 1;
}

/*===========================================================================*/


