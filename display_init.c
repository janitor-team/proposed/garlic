/* Copyright (C) 2000-2002 Damir Zucic */

/*=============================================================================

				display_init.c

Purpose:
	Display initializations:  connect to X server, get default screen ID
	and screen size.  In addition,  prepare the three graphics contexts.

Input:
	(1) Pointer to GUIS structure (with GUI data).

Output:
	(1) Data stored to GUIS structure.
	(2) Return value.

Return value:
	(1) Positive on success.
	(2) Negative on failure (if attempt to connect to X server fails).

=============================================================================*/

#include <stdio.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======initialize display:==================================================*/

int DisplayInit_ (GUIS *guiSP)
{

/* Connect to X server: */
if ((guiSP->displaySP = XOpenDisplay (guiSP->display_nameP)) == NULL)
	{
	return -1;
	}

/* Default screen for given display: */
guiSP->screenID = DefaultScreen (guiSP->displaySP);

/* Default screen size: */
guiSP->screen_width = DisplayWidth (guiSP->displaySP, guiSP->screenID);
guiSP->screen_height = DisplayHeight (guiSP->displaySP, guiSP->screenID);

/* Ten graphics contexts: */
guiSP->theGCA[0] = XCreateGC (guiSP->displaySP,
			      DefaultRootWindow (guiSP->displaySP),
			      0, NULL);
guiSP->theGCA[1] = XCreateGC (guiSP->displaySP,
			      DefaultRootWindow (guiSP->displaySP),
			      0, NULL);
guiSP->theGCA[2] = XCreateGC (guiSP->displaySP,
			      DefaultRootWindow (guiSP->displaySP),
			      0, NULL);
guiSP->theGCA[3] = XCreateGC (guiSP->displaySP,
			      DefaultRootWindow (guiSP->displaySP),
			      0, NULL);
guiSP->theGCA[4] = XCreateGC (guiSP->displaySP,
			      DefaultRootWindow (guiSP->displaySP),
			      0, NULL);
guiSP->theGCA[5] = XCreateGC (guiSP->displaySP,
			      DefaultRootWindow (guiSP->displaySP),
			      0, NULL);
guiSP->theGCA[6] = XCreateGC (guiSP->displaySP,
			      DefaultRootWindow (guiSP->displaySP),
			      0, NULL);
guiSP->theGCA[7] = XCreateGC (guiSP->displaySP,
			      DefaultRootWindow (guiSP->displaySP),
			      0, NULL);
guiSP->theGCA[8] = XCreateGC (guiSP->displaySP,
			      DefaultRootWindow (guiSP->displaySP),
			      0, NULL);
guiSP->theGCA[9] = XCreateGC (guiSP->displaySP,
			      DefaultRootWindow (guiSP->displaySP),
			      0, NULL);

guiSP->gca_createdF = 1;

/* Return positive value on success: */
return 1;
}

/*===========================================================================*/


