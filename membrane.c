/* Copyright (C) 2001-2003 Damir Zucic */

/*=============================================================================

				membrane.c

Purpose:
	Execute membrane command.

Input:
	(1) Pointer to MolComplexS structure.
	(2) The number of macromolecular complexes.
	(3) Pointer to RuntimeS structure.
	(4) Pointer to ConfigS structure.
	(5) Pointer to GUIS structure.
	(6) Pointer to NearestAtomS structure.
	(7) The number of pixels in the main window free area.
	(8) Pointer to refreshI.
	(9) Pointer to the remainder of the command string. It may be empty
	    or it  may contain  the keyword  OFF,  TRA (TRANSPARENCY),  RAD
	    (RADIUS), THI (THICKNESS) or BET (BETA).

Output:
	(1) The membrane flag set.
	(2) Return value.

Return value:
	(1) Positive (command) code on success.
	(2) Negative (error) code on failure.

Notes:
	(1) By default, an alpha helix bundle protein is expected. This may
	    be changed by using the keyword BETA. In that case, garlic will
	    expect a beta barrel protein.  A different method  will be used
	    to place the membrane with respect to the protein.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>
#include <ctype.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "commands.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

char		*ExtractToken_ (char *, int, char *, char *);
void		InformUser_ (GUIS *, char *);
int		AlphaMembrane_ (MolComplexS *, RuntimeS *, ConfigS *, GUIS *);
int		BetaMembrane_ (MolComplexS *, RuntimeS *, ConfigS *, GUIS *);
size_t		MainRefresh_ (MolComplexS *, int,
			      RuntimeS *, ConfigS *, GUIS *,
			      NearestAtomS *, size_t, unsigned int);
int		ControlRefresh_ (MolComplexS *, ConfigS *, GUIS *);

/*======execute membrane command:============================================*/

int Membrane_ (MolComplexS *mol_complexSP, int mol_complexesN,
	       RuntimeS *runtimeSP, ConfigS *configSP, GUIS *guiSP,
	       NearestAtomS *nearest_atomSP, size_t pixelsN,
	       unsigned int *refreshIP, char *stringP)
{
char		*remainderP;
char		tokenA[STRINGSIZE];
int		mol_complexI;
MolComplexS	*curr_mol_complexSP;
char		*P;
int		n;
double		value;

/* Extract the first token, if present: */
remainderP = ExtractToken_ (tokenA, STRINGSIZE, stringP, " \t\n");

/*---------------------------------------------------------------------------*/

/* If there are no tokens in the remainder of the command string, make the */
/* membrane visible and find the position of  the membrane with respect to */
/* the structure.  The same job should be done  for each caught structure. */
/* In this case, only membrane proteins of helix bundle type are expected! */
if (!remainderP)
	{
	/* The sliding window width should be at least seven: */
	if (runtimeSP->sliding_window_width < 7)
		{
		strcpy (runtimeSP->messageA,
			"The sliding window should contain");
		strcat (runtimeSP->messageA, " at least seven residues!");
		runtimeSP->message_length = strlen (runtimeSP->messageA);
		return ERROR_MEMBRANE;
		}

	/* Scan the array of macromolecular complexes: */
	for (mol_complexI = 0; mol_complexI < mol_complexesN; mol_complexI++)
		{
		/* Pointer to the current macromolecular complex: */
		curr_mol_complexSP = mol_complexSP + mol_complexI;

		/* Check is the current macromolecular complex caught: */
		if (curr_mol_complexSP->catchF == 0) continue;

		/* Set the flag which says that the membrane is defined: */
		curr_mol_complexSP->membraneS.definedF = 1;

		/* Make the membrane visible: */
		curr_mol_complexSP->membraneS.hiddenF = 0;

		/* Inform user that the following function may be slow: */
		InformUser_ (guiSP,
			     "This may take some time, please be patient!");

		/* Find the position of the membrane: */
		n = AlphaMembrane_ (curr_mol_complexSP,
				    runtimeSP, configSP, guiSP);

		/* Set flags on success,  write */
		/* an error message on failure: */
		if (n > 0)
			{
			/* Set  the flag  which says */
			/* that membrane is defined: */
			curr_mol_complexSP->membraneS.definedF = 1;

			/* Make the membrane visible: */
			curr_mol_complexSP->membraneS.hiddenF  = 0;
			}
		else
			{
			/* Reset flags: */
			curr_mol_complexSP->membraneS.definedF = 0;
			curr_mol_complexSP->membraneS.hiddenF  = 1;

			/* Write error message: */
			strcpy (runtimeSP->messageA,
				"Unable to prepare the membrane position!");
			runtimeSP->message_length =
				strlen (runtimeSP->messageA);

			/* Refresh the main window: */
			(*refreshIP)++;
			MainRefresh_ (mol_complexSP, mol_complexesN,
				      runtimeSP, configSP, guiSP,
				      nearest_atomSP, pixelsN, *refreshIP);

			/* Refresh the control window: */
			ControlRefresh_ (mol_complexSP +
					 runtimeSP->default_complexI,
					 configSP, guiSP);

			return ERROR_MEMBRANE;
			}
		}
	}

/*---------------------------------------------------------------------------*/

/* If keyword OFF is present, hide membrane(s): */
else if (strstr (tokenA, "OFF") == tokenA)
	{
	/* For each caught macromolecular complex, hide the membrane: */
	for (mol_complexI = 0; mol_complexI < mol_complexesN; mol_complexI++)
		{
		/* Pointer to the current macromolecular complex: */
		curr_mol_complexSP = mol_complexSP + mol_complexI;

		/* Check is the current macromolecular complex caught: */
		if (curr_mol_complexSP->catchF == 0) continue;

		/* Hide the membrane: */
		curr_mol_complexSP->membraneS.hiddenF = 1;
		}
	}

/*---------------------------------------------------------------------------*/

/* If keyword TRA (TRANSPARENCY) is present, change membrane transparency: */
else if (strstr (tokenA, "TRA") == tokenA)
	{
	/* Replace each non-numeric character (except */
	/* minus sign and  decimal point) with space: */
	P = stringP;
	while ((n = *P++) != '\0')
		{
		if (!isdigit (n) && (n != '-') && (n != '.')) *(P - 1) = ' ';
		}

	/* Try to extract the membrane transparency: */
	if (sscanf (stringP, "%lf", &value) != 1)
		{
		strcpy (runtimeSP->messageA,
			"Failed to extract the membrane transparency!");
		runtimeSP->message_length = strlen (runtimeSP->messageA);
		return ERROR_MEMBRANE;
		}

	/* Check the value; it should be between 0 and 1: */
	if ((value < 0.0) || (value > 1.0))
		{
		strcpy (runtimeSP->messageA,
			"The value should be between 0 and 1!");
		runtimeSP->message_length = strlen (runtimeSP->messageA);
		return ERROR_MEMBRANE;
		}

	/* If this point is reached, the membrane transparency is good: */
	for (mol_complexI = 0; mol_complexI < mol_complexesN; mol_complexI++)
		{
		/* Pointer to the current macromolecular complex: */
		curr_mol_complexSP = mol_complexSP + mol_complexI;

		/* Check is the current macromolecular complex caught: */
		if (curr_mol_complexSP->catchF == 0) continue;

		/* Set the membrane transparency: */
		curr_mol_complexSP->membraneS.plane1S.transparency = value;
		curr_mol_complexSP->membraneS.plane2S.transparency = value;
		}
	}

/*---------------------------------------------------------------------------*/

/* If keyword RAD (RADIUS) is present, change the circle radius: */
else if (strstr (tokenA, "RAD") == tokenA)
	{
	/* Replace each non-numeric character (except */
	/* minus sign and  decimal point) with space: */
	P = stringP;
	while ((n = *P++) != '\0')
		{
		if (!isdigit (n) && (n != '-') && (n != '.')) *(P - 1) = ' ';
		}

	/* Try to extract the circle radius: */
	if (sscanf (stringP, "%lf", &value) != 1)
		{
		strcpy (runtimeSP->messageA,
			"Failed to extract the membrane radius!");
		runtimeSP->message_length = strlen (runtimeSP->messageA);
		return ERROR_MEMBRANE;
		}

	/* Check the value; it should be positive: */
	if (value < 0.0)
		{
		strcpy (runtimeSP->messageA,
			"The value should be positive!");
		runtimeSP->message_length = strlen (runtimeSP->messageA);
		return ERROR_MEMBRANE;
		}

	/* If this point is reached, the membrane radius is good: */
	for (mol_complexI = 0; mol_complexI < mol_complexesN; mol_complexI++)
		{
		/* Pointer to the current macromolecular complex: */
		curr_mol_complexSP = mol_complexSP + mol_complexI;

		/* Check is the current macromolecular complex caught: */
		if (curr_mol_complexSP->catchF == 0) continue;

		/* Set the membrane radius: */
		curr_mol_complexSP->membraneS.plane1S.circle_radius = value;
		curr_mol_complexSP->membraneS.plane2S.circle_radius = value;

		/* Set the position_changedF, to force projection: */
		curr_mol_complexSP->position_changedF = 1;
		}
	}

/*---------------------------------------------------------------------------*/

/* If keyword THI (THICKNESS) is present, change the membrane thickness: */
else if (strstr (tokenA, "THI") == tokenA)
	{
	/* Replace each non-numeric character (except */
	/* minus sign and  decimal point) with space: */
	P = stringP;
	while ((n = *P++) != '\0')
		{
		if (!isdigit (n) && (n != '-') && (n != '.')) *(P - 1) = ' ';
		}

	/* Try to extract the membrane thickness: */
	if (sscanf (stringP, "%lf", &value) != 1)
		{
		strcpy (runtimeSP->messageA,
			"Failed to extract the membrane thickness!");
		runtimeSP->message_length = strlen (runtimeSP->messageA);
		return ERROR_MEMBRANE;
		}

	/* Check the value; it should be positive and non-zero: */
	if (value <= 0.0)
		{
		strcpy (runtimeSP->messageA, "The value should be positive!");
		runtimeSP->message_length = strlen (runtimeSP->messageA);
		return ERROR_MEMBRANE;
		}

	/* If this point is reached, the membrane thickness is good: */
	for (mol_complexI = 0; mol_complexI < mol_complexesN; mol_complexI++)
		{
		/* Pointer to the current macromolecular complex: */
		curr_mol_complexSP = mol_complexSP + mol_complexI;

		/* Check is the current macromolecular complex caught: */
		if (curr_mol_complexSP->catchF == 0) continue;

		/* Set the membrane thickness: */
		curr_mol_complexSP->membraneS.thickness = value;

		/* Set the position_changedF, to force projection: */
		curr_mol_complexSP->position_changedF = 1;
		}
	}

/*---------------------------------------------------------------------------*/

/* If keyword BET (BETA) is present, a beta barrel protein is expected: */
else if (strstr (tokenA, "BET") == tokenA)
	{
	/* The sliding window width should be at least five: */
	if (runtimeSP->sliding_window_width < 5)
		{
		strcpy (runtimeSP->messageA,
			"The sliding window should contain");
		strcat (runtimeSP->messageA, " at least five residues!");
		runtimeSP->message_length = strlen (runtimeSP->messageA);
		return ERROR_MEMBRANE;
		}

	/* Scan the array of macromolecular complexes: */
	for (mol_complexI = 0; mol_complexI < mol_complexesN; mol_complexI++)
		{
		/* Pointer to the current macromolecular complex: */
		curr_mol_complexSP = mol_complexSP + mol_complexI;

		/* Check is the current macromolecular complex caught: */
		if (curr_mol_complexSP->catchF == 0) continue;

		/* Set the flag which says that the membrane is defined: */
		curr_mol_complexSP->membraneS.definedF = 1;

		/* Make the membrane visible: */
		curr_mol_complexSP->membraneS.hiddenF = 0;

		/* Inform user that the following function may be slow: */
		InformUser_ (guiSP,
			     "This may take some time, please be patient!");

		/* Find the position of the membrane: */
		n = BetaMembrane_ (curr_mol_complexSP,
				   runtimeSP, configSP, guiSP);

		/* Set flags on success,  write */
		/* an error message on failure: */
		if (n > 0)
			{
			/* Set  the flag  which says */
			/* that membrane is defined: */
			curr_mol_complexSP->membraneS.definedF = 1;

			/* Make the membrane visible: */
			curr_mol_complexSP->membraneS.hiddenF  = 0;
			}
		else
			{
			/* Reset flags: */
			curr_mol_complexSP->membraneS.definedF = 0;
			curr_mol_complexSP->membraneS.hiddenF  = 1;

			/* Write error message: */
			strcpy (runtimeSP->messageA,
				"Unable to prepare the membrane position!");
			runtimeSP->message_length =
				strlen (runtimeSP->messageA);

			/* Refresh the main window: */
			(*refreshIP)++;
			MainRefresh_ (mol_complexSP, mol_complexesN,
				      runtimeSP, configSP, guiSP,
				      nearest_atomSP, pixelsN, *refreshIP);

			/* Refresh the control window: */
			ControlRefresh_ (mol_complexSP +
					 runtimeSP->default_complexI,
					 configSP, guiSP);

			return ERROR_MEMBRANE;
			}
		}
	}

/*---------------------------------------------------------------------------*/

/* Keyword not recognized: */
else
	{
	strcpy (runtimeSP->messageA, "Keyword not recognized!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return ERROR_MEMBRANE;
	}

/*---------------------------------------------------------------------------*/

/* Refresh the main window: */
(*refreshIP)++;
MainRefresh_ (mol_complexSP, mol_complexesN,
	      runtimeSP, configSP, guiSP,
	      nearest_atomSP, pixelsN, *refreshIP);

/* Refresh the control window: */
ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI,
		 configSP, guiSP);

/* Return the command code: */
return COMMAND_MEMBRANE;
}

/*===========================================================================*/


