/* Copyright (C) 2001, 2002 Damir Zucic */

/*=============================================================================

				extract_cdxe.c

Purpose:
	Extract CD and XE coordinates for a given residue.  The symbol XE
        stands for NE in ARG and for CE in LYS. This function is not able
	to handle non-standard residues.

Input:
	(1) Pointer to VectorS structure, for CD coordinates.
	(2) Pointer to VectorS structure, for XE coordinates.
	(3) Pointer to AtomS structure,  pointing to the first element of
	    the atomic array.
	(4) Index of the first atom of a given residue.
	(5) Index of the last atom of a given residue.

Output:
	(1) VectorS structures filled with data.
	(2) Return value.

Return value:
	(1) The number of successfully extracted vectors  (at least zero,
	    at most two).

Notes:
	(1) Some files contain more than one entry for some atoms. Garlic
	    uses only the first entry and  discards other entries for the
	    same atom.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======extract CD and XE coordinates:=======================================*/

int ExtractCDXE_ (VectorS  *CD_vectorSP, VectorS *XE_vectorSP,
		  AtomS *atomSP, size_t atom_startI, size_t atom_endI)
{
int		vectors_extractedN = 0;
size_t		atomI;
AtomS		*curr_atomSP;
char		*atom_nameP;
int		CD_foundF = 0, XE_foundF = 0;

/* Scan the given residue: */
for (atomI = atom_startI; atomI <= atom_endI; atomI++)
	{
	/* Pointer to the current atom: */
	curr_atomSP = atomSP + atomI;

	/* Pointer to the purified atom name: */
	atom_nameP = curr_atomSP->raw_atomS.pure_atom_nameA;

	/* CD: */
	if (strcmp (atom_nameP, "CD") == 0)
		{
		if (CD_foundF) continue;
		CD_vectorSP->x = curr_atomSP->raw_atomS.x[0];
		CD_vectorSP->y = curr_atomSP->raw_atomS.y;
		CD_vectorSP->z = curr_atomSP->raw_atomS.z[0];
		vectors_extractedN++;
		CD_foundF = 1;
		}

	/* XE: */
	else if ((strcmp (atom_nameP, "NE") == 0) ||
		 (strcmp (atom_nameP, "CE") == 0))
		{
		if (XE_foundF) continue;
		XE_vectorSP->x = curr_atomSP->raw_atomS.x[0];
		XE_vectorSP->y = curr_atomSP->raw_atomS.y;
		XE_vectorSP->z = curr_atomSP->raw_atomS.z[0];
		vectors_extractedN++;
		XE_foundF = 1;
		}
	}

/* Return the number of extracted vectors: */
return vectors_extractedN;
}

/*===========================================================================*/


