/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				input_refresh.c

Purpose:
	Refresh the input window.

Input:
	(1) Pointer to GUIS structure.
	(2) Pointer to RuntimeS structure, with command strings.

Output:
	(1) Input window redrawn.
	(2) Return value.

Return value:
	(1) Positive always.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======refresh the input window:============================================*/

int InputRefresh_ (GUIS *guiSP, RuntimeS *runtimeSP)
{
int			comm_length, carriage_pos, the_rest;
char			left_partA[COMMSTRINGSIZE];
char			right_partA[COMMSTRINGSIZE];
int			full_left_part_width, left_part_width, max_width;
int			i;
static char		*left_partP;
int			left_part_length;
int			x0, y0, y1, y2;

/* Refresh the input window: */
XSetForeground (guiSP->displaySP, guiSP->theGCA[0],
		guiSP->input_winS.bg_colorID);
XFillRectangle (guiSP->displaySP, guiSP->input_winS.ID, guiSP->theGCA[0],
		0, 0, guiSP->input_winS.width, guiSP->input_winS.height);

/* Prepare the text color: */
XSetForeground (guiSP->displaySP, guiSP->theGCA[0],
		guiSP->output_winS.fg_colorID);

/* Draw the current message (this is often empty): */
x0 = TEXTMARGIN;
y0 = guiSP->input_winS.height -
     guiSP->input_winS.text_line_height -
     guiSP->input_winS.half_font_height;
XDrawString (guiSP->displaySP, guiSP->input_winS.ID, guiSP->theGCA[0],
	     x0, y0,
	     runtimeSP->messageA, runtimeSP->message_length);

/* Copy the command string length and carriage position: */
comm_length  = runtimeSP->command_length;
carriage_pos = runtimeSP->carriage_position;

/* Prepare the left and right part of the command string: */
strncpy (left_partA, runtimeSP->curr_commandA, carriage_pos);
left_partA[carriage_pos] = '\0';
the_rest = comm_length - carriage_pos;
strncpy (right_partA, runtimeSP->curr_commandA + carriage_pos, the_rest);
right_partA[the_rest] = '\0';

/* Prepare the left part width: */
full_left_part_width = XTextWidth (guiSP->main_winS.fontSP,
				   left_partA, carriage_pos) + TEXTMARGIN;

/* Initialize the pointer to the left part: */
left_partP = left_partA;

/* The maximal allowed width  of the left */
/* part (some space left for the cursor): */
max_width = (int) guiSP->input_winS.width - 3;

/* If the left part is too wide, skip some characters: */
if (full_left_part_width > max_width)
	{
	for (i = 0; i < COMMSTRINGSIZE - 1; i++)
		{
		left_part_width = full_left_part_width -
				  runtimeSP->left_part_widthA[i];
		if (left_part_width <= max_width)
			{
			left_partP = left_partA + i + 1;
			break;
			}
		}
	}
else left_part_width = full_left_part_width;

/* The length of the remainder: */
left_part_length = strlen (left_partP);

/* Draw the left part to the bottom of input window: */
y0 = guiSP->input_winS.height - guiSP->input_winS.half_font_height;
XDrawString (guiSP->displaySP, guiSP->input_winS.ID, guiSP->theGCA[0],
	     x0, y0, left_partP, left_part_length);

/* Draw the keyboard cursor (showing the "carriage" position): */
x0 = left_part_width + 1;
y1 = guiSP->input_winS.height -
     guiSP->input_winS.text_line_height -
     guiSP->input_winS.quarter_font_height;
y2 = guiSP->input_winS.height - 2;
XDrawLine (guiSP->displaySP, guiSP->input_winS.ID, guiSP->theGCA[0],
	   x0, y1, x0, y2);
XDrawLine (guiSP->displaySP, guiSP->input_winS.ID, guiSP->theGCA[0],
	   x0 + 1, y1, x0 + 1, y2);

/* Draw the right part of command string: */
x0 = left_part_width + 3;
XDrawString (guiSP->displaySP, guiSP->input_winS.ID, guiSP->theGCA[0],
	     x0, y0, right_partA, strlen (right_partA));

/* Return positive value on success: */
return 1;
}

/*===========================================================================*/


