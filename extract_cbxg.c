/* Copyright (C) 2001, 2002 Damir Zucic */

/*=============================================================================

				extract_cbxg.c

Purpose:
	Extract CB and XG coordinates for a given residue.  The symbol XG
        stands for  CG, CG1, OG, OG1 or SG.  The  CG atom appears in PRO,
	LEU, MET, PHE, TRP, ASN, GLN, TYR, ASP, GLU,  LYS, ARG  and  HIS.
        The CG1 atom appears in  ILE,  OG in SER,  OG1 in THR  and  SG in
        CYS.  This function is not able  to handle non-standard residues.

Input:
	(1) Pointer to VectorS structure, for CB coordinates.
	(2) Pointer to VectorS structure, for XG coordinates.
	(3) Pointer to AtomS structure,  pointing to the first element of
	    the atomic array.
	(4) Index of the first atom of a given residue.
	(5) Index of the last atom of a given residue.

Output:
	(1) VectorS structures filled with data.
	(2) Return value.

Return value:
	(1) The number of successfully extracted vectors  (at least zero,
	    at most two).

Notes:
	(1) Some files contain more than one entry for some atoms. Garlic
	    uses only the first entry and  discards other entries for the
	    same atom.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======extract CB and XG coordinates:=======================================*/

int ExtractCBXG_ (VectorS  *CB_vectorSP, VectorS *XG_vectorSP,
		  AtomS *atomSP, size_t atom_startI, size_t atom_endI)
{
int		vectors_extractedN = 0;
size_t		atomI;
AtomS		*curr_atomSP;
char		*atom_nameP;
int		CB_foundF = 0, XG_foundF = 0;

/* Scan the given residue: */
for (atomI = atom_startI; atomI <= atom_endI; atomI++)
	{
	/* Pointer to the current atom: */
	curr_atomSP = atomSP + atomI;

	/* Pointer to the purified atom name: */
	atom_nameP = curr_atomSP->raw_atomS.pure_atom_nameA;

	/* CB: */
	if (strcmp (atom_nameP, "CB") == 0)
		{
		if (CB_foundF) continue;
		CB_vectorSP->x = curr_atomSP->raw_atomS.x[0];
		CB_vectorSP->y = curr_atomSP->raw_atomS.y;
		CB_vectorSP->z = curr_atomSP->raw_atomS.z[0];
		vectors_extractedN++;
		CB_foundF = 1;
		}

	/* XG: */
	else if ((strcmp (atom_nameP, "CG" ) == 0) ||
		 (strcmp (atom_nameP, "CG1") == 0) ||
		 (strcmp (atom_nameP, "OG" ) == 0) ||
		 (strcmp (atom_nameP, "OG1") == 0) ||
		 (strcmp (atom_nameP, "SG" ) == 0))
		{
		if (XG_foundF) continue;
		XG_vectorSP->x = curr_atomSP->raw_atomS.x[0];
		XG_vectorSP->y = curr_atomSP->raw_atomS.y;
		XG_vectorSP->z = curr_atomSP->raw_atomS.z[0];
		vectors_extractedN++;
		XG_foundF = 1;
		}
	}

/* Return the number of extracted vectors: */
return vectors_extractedN;
}

/*===========================================================================*/


