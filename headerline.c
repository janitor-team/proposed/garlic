/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				headerline.c

Purpose:
	Check the input line;  if recognized  as mandatory record,  copy
	the line to HeaderS structure,  which is  member of  MolComplexS
	structure.  The trailing newline is removed (if present at all).
	The PDB header contains,  among others, a keyword called HEADER.
	Don't get confused with the usage of word "header"!

Input:
	(1) Pointer to HeaderS structure,  where line should be added if
	    recognized as mandatory record.
	(2) Input line.
	(3) The first character in the line.

Output:
	(1) If line is mandatory, it will be added to HeaderS structure.
	(2) Return value.

Return value:
	(1) Positive if line added to HeaderS structure.
	(2) Zero if line does not contain mandatory record.
	(3) Negative  if line does  contain  mandatory record,  but  the
	    maximal number of  records of this type is  reached already.

========includes:============================================================*/

#include <stdio.h>
#include <string.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======check line, copy it if contains mandatory record:====================*/

int CopyHeaderLine_ (HeaderS *hSP, char *lineP, int first_char)
{
char		*P;

P = hSP->dataP;

/* Check is there a mandatory record in input line: */
switch (first_char)
	{
	case 'H':
		if (strstr (lineP, "HEADER") != lineP) return 0;
		if (hSP->header_linesN >= MAXHEADERLINES) return -1;
		hSP->header_linesN++;
		P += hSP->header_offset + hSP->header_linesN * HEADERLINESIZE;
		break;
	case 'T':
		if (strstr (lineP, "TITLE")  != lineP) return 0;
		if (hSP->title_linesN  >= MAXTITLELINES)  return -2;
		hSP->title_linesN++;
		P += hSP->title_offset  + hSP->title_linesN  * HEADERLINESIZE; 
		break;
	case 'C':
		if (strstr (lineP, "COMPND") != lineP) return 0;
		if (hSP->compnd_linesN >= MAXCOMPNDLINES) return -3;
		hSP->compnd_linesN++;
		P += hSP->compnd_offset + hSP->compnd_linesN * HEADERLINESIZE;
		break;
	case 'S':
		if (strstr (lineP, "SOURCE") != lineP) return 0;
		if (hSP->source_linesN >= MAXSOURCELINES) return -4;
		hSP->source_linesN++;
		P += hSP->source_offset + hSP->source_linesN * HEADERLINESIZE;
		break;
	case 'E':
		if (strstr (lineP, "EXPDTA") != lineP) return 0;
		if (hSP->expdta_linesN >= MAXEXPDTALINES) return -5;
		hSP->expdta_linesN++;
		P += hSP->expdta_offset + hSP->expdta_linesN * HEADERLINESIZE;
		break;
	case 'A':
		if (strstr (lineP, "AUTHOR") != lineP) return 0;
		if (hSP->author_linesN >= MAXAUTHORLINES) return -6;
		hSP->author_linesN++;
		P += hSP->author_offset + hSP->author_linesN * HEADERLINESIZE;
		break;
	default:
		return 0;
	}

/* If this point is reached, line contains mandatory record - copy it: */
strncpy (P, lineP, HEADERLINESIZE - 1);
*(P + HEADERLINESIZE - 1) = '\0';

return 1;
}

/*===========================================================================*/


