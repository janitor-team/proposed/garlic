/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				wmhints.c

Purpose:
	Set window manager hints: icon information, the initial state of
	the window etc.

Input:
	Pointer to GUIS structure.

Output:
	(1) Some data stored to GUIS structure.

Return value:
	No return value.

Notes:
	(1) The function XSetWMHints is not called here  (as it was done
	    in versions 1.x and 1.0). This function is obsolete (X11R3).
	

========includes:============================================================*/

#include <stdio.h>
#include <string.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======set window manager hints:============================================*/

void SetWMHints_ (GUIS *guiSP)
{
guiSP->wm_hintsS.flags         = InputHint | StateHint | IconWindowHint;
guiSP->wm_hintsS.input         = True;
guiSP->wm_hintsS.initial_state = NormalState;
guiSP->wm_hintsS.icon_window   = guiSP->icon_winS.ID;
}

/*===========================================================================*/


