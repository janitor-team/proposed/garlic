/* Copyright (C) 2000-2003 Damir Zucic */

/*=============================================================================

				copy_sequence.c

Purpose:
	Copy sequence from the general purpose buffer to the reference
	buffer.

Input:
	(1) Pointer to RuntimeS structure.

Output:
	(1) Sequence copyed to reference buffer.
	(2) Return value.

Return value:
	(1) The number of residues in both buffers.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

void		InitHyphob_ (RuntimeS *);

/*======copy sequence to reference buffer:===================================*/

int CopySequence_ (RuntimeS *runtimeSP)
{
int		max_length, i;
size_t		residuesN, residueI;
char		*sourceP, *destP;

/* The maximal residue name length: */
max_length = RESNAMESIZE - 1;

/* Copy the number of residues: */
residuesN = runtimeSP->residuesN;
runtimeSP->reference_residuesN = residuesN;

/* Zero initialize the reference buffer: */
for (i = 0; i < (int) runtimeSP->sequence_buffer_size; i++)
	{
	*(runtimeSP->reference_sequenceP + i) = '\0';
	}

/* Scan the source sequence: */
for (residueI = 0; residueI < residuesN; residueI++)
	{
	/* Copy the residue name: */
	sourceP = runtimeSP->sequenceP + residueI * max_length;
	destP = runtimeSP->reference_sequenceP + residueI * max_length;
	strncpy (destP, sourceP, max_length);

	/* Copy the residue serial number: */
	*(runtimeSP->reference_serialIP + residueI) =
					*(runtimeSP->serialIP + residueI);
	}

/* Initialize the hydrophobicity values: */
InitHyphob_ (runtimeSP);

/* Return the number of residues in both buffers: */
return residuesN;
}

/*===========================================================================*/


