/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				init_slab.c

Purpose:
	Initialize slab data:  reference point (center) position, front
	and back surface distance from the center. This function is not
	very precise,  because it uses the complex spatial extent.  The
	available extent  is increased for 40%.  The reason is that the
	initial slab should show all atoms, even after the rotation for
	45 degrees.  As - at least  in theory - some  structure  may be
	planar,  tilted 45 degrees with respect  to z axis,  there is a
	chance that the actual  spatial extent is for a factor  sqrt(2)
	larger than  the calculated spatial extent.  The  40%  increase
	should  ensure that  all atoms will  be visible after rotation.
	See note 1.
	
Input:
	(1) Pointer to MolComplexS structure.

Output:
	(1) Slab data initialized (center position, surface positions).

Return value:
	No return value.

Notes:
	(1) Illustration of the problem with the spatial extent:
	           ______ 
	          |    **|
	          |  **  |    ... the molecule is positioned diagonally
                  |**    |        in the "extent box".
		   ------ 
	    The spatial extent of the molecule is larger than the width
	    and height of the "extent box".

	(2) Soluble proteins are globular (approx. spherical). Membrane
	    proteins usually look like cylinders. An asymmetric protein
	    with geometric center significantly closer to one edge will
	    cause this function to place slab surfaces at wrong places.
	    It may happen that some atoms  will be unvisible.  This can
	    be easily corrected  at runtime - just set the proper slab.

========includes:============================================================*/

#include <stdio.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======initialize slab:=====================================================*/

void InitSlab_ (MolComplexS *mol_complexSP)
{
double		initial_extent, half_initial_extent;

/* Initialize the reference point: */
mol_complexSP->slab_center_vectorS.x =
				mol_complexSP->geometric_center_vectorS.x;
mol_complexSP->slab_center_vectorS.y =
				mol_complexSP->geometric_center_vectorS.y;
mol_complexSP->slab_center_vectorS.z =
				mol_complexSP->geometric_center_vectorS.z;

/* Do the correction (add 40%; read the explanation above): */
initial_extent = 1.40 * mol_complexSP->max_extent;

/* Half of the corrected maximal extent: */
half_initial_extent = 0.5 * initial_extent;

/* Set the initial front and back surface distance */
/* from the center. This depends on the slab mode. */
switch (mol_complexSP->slab_modeI)
	{
	/** Slab off (slab not used, do nothing): **/
	case 0:
		break;

	/** Planar: **/
	case 1:
		mol_complexSP->slab_front_relative_position =
							-half_initial_extent;
		mol_complexSP->slab_back_relative_position  =
							 half_initial_extent;
		break;

	/** Sphere, half-sphere, cylinder and half-cylinder: **/
	case 2:
	case 3:
	case 4:
	case 5:
		mol_complexSP->slab_front_relative_position =
							 half_initial_extent;
		mol_complexSP->slab_back_relative_position  =  0.0;
		break;
	
	/** Unsupported slab modes (do nothing): **/
	default:
		;
	}
}

/*===========================================================================*/


