/* Copyright (C) 2000-2006 Damir Zucic */

/*=============================================================================

				font.c

Purpose:
	Execute font command: change font.

Input:
	(1) Pointer to MolComplexS structure, with macromol. complexes.
	(2) Pointer to the number of macromolecular complexes.
	(3) Pointer to RuntimeS structure, with some runtime data.
	(4) Pointer to ConfigS structure, with configuration data.
	(5) Pointer to GUIS structure, with GUI data.
	(6) Pointer to pointer to NearestAtomS structure.
	(7) Pointer to the number of pixels in the main window free area.
	(8) Pointer to refreshI.

Output:
	(1) Font changed.
	(2) Return value.

Return value:
	(1) Positive (command) code on success.
	(2) Negative (error) code on failure.

Notes:
	(1) The original command string should be used.

========includes:============================================================*/

#include <stdio.h>

#include <stdlib.h>
#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "commands.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

char		*ExtractToken_ (char *, int, char *, char *);
int		CalculateParameters_ (ConfigS *, GUIS *);
NearestAtomS	*AllocateNearest_ (size_t *, GUIS *);
void		InitNearest_ (NearestAtomS *, size_t);

/*======execute font command:================================================*/

int Font_ (MolComplexS *mol_complexSP, int mol_complexesN,
	   RuntimeS *runtimeSP, ConfigS *configSP, GUIS *guiSP,
	   NearestAtomS **nearest_atomSPP, size_t *pixelsNP,
	   unsigned int *refreshIP)
{
char			*remainderP;
char			tokenA[STRINGSIZE];
static XFontStruct	*fontSP;
int			n;
int			old_input_win_height, new_input_win_height;
int			delta_height;
int			mol_complexI;

/* Skip the first token: */
remainderP = ExtractToken_ (tokenA, STRINGSIZE,
			    runtimeSP->curr_commandA, " \t\n");
if (!remainderP) return ERROR_FONT;

/* The second token should contain the font name: */
remainderP = ExtractToken_ (tokenA, STRINGSIZE, remainderP, " \t\n");
if (!remainderP)
	{
	strcpy (runtimeSP->messageA, "Missing font name!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return ERROR_NO_FONT_NAME;
	}

/* Try to load the requested font: */
fontSP = XLoadQueryFont (guiSP->displaySP, tokenA);
if (fontSP == NULL)
	{
	sprintf (runtimeSP->messageA, "Font %s is not available!", tokenA);
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return ERROR_FONT;
	}

/* If this point is reached, copy the font to GUIS structure: */
guiSP->main_winS.fontSP = fontSP;

/* Set this font to all GC's: */
XSetFont(guiSP->displaySP, guiSP->theGCA[0], guiSP->main_winS.fontSP->fid);
XSetFont(guiSP->displaySP, guiSP->theGCA[1], guiSP->main_winS.fontSP->fid);
XSetFont(guiSP->displaySP, guiSP->theGCA[2], guiSP->main_winS.fontSP->fid);
XSetFont(guiSP->displaySP, guiSP->theGCA[3], guiSP->main_winS.fontSP->fid);
XSetFont(guiSP->displaySP, guiSP->theGCA[4], guiSP->main_winS.fontSP->fid);
XSetFont(guiSP->displaySP, guiSP->theGCA[5], guiSP->main_winS.fontSP->fid);
XSetFont(guiSP->displaySP, guiSP->theGCA[6], guiSP->main_winS.fontSP->fid);
XSetFont(guiSP->displaySP, guiSP->theGCA[7], guiSP->main_winS.fontSP->fid);
XSetFont(guiSP->displaySP, guiSP->theGCA[8], guiSP->main_winS.fontSP->fid);
XSetFont(guiSP->displaySP, guiSP->theGCA[9], guiSP->main_winS.fontSP->fid);

/* Initialize font heights: */
n = guiSP->main_winS.fontSP->ascent + guiSP->main_winS.fontSP->descent;

/** Main window: **/
guiSP->main_winS.quarter_font_height   = (n + 3) / 4;
guiSP->main_winS.half_font_height      = (n + 1) / 2;
guiSP->main_winS.font_height           = n;
guiSP->main_winS.text_line_height      = guiSP->main_winS.font_height +
                                         guiSP->main_winS.quarter_font_height;

/** Output window: **/
guiSP->output_winS.quarter_font_height = guiSP->main_winS.quarter_font_height;
guiSP->output_winS.half_font_height    = guiSP->main_winS.half_font_height;
guiSP->output_winS.font_height         = guiSP->main_winS.font_height;
guiSP->output_winS.text_line_height    = guiSP->main_winS.text_line_height;

/** Input window: **/
guiSP->input_winS.quarter_font_height  = guiSP->main_winS.quarter_font_height;
guiSP->input_winS.half_font_height     = guiSP->main_winS.half_font_height;
guiSP->input_winS.font_height          = guiSP->main_winS.font_height;
guiSP->input_winS.text_line_height     = guiSP->main_winS.text_line_height;

/* Change input window height and position: */
old_input_win_height = guiSP->input_winS.height;
new_input_win_height = 2 * guiSP->input_winS.text_line_height +
			   guiSP->input_winS.quarter_font_height;
delta_height = new_input_win_height - old_input_win_height;
guiSP->input_winS.height = (unsigned int) new_input_win_height;
n = (int) guiSP->input_winS.y0 - delta_height;
if (n < 0) n = 0;
guiSP->input_winS.y0 = n;

/* Change the main window free area height: */
n = (int) guiSP->main_win_free_area_height - delta_height;
if (n < 0) n = 0;
guiSP->main_win_free_area_height = (unsigned int) n;

/* Refresh calculated parameters: */
if (CalculateParameters_ (configSP, guiSP) < 0) return ERROR_FONT;

/* Resize and move the input window: */
XResizeWindow (guiSP->displaySP, guiSP->input_winS.ID,
	       guiSP->input_winS.width, guiSP->input_winS.height);
XMoveWindow (guiSP->displaySP, guiSP->input_winS.ID,
             guiSP->input_winS.x0, guiSP->input_winS.y0);

/* Resize the main hidden pixmap: */
if ((guiSP->main_hidden_pixmapF) && (guiSP->main_win_free_area_height > 0))
	{
	XFreePixmap (guiSP->displaySP, guiSP->main_hidden_pixmapID);
	guiSP->main_hidden_pixmapID = XCreatePixmap (
					guiSP->displaySP,
					DefaultRootWindow (guiSP->displaySP),
					guiSP->main_win_free_area_width,
					guiSP->main_win_free_area_height,
					guiSP->depth);
	}

/* Resize the NearestAtomS array: */
if (*nearest_atomSPP)
	{
	free (*nearest_atomSPP);
	*nearest_atomSPP = AllocateNearest_ (pixelsNP, guiSP);
	InitNearest_ (*nearest_atomSPP, *pixelsNP);
	}

/* Set the position_changedF flag for each macromolecular complex: */
for (mol_complexI = 0; mol_complexI < mol_complexesN; mol_complexI++)
	{
	(mol_complexSP + mol_complexI)->position_changedF = 1;
	}

/* Return positive value on success: */
return COMMAND_FONT;
}

/*===========================================================================*/


