/* Copyright (C) 2000, 2001 Damir Zucic */

/*=============================================================================

				dock.c

Purpose:
	Execute dock command.

Input:
	(1) Pointer to MolComplexS structure.
	(2) The number of macromolecular complexes.
	(3) Pointer to RuntimeS structure.
	(4) Pointer to ConfigS structure.
	(5) Pointer to GUIS structure.
	(6) Pointer to NearestAtomS structure.
	(7) The number of pixels in the main window free area.
	(8) Pointer to refreshI.
        (9) Pointer to the remainder of the command string.  This command
            may be given without keyword or with keyword OFF.


Output:
	(1) Switch to docking mode. Docking window mapped.
	(2) Return value.

Return value:
	(1) Positive (command) code on success.
	(2) Negative (error) code on failure.

Notes:
	(1) Docking window should be created if docking is called for the
	    first time. If docking is switched off, this window should be
	    hidden.

========includes:============================================================*/

#include <stdio.h>

#include <string.h>
#include <ctype.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "commands.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

char		*ExtractToken_ (char *, int, char *, char *);
int		AlignNormalVector_ (MolComplexS *, ConfigS *, int, int);
int		AlignComplexes_ (MolComplexS *, MolComplexS *, ConfigS *);
int		PrepareDockingWindow_ (RuntimeS *, GUIS *);
size_t		MainRefresh_ (MolComplexS *, int,
			      RuntimeS *, ConfigS *, GUIS *,
			      NearestAtomS *, size_t, unsigned int);
int		ControlRefresh_ (MolComplexS *, ConfigS *, GUIS *);
int		DockingRefresh_ (RuntimeS *, GUIS *);

/*======execute dock command:================================================*/

int Dock_ (MolComplexS *mol_complexSP, int mol_complexesN,
	   RuntimeS *runtimeSP, ConfigS *configSP, GUIS *guiSP,
	   NearestAtomS *nearest_atomSP, size_t pixelsN,
	   unsigned int *refreshIP, char *stringP)
{
char			*remainderP;
char			tokenA[STRINGSIZE];
char			*P;
int			n;
int			mol_complexI;
MolComplexS		*curr_mol_complexSP;
static int		complex1ID, complex2ID, curr_complexID;
int			complex1F = 0, complex2F = 0;
static MolComplexS	*mol_complex1SP, *mol_complex2SP;
unsigned int		width, height;
int			x0, y0;

/* Extract the first token, if present: */
remainderP = ExtractToken_ (tokenA, STRINGSIZE, stringP, " \t\n");

/* If there are no additional tokens in the remainder */
/* of the command string,  the command is incomplete: */
if (!remainderP)
        {
        strcpy (runtimeSP->messageA, "The command string is incomplete!");
        runtimeSP->message_length = strlen (runtimeSP->messageA);
        return ERROR_DOCK;
        }

/*------switch docking off:--------------------------------------------------*/

/* If keyword OFF is present, exit docking mode and hide docking window: */
else if (strstr (tokenA, "OFF") == tokenA)
        {
	/* Reset flag: */
	guiSP->dockingF = 0;

	/* Hide docking window if it was created and mapped: */
	if (guiSP->docking_window_createdF)
		{
		if (guiSP->docking_window_mappedF)
			{
			XUnmapWindow (guiSP->displaySP,
				      guiSP->docking_winS.ID);
			guiSP->docking_window_mappedF = 0;
			}
		}

	/* Reset molecular pointers: */
	mol_complex1SP = NULL;
	mol_complex2SP = NULL;

	/* Return the command code: */
	return COMMAND_DOCK;
        }

/*------read two complex identifiers:----------------------------------------*/

/* If this point is reached, keyword OFF was not found. */

/* Replace each non-numeric character (except */
/* minus sign and  decimal point) with space: */
P = stringP;
while ((n = *P++) != '\0')
	{
	if (!isdigit (n) && (n != '-') && (n != '.')) *(P - 1) = ' ';
	}

/* Try to extract two identifiers: */
if (sscanf (stringP, "%d %d", &complex1ID, &complex2ID) != 2)
	{
	strcpy (runtimeSP->messageA, "Failed to extract two identifiers!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return ERROR_DOCK;
	}

/* The identifiers should not be equal: */
if (complex1ID == complex2ID)
	{
	strcpy (runtimeSP->messageA, "The identifiers should not be equal!");
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return ERROR_DOCK;
	}

/* Check both identifiers (do they refer to available complexes): */
for (mol_complexI = 0; mol_complexI < mol_complexesN; mol_complexI++)
	{
	/* Pointer to the current macromolecular complex: */
	curr_mol_complexSP = mol_complexSP + mol_complexI;

	/* If there are no atoms in the current complex, ignore it: */
	if (curr_mol_complexSP->atomsN == 0) continue;

	/* Compare the identifier  of the current */
	/* complex with both complex identifiers: */
	curr_complexID = curr_mol_complexSP->mol_complexID;
	if (curr_complexID == complex1ID)
		{
		complex1F = 1;
		mol_complex1SP = curr_mol_complexSP;
		}
	else if (curr_complexID == complex2ID)
		{
		complex2F = 1;
		mol_complex2SP = curr_mol_complexSP;
		}
	}

/* If the first identifier refers to nonexisting complex: */
if ((complex1F == 0) && (complex2F == 1))
	{
	sprintf (runtimeSP->messageA,
		"The first identifier (%d) is bad!", complex1ID);
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return ERROR_DOCK;
	}

/* If the second identifier refers to nonexisting complex: */
else if ((complex1F == 1) && (complex2F == 0))
	{
	sprintf (runtimeSP->messageA,
		"The second identifier (%d) is bad!", complex2ID);
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return ERROR_DOCK;
	}

/* If both identifiers refer to nonexisting complexes: */
else if ((complex1F == 0) && (complex2F == 0))
	{
	sprintf (runtimeSP->messageA,
		"Both identifiers (%d, %d) are bad!", complex1ID, complex2ID);
	runtimeSP->message_length = strlen (runtimeSP->messageA);
	return ERROR_DOCK;
	}

/*------set docking flag:----------------------------------------------------*/

guiSP->dockingF = 1;

/*------store molecular pointers:--------------------------------------------*/

runtimeSP->mol_complex1SP = mol_complex1SP;
runtimeSP->mol_complex2SP = mol_complex2SP;

/*------bind planes (and other things) to structures:------------------------*/

mol_complex1SP->move_bits =	STRUCTURE_MASK |
				PLANE_MASK |
				MEMBRANE_MASK |
				ENVELOPE_MASK;
mol_complex2SP->move_bits =	STRUCTURE_MASK |
				PLANE_MASK |
				MEMBRANE_MASK |
				ENVELOPE_MASK;

/*------align normal vectors:------------------------------------------------*/

/* The normal vector of the first complex should be antiparallel to y axis: */
AlignNormalVector_ (mol_complex1SP, configSP, 2, -1);

/* The normal vector of the second complex should be parallel to y axis: */
AlignNormalVector_ (mol_complex2SP, configSP, 2, +1);

/*------align two structures vertically:-------------------------------------*/

AlignComplexes_ (mol_complex1SP, mol_complex2SP, configSP);

/*------resize the main window and move it to the upper left corner:---------*/

width = 2 * guiSP->screen_width / 3 - 40;
height = guiSP->screen_height - 45;
x0 = 10;
y0 = 30;
XResizeWindow (guiSP->displaySP, guiSP->main_winS.ID, width, height);
XMoveWindow (guiSP->displaySP, guiSP->main_winS.ID, x0, y0);

/*------create docking window if not created before:-------------------------*/

PrepareDockingWindow_ (runtimeSP, guiSP);

/*------refresh windows:-----------------------------------------------------*/

/* Refresh the main window: */
(*refreshIP)++;
MainRefresh_ (mol_complexSP, mol_complexesN,
	      runtimeSP, configSP, guiSP,
	      nearest_atomSP, pixelsN, *refreshIP);

/* Refresh the control window: */
ControlRefresh_ (mol_complexSP + runtimeSP->default_complexI,
		 configSP, guiSP);

/* Refresh docking window: */
DockingRefresh_ (runtimeSP, guiSP);

/* Return the command code: */
return COMMAND_DOCK;
}

/*===========================================================================*/


