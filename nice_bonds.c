/* Copyright (C) 2002 Damir Zucic */

/*=============================================================================

				nice_bonds.c

Purpose:
	Draw nice bonds (sort of spacefill style for bonds).  Half of each
	bond is drawn and  another half  is assigned to the partner.  Find
	the z value for each pixel.  A bond is visible  only if both atoms
	forming this bond are visible. Drawing direction: 1 -> 2.

Input:
	(1) Pointer to MolComplexS structure.
	(2) Number of macromolecular complexes.
	(3) Pointer to ConfigS structure.
	(4) Pointer to GUIS structure.
	(5) Pointer to NearestAtomS structure,  with information about the
	    atom occupying the given pixel.
	(6) The number of pixels in the main window free area.
	(7) The refreshI,  used to check the  NearestAtomS associated with
	    a given pixel.

Output:
	(1) Bonds drawn to the hidden pixmap.
	(2) Return value.

Return value:
	(1) The number of bonds drawn.

Notes:
	(1) Indentation is exceptionally 4 spaces.

	(2) Some geometric parameters  used to draw nice bonds are defined
	    in the figure below:

                       ++++++++                     .
                    +++        +++                  |\
                   +              +                 | \                 +
                  +                +                |  \                +
                  +       .________+____________    |   \              +
                  +       |\  r3   +          |     |    \            +
                   +     /| \     +           |     |     \         ++
                    +++ / |  \ +++            |     |      \      ++
                  ooooo/++|+++\xxxxx         rho0   |       \   ++
               ooo    /ooo|xxxx\    xxxx      |     |       ++++
             oo      /    |   | \       xx    |     |+++++++ |\  r1 + r3
            o    r2 /     |   |  \ r1     x   |     |        | \
            o      /______|___|___\_______x___|_    |        |  \
             oo           |       |       x         |        |   \
               ooo     ooo|       |     xx          |        |    \
                  ooooo   |xxxx   | xxxx            |  v0max |     \
                   |      |    xxx|x           rho0 |        |      \
                   |      |       |                 |        | alpha1\
                   |      |---a0--|                 |delta_u0|        \
                   |              |                 x________x_________\
                   |------d0------|                 |        |         |
                                               end_point start_point   |
                                                    |                  |
                                                    |--------a0--------|

	(3) If alpha1 < PI/2,  the parameters defined above are calculated
	    from the following equations:

            r13 = r1 + r3;

            r23 = r2 + r3;

            alpha1 = acos ((r13 * r13 + d0 * d0 - r23 * r23) / (2 * d0 * r13));

            rho0 = r13 * sin (alpha1);

            a0 = r13 * cos (alpha1);           |
                                               |
            u0min = r1 * cos (alpha1);         |
                                               | v0
            u0max = r13 * cos (alpha1);        |
                                               |
            u0min <= u0 <= u0max;              |
                                               +---------------
            v0max = r1 * sin (alpha1);       u0max  <--   u0min

            v0 = rho0 - sqrt (r3 * r3 - (a0 - u0) * (a0 - u0));

            rho0 - r3 <= v0 <= v0max;

	(4) Sometimes it may happen that the angle  alpha1  is larger than
	    PI/2.  It that case,  the entire bond  should be  assigned  to
	    another atom.  To identify such bond,  the cosine of the angle
	    alpha2 should be checked.  The angle alpha2  is formed by  r23
	    and d0 (the chemical bond). The problem described above may be
	    experienced if one of two atoms is small and both another atom
	    and the probe are large.  Very large probe may also cause this
	    kind of trouble.

            alpha2 = acos ((r23 * r23 + d0 * d0 - r13 * r13) / (2 * d0 * r23);

	(5) In the case that alpha2 > PI/2 the formula for u0max should be
	    replaced by:

	    u0max = d0 - r2 * cos (alpha2),

	    where cos (alpha2) < 0,  so that u0max will be larger than d0.

	    the lower limit for v0 will be changed too, but this parameter
	    is not required for drawing.  Other formulas remain unchanged.

	(6) It is very important  to reset the  auxiliaryI  member of each
	    NearestAtomS structure before drawing nice bonds. This counter
	    is used to signal  that color correction was done and there is
	    no need for  further correction.  The color  correction  (i.e.
	    smoothing) is required to ensure nice connections of atoms and
	    bonds.

	(7) Nice style is not allowed for hydrogen bonds.

=============================================================================*/

#include <stdio.h>

#include <math.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

int		HalfNiceBond_ (Aux2S *, int);

/*======draw bonds using the nice style:=====================================*/

size_t DrawNiceBonds_ (MolComplexS *mol_complexSP, int mol_complexesN,
		       ConfigS *configSP, GUIS *guiSP,
		       NearestAtomS *nearest_atomSP, size_t pixelsN,
		       unsigned int refreshI)
{
static size_t		pixelI;
static size_t		bonds_drawnN = 0;
static int		imageI, imagesN;
static int		mol_complexI;
static MolComplexS	*curr_mol_complexSP, *partner_mol_complexSP;
static size_t		atomsN, atomI;
static AtomS		*curr_atomSP, *partner_atomSP;
static int		bondsN, bondI;
static TrueBondS	*curr_bondSP;
static Aux2S		aux2S;

/* Return if there are no structures: */
if (mol_complexesN == 0) return 0;

/* Reset the auxiliaryI member of each NearestAtomS structure. */
/* The zero value means that color correction is not done yet. */
for (pixelI = 0; pixelI < pixelsN; pixelI++)
	{
	(nearest_atomSP + pixelI)->auxiliaryI = 0;
	}

/* Number of images: */
if (configSP->stereoF) imagesN = 2;
else imagesN = 1;

/* Copy some important pointers and parameters: */
aux2S.configSP = configSP;
aux2S.guiSP = guiSP;
aux2S.nearest_atomSP = nearest_atomSP;
aux2S.pixelsN = pixelsN;
aux2S.refreshI = refreshI;

/* Draw each macromolecular complex: */
for (mol_complexI = 0; mol_complexI < mol_complexesN; mol_complexI++)
    {
    /* Pointer to current macromolecular complex: */
    curr_mol_complexSP = mol_complexSP + mol_complexI;

    /* Prepare and check the number of atoms: */
    atomsN = curr_mol_complexSP->atomsN;
    if (atomsN == 0) continue;

    /* Copy the macromolecular complex index: */
    aux2S.mol_complexI = mol_complexI;

    /* Copy the probe radius: */
    aux2S.bond_probe_radius = curr_mol_complexSP->bond_probe_radius;

    /* Draw bonds which have the given style: */
    for (atomI = 0; atomI < atomsN; atomI++)
	{
	/* Pointer to the current atom: */
	curr_atomSP = curr_mol_complexSP->atomSP + atomI;

	/* Check is atom hidden: */
	if (curr_atomSP->hiddenF) continue;

	/* Check is atom inside slab: */
	if (!curr_atomSP->inside_slabF) continue;

	/* Check is atom inside window: */
	if (!curr_atomSP->inside_windowF) continue;

	/* Copy the pointer to the current atom: */
	aux2S.atom1SP = curr_atomSP;

	/* Copy the atomic index: */
	aux2S.atomI = atomI;

	/* Number of bonds: */
	bondsN = curr_atomSP->bondsN;

	/* Bond loop: */
	for (bondI = 0; bondI < bondsN; bondI++)
	    {
	    /* Pointer to the structure with data about current bond: */
	    curr_bondSP = curr_atomSP->true_bondSA + bondI;

	    /* Check bond drawing style: */
	    if (curr_bondSP->bond_styleI != NICE_BONDS) continue;

	    /* If the current bond is hydrogen bond, skip it: */
	    if (curr_bondSP->bond_typeI == 0) continue;

	    /* The complex which contains the atom forming the bond: */
	    partner_mol_complexSP = mol_complexSP +
				    curr_bondSP->neighbor_mol_complexI;

	    /* Pointer to the bond partner: */
	    partner_atomSP = partner_mol_complexSP->atomSP +
			     curr_bondSP->neighbor_arrayI;

	    /* If partner is not visible, do not draw this bond: */
	    if (partner_atomSP->hiddenF) continue;

	    /* Check is the partner inside the slab: */
	    if (!partner_atomSP->inside_slabF) continue;

	    /* Check is the partner inside the window: */
	    if (!partner_atomSP->inside_windowF) continue;

	    /* Copy the bond index: */
	    aux2S.bondI = bondI;

	    /* Copy the pointer to the current bond: */
	    aux2S.curr_bondSP = curr_bondSP;

	    /* Copy the pointer to the bond partner: */
	    aux2S.atom2SP = partner_atomSP;

	    /* Draw one (mono) or two bonds (stereo): */
	    for (imageI = 0; imageI < imagesN; imageI++)
		{
		/* Copy the image index: */
		aux2S.imageI = imageI;

		/* Draw half of a nice bond: */
		HalfNiceBond_ (&aux2S, bondI);

		/* Update the counter: */
		bonds_drawnN++;
		}	/* imageI loop */
	    }		/* bondI  loop */
	}		/* atomI  loop */
    }			/* mol_complexI loop */

/* Return the number of bonds which were drawn: */
return bonds_drawnN;
}

/*===========================================================================*/


