/* Copyright (C) 2000 Damir Zucic */

/*=============================================================================

				copy_doubles.c

Purpose:
	Copy digits, signs and decimal points. Replace anything else with
	space.

Input:
	(1) Output string pointer.
	(2) Input string pointer.
	(3) The number of characters to be copied.

Output:
	(1) Output string.

Return value:
	No return value.

========includes:============================================================*/

#include <stdio.h>

#include <ctype.h>

/*======copy digits, signs and decimal points:===============================*/

void CopyDoubles_ (char *output_stringP, char *input_stringP, int charsN)
{
int		i, n;

/* Fill the output string with zeros (the number of zeros is charsN): */
for (i = 0; i < charsN; i++) *(output_stringP + i) = '\0';

/* Copy digits, signs and decimal points: */
for (i = 0; i < charsN; i++)
	{
	n = *(input_stringP + i);
	if (n == '\0')
		{
		*(output_stringP + i) = '\0';
		break;
		}
	if (isdigit (n) || (n == '-') || (n == '+') || (n == '.'))
		{
		*(output_stringP + i) = n;
		}
	else
		{
		*(output_stringP + i) = ' ';
		}
	}

}

/*===========================================================================*/


