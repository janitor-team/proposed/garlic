/* Copyright (C) 2000-2002 Damir Zucic */

/*=============================================================================

				weight_colors.c

Purpose:
	Weight two colors to prepare the third color.

Input:
	(1) The first color identifier.
	(2) The second color identifier.
	(3) Scale factor (double), which weights the second color.
	(4) Pointer to GUIS structure.

Output:
	Return value.

Return value:
	The identifier of the prepared (new) color.

=============================================================================*/

#include <stdio.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======weight two colors:===================================================*/

unsigned long WeightColors_ (unsigned long color1ID, unsigned long color2ID,
			     double scale_factor, GUIS *guiSP)
{
static unsigned long	new_colorID;
static unsigned long	red_mask, green_mask, blue_mask;
static unsigned long	red1, red2, green1, green2, blue1, blue2;
static long double	r1, r2, g1, g2, b1, b2;
static long double	r, g, b;
static unsigned long	red, green, blue;

/* Copy masks: */
red_mask   = guiSP->visual_infoS.red_mask;
green_mask = guiSP->visual_infoS.green_mask;
blue_mask  = guiSP->visual_infoS.blue_mask;

/* Extract input color components: */
red1   = color1ID & red_mask;
red2   = color2ID & red_mask;
green1 = color1ID & green_mask;
green2 = color2ID & green_mask;
blue1  = color1ID & blue_mask;
blue2  = color2ID & blue_mask;

/* Convert to doubles: */
r1 = (long double) red1;
r2 = (long double) red2;
g1 = (long double) green1;
g2 = (long double) green2;
b1 = (long double) blue1;
b2 = (long double) blue2;

/* Calculate new color components: */
r = r1 + scale_factor * (r2 - r1);
g = g1 + scale_factor * (g2 - g1);
b = b1 + scale_factor * (b2 - b1);
red   = ((unsigned long) r) & red_mask;
green = ((unsigned long) g) & green_mask;
blue  = ((unsigned long) b) & blue_mask;

/* Combine new color components: */
new_colorID = red | green | blue;

/* Return the identifier of the prepared color: */
return new_colorID;
}

/*===========================================================================*/


