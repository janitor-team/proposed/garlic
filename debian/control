Source: garlic
Maintainer: Debichem Team <debichem-devel@lists.alioth.debian.org>
Uploaders: Michael Banck <mbanck@debian.org>,
           Andreas Tille <tille@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13), libx11-dev
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/debichem-team/garlic
Vcs-Git: https://salsa.debian.org/debichem-team/garlic.git
Homepage: http://www.zucic.org/garlic/

Package: garlic
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Suggests: garlic-doc,
          openbabel
Description: visualization program for biomolecules
 Garlic is written for the investigation of membrane proteins. It may be
 used to visualize other proteins, as well as some geometric objects.
 This version of garlic recognizes PDB format version 2.1. Garlic may
 also be used to analyze protein sequences.
 .
 It only depends on the X libraries, no other libraries are needed.
 .
 Features include:
  - The slab position and thickness are visible in a small window.
  - Atomic bonds as well as atoms are treated as independent drawable
    objects.
  - The atomic and bond colors depend on position. Five mapping modes
    are available (as for slab).
  - Capable to display stereo image.
  - Capable to display other geometric objects, like membrane.
  - Atomic information is available for atom covered by the mouse
    pointer. No click required, just move the mouse pointer over the
    structure!
  - Capable to load more than one structure.
  - Capable to draw Ramachandran plot, helical wheel, Venn diagram,
    averaged hydrophobicity and hydrophobic moment plot.
  - The command prompt is available at the bottom of the main window.
    It is able to display one error message and one command string.
