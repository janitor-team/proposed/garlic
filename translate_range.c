/* Copyright (C) 2001 Damir Zucic */

/*=============================================================================

			    translate_range.c

Purpose:
	Translate the specified range of atoms. This function is used to
	translate the residues, in fact.

Input:
	(1) Pointer to AtomS structure, pointing to the first element of
	    the atomic array.
	(2) Index of the first atom in the range.
	(3) Index of the last atom in the range.
	(4) Shift along x axis.
	(5) Shift along y axis.
	(6) Shift along z axis.

Output:
	(1) The specified range of atoms translated.
	(2) Return value.

Return value:
	No return value.

========includes:============================================================*/

#include <stdio.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======translate the range of atoms:========================================*/

void TranslateRange_ (AtomS *atomSP, size_t atom_startI, size_t atom_endI,
		      double delta_x, double delta_y, double delta_z)
{
size_t		atomI;
AtomS		*curr_atomSP;

/* Scan the range of atoms: */
for (atomI = atom_startI; atomI <= atom_endI; atomI++)
	{
	/* Pointer to the current atom: */
	curr_atomSP = atomSP + atomI;

	/* Update the position of the current atom: */
	curr_atomSP->raw_atomS.x[0] += delta_x;
	curr_atomSP->raw_atomS.y    += delta_y;
	curr_atomSP->raw_atomS.z[0] += delta_z;
	}
}

/*===========================================================================*/


