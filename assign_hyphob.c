/* Copyright (C) 2000-2003 Damir Zucic */

/*=============================================================================

				assign_hyphob.c

Purpose:
	Assign hydrophobicity to each atom in a macromolecular complex.
	Use  the scale specified by  the last argument.  If zero is the
	last argument, use default scale.

Input:
	(1) Pointer to MolComplexS structure, with macromolecular data.
	(2) Hydrophobicity scale index.

Output:
	(1) Hydrophobicity information assigned to each atom.
	(2) Return value.

Return value:
	(1) Positive on success (scale is available).
	(2) Zero, if complex contains no atoms.
	(3) Negative on failure (scale not available).

Notes:
	(1) The average  hydrophobicity  is assigned to all atoms which
	    belong to unknown (unidentifyed) residues.

	(2) Check init_runtime.c for default hydrophobicity scale!

========includes:============================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"

/*======function prototypes:=================================================*/

double		Hydrophobicity_ (char *, int, int);

/*======assign hydrophobicity to each atom in a given complex:===============*/

int AssignHydrophobicity_ (MolComplexS *mol_complexSP, int scaleI)
{
size_t		atomsN, atomI;
RawAtomS	*raw_atomSP;
char		*nameP;

/* Return negative value if scale is not available: */
if (scaleI >= SCALESN) return -1;

/* Copy the scale index to the MolComplexS structure: */
mol_complexSP->hydrophobicity_scaleI = scaleI;

/* Store extreme values of hydrophobicity for the given scale: */
mol_complexSP->min_hydrophobicity = Hydrophobicity_ ("", scaleI, 1);
mol_complexSP->max_hydrophobicity = Hydrophobicity_ ("", scaleI, 2);

/* Store average hydrophobicity: */
mol_complexSP->average_hydrophobicity = Hydrophobicity_ ("", scaleI, 3);

/* Store threshold hydrophobicity: */
mol_complexSP->threshold_hydrophobicity = Hydrophobicity_ ("", scaleI, 4);

/* Return zero if there are no atoms: */
atomsN = mol_complexSP->atomsN;
if (atomsN == 0) return 0;

/* Scan all atoms: */
for (atomI = 0; atomI < atomsN; atomI++)
	{
	/** Pointer to raw atomic data: **/
	raw_atomSP = &(mol_complexSP->atomSP + atomI)->raw_atomS;

	/** Pointer to residue name: **/
	nameP = raw_atomSP->pure_residue_nameA;

	/** Assign hydrophobicity: **/
	raw_atomSP->hydrophobicity = Hydrophobicity_ (nameP, scaleI, 0);
	}

/* If this point is reached, return positive value (success indicator): */
return 1;
}

/*===========================================================================*/


