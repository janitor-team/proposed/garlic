/* Copyright (C) 2000-2006 Damir Zucic */


/* Structures used in garlic program. */


/* Color RGB specifications; this structure is slightly smaller than XColor: */

typedef struct		/* RGBS */
	{
	unsigned short	red;
	unsigned short	green;
	unsigned short	blue;

	} RGBS;


/* The structure used to store color schemes: */
typedef struct		/* ColorSchemeS */
	{
	int		surfacesN;
	RGBS		left_rgbSA[MAXCOLORSURFACES];
	RGBS		middle_rgbSA[MAXCOLORSURFACES];
	RGBS		right_rgbSA[MAXCOLORSURFACES];

	} ColorSchemeS;


/* Three dimensional vector structure: */

typedef struct		/* VectorS */
	{
	double		x;
	double		y;
	double		z;

	} VectorS;


/* Configuration data from  $HOME/.garlicrc , or - if this file */
/* does not exist - from  /usr/local/lib/garlic/.garlicrc file: */
/*----------------------------------*/
/* _atomic_ = atomic world units    */
/* _real_   = real world units      */
/* _screen_ = screen units (pixels) */
/*----------------------------------*/
/* Slab and  fading modes: */
/*-------------------------*/
/* 0 = off (not used)      */
/* 1 = planar (default)    */
/* 2 = spherical           */
/* 3 = half-sphere         */
/* 4 = cylindrical         */
/* 5 = half-cylinder       */
/*-------------------------*/

typedef struct		/* ConfigS */
	{
	/* Log file name, flag and pointer: */
	char		log_file_nameA[STRINGSIZE];
	int		log_fileF;
	FILE		*log_fileP;

	/* Main window geometry: */
	char		geometryA[SHORTSTRINGSIZE];

	/* Font name and cursor name: */
	char		font_nameA[SHORTSTRINGSIZE];
	char		cursor_nameA[SHORTSTRINGSIZE];

	/* Coordinate system flag (0 = hidden, 1 = visible): */
	int		show_coord_systemF;

	/* Control window flag (0 = hidden, 1 = visible): */
	int		show_control_windowF;

	/* Sequence neighborhood flag  (0 = hide, 1 = show residue names, */
	/* 2 = show residue names, serial numbers and chain identifiers): */
	int		show_sequence_neighborhoodF;

	/* Stereo flag (0 = draw mono image, 1 = draw stereo image): */
	int		stereoF;

	/* Default slab mode flag (see the list above for possible values): */
	int		default_slab_modeI;

	/* Default color fading mode (see above for possible modes): */
	int		default_fading_modeI;

	/* If print_configF is 1, print configuration data to stdout: */
	int		print_configF;

	/* If print_cursor_namesF is 1, print cursor names and exit: */
	int		print_cursor_namesF;

	/* Light source position may be defined  using two angles. */
	/* The angle  light_theta  is defined relative  to z axis, */
	/* while light_phi is defined relative to x axis. Positive */
	/* values of light_phi are defined according to right-hand */
	/* rule if positive z axis  is rotation axis.  Ranges are: */
	/* light_theta from 0 to PI,  light_phi from  0 to 2 * PI. */
	double		light_theta;
	double		light_phi;

	/* Light source unit vector: */
	VectorS		light_vectorS;

	/* Strings specifying default colors for all windows: */
	char		bg_colorA[SHORTSTRINGSIZE];
	char		fg_colorA[SHORTSTRINGSIZE];
	char		text_bg_colorA[SHORTSTRINGSIZE];
	char		text_fg_colorA[SHORTSTRINGSIZE];

	/* Default number of color fading surfaces: */
	int		default_surfacesN;

	/* Strings specifying default basic colors for atoms and bonds: */
	char		left_colorAA[MAXCOLORSURFACES][SHORTSTRINGSIZE];
	char		middle_colorAA[MAXCOLORSURFACES][SHORTSTRINGSIZE];
	char		right_colorAA[MAXCOLORSURFACES][SHORTSTRINGSIZE];

	/* Default basic colors (color structures) for atoms and bonds: */
	RGBS		left_rgbSA[MAXCOLORSURFACES];
	RGBS		middle_rgbSA[MAXCOLORSURFACES];
	RGBS		right_rgbSA[MAXCOLORSURFACES];

	/* Default surface color schemes: */
	char		surface_outer_color_schemeA[SHORTSTRINGSIZE];
	char		surface_inner_color_schemeA[SHORTSTRINGSIZE];

	/* Six default colors for the outer side of the surface: */
	RGBS		out_left_near_rgbS;
	RGBS		out_middle_near_rgbS;
	RGBS		out_right_near_rgbS;
	RGBS		out_left_far_rgbS;
	RGBS		out_middle_far_rgbS;
	RGBS		out_right_far_rgbS;

	/* Six default colors for the inner side of the surface: */
	RGBS		in_left_near_rgbS;
	RGBS		in_middle_near_rgbS;
	RGBS		in_right_near_rgbS;
	RGBS		in_left_far_rgbS;
	RGBS		in_middle_far_rgbS;
	RGBS		in_right_far_rgbS;

	/* Default drawing styles for atoms, bonds and backbone: */
	int		default_atom_styleI;
	int		default_bond_styleI;
	int		default_backbone_styleI;

	/* Bond length parameters: */
	double		max_bond_length;	  /* In angstroms */
	double		C_C_bond_length_min;      /* carbon-carbon */
	double		C_C_bond_length_max;
	double		C_N_bond_length_min;	  /* carbon-nitrogen */
	double		C_N_bond_length_max;
	double		C_O_bond_length_min;	  /* carbon-oxygen */
	double		C_O_bond_length_max;
	double		C_S_bond_length_min;	  /* carbon-sulfur */
	double		C_S_bond_length_max;
	double		C_H_bond_length_min;      /* carbon-hydrogen */
	double		C_H_bond_length_max;
	double		N_O_bond_length_min;	  /* nitrogen-oxygen */
	double		N_O_bond_length_max;
	double		N_H_bond_length_min;      /* nitrogen-hydrogen */
	double		N_H_bond_length_max;
	double		O_H_bond_length_min;	  /* oxygen-hydrogen */
	double		O_H_bond_length_max;
	double		S_H_bond_length_min;	  /* sulfur-hydrogen */
	double		S_H_bond_length_max;
	double		O_P_bond_length_min;	  /* oxygen-phosphorus */
	double		O_P_bond_length_max;
	double		S_S_bond_length_min;	  /* sulfur-sulfur (disulf.) */ 
	double		S_S_bond_length_max;
	double		generic_bond_length_min;  /* unrecognized bond */
	double		generic_bond_length_max;
	double		hydro_bond_length_min;	  /* hydrogen bond */
	double		hydro_bond_length_max;
	double		hydro_bond_angle_min;
	double		hydro_bond_angle_max;

	/* The calculated (practical) bond length parameters: */
	double		C_C_min_squared;
	double		C_C_max_squared;
        double		C_N_min_squared;
        double		C_N_max_squared;
        double		C_O_min_squared;
        double		C_O_max_squared;
        double		C_S_min_squared;
        double		C_S_max_squared;
	double		C_H_min_squared;
	double		C_H_max_squared;
	double		N_O_min_squared;
	double		N_O_max_squared;
	double		N_H_min_squared;
	double		N_H_max_squared;
	double		O_H_min_squared;
	double		O_H_max_squared;
	double		S_H_min_squared;
	double		S_H_max_squared;
	double		O_P_min_squared;
	double		O_P_max_squared;
	double		S_S_min_squared;
	double		S_S_max_squared;
	double		generic_min_squared;
	double		generic_max_squared;
	double		hydro_min_squared;
	double		hydro_max_squared;

	/* The maximal CA-CA distance, for two neighbouring residues: */
	double		CA_CA_dist_max;
	double		CA_CA_dist_max_squared;

	/* Atomic radii (in angstroms): */
	double		H_radius;
	double		C_radius;
	double		N_radius;
	double		O_radius;
	double		S_radius;
	double		P_radius;
	double		generic_radius;

	/* Covalent radii (in angstroms): */
	double		H_covalent_radius;
	double		C_covalent_radius;
	double		N_covalent_radius;
	double		O_covalent_radius;
	double		S_covalent_radius;
	double		P_covalent_radius;
	double		generic_covalent_radius;

	/* Small radii (in angstroms): */
	double		H_small_radius;
	double		C_small_radius;
	double		N_small_radius;
	double		O_small_radius;
	double		S_small_radius;
	double		P_small_radius;
	double		generic_small_radius;

	/* van der Waals radii (in angstroms): */
	double		H_van_der_Waals_radius;
	double		C_van_der_Waals_radius;
	double		N_van_der_Waals_radius;
	double		O_van_der_Waals_radius;
	double		S_van_der_Waals_radius;
	double		P_van_der_Waals_radius;
	double		generic_van_der_Waals_radius;

	/* Ball radius (used to draw balls and sticks): */
	double		default_ball_radius;

	/* Default bond probe radius (used to draw nice bonds): */
	double		default_bond_probe_radius;

	/* Default stick radius (used to draw bonds as sticks): */
	double		default_stick_radius;

	/* Number of neighbors  which should  be checked as */
	/* bond candidates; both values should be positive: */
	int		bond_candidates_backward;
	int		bond_candidates_forward;

	/* Miscelaneous geometric parameters, related to drawing: */
	int		main_margin_left;
	int		main_margin_right;
	int		main_margin_top;
	int		main_margin_bottom;
	int		max_main_win_width;
	int		max_main_win_height;
	int		nearest_line_thickness;
	double		screen_real_width;
	double		screen_real_height;
	double		user_screen_real_distance;
	double		screen_atomic_width;
	double		user_atomic_position;
	int		stereo_screen_margin;   /* Screen units */
	double		stereo_angle;		/* radians, not degrees */
	double		rotation_stepA[5];
	double		translation_stepA[5];
	double		slab_stepA[5];
	double		fading_stepA[5];

	/* Parameters calculated from geometric data. Used */
	/* to reduce the number of mathematical operations */
	/* required to  project raw coordinates to screen. */
	/* The rationalized_x0 and other rationalized data */
	/* are used to identify atoms  which are invisible */
	/* and whose bonds are invisible  (out of window). */
	double		user_screen_atomic_distance;
	double		screen_atomic_height;
	double		screen_atomic_z;
	double		atomic_to_screen_scale_x;
	double		atomic_to_screen_scale_y;
	double		screen_to_atomic_scale_x;
	double		win_atomic_free_width;
	double		win_atomic_free_height;
	double		win_atomic_x0;		/* Window left edge (mono) */
	double		win_atomic_y0;		/* Top edge */
	double		win_atomic_z0;		/* Screen and window z value */
	double		win_atomic_x1;		/* Right edge */
	double		win_atomic_y1;		/* Bottom edge */
	double		rationalized_x0;
	double		rationalized_y0;
	double		rationalized_x1;
	double		rationalized_y1;
	double		stereo_atomic_margin;
	int		center_screen_x[2];	/* ..._x[0] is the left one */
	int		center_screen_y;
	int		image_screen_x0[2];	/* Image extent */
	int		image_screen_x1[2];
	int		image_screen_y0;
	int		image_screen_y1;

	/* Blur rectangle default width and height: */
	int		blur_default_width;
	int		blur_default_height;

	} ConfigS;


/* Structure with the most important window data: */

typedef struct		/* WindowS */
	{
	Window		ID;
	int		x0;
	int		y0;
	unsigned int	width;
	unsigned int	height;
	int		border_width;
	XSizeHints	size_hintsS;
	unsigned int	colorsN;
	RGBS		bg_rgbS;
	unsigned long	bg_colorID;
	RGBS		fg_rgbS;
	unsigned long	fg_colorID;
	int		pixmapF;             /* 0 = created, 1 = not created */
	Pixmap		pixmapID;
	int		fontF;                 /* 0 = not loaded, 1 = loaded */
	int		font_height;
	int		half_font_height;
	int		quarter_font_height;
	int		text_line_height;
	XFontStruct	*fontSP;
	int		cursorF;             /* 0 = not created, 1 = created */
	Cursor		cursorID;

	} WindowS;


/* Structure needed to store GUI data: */

typedef struct		/* GUIS */
	{
	/* Main window refresh flag (0 = do not refresh, 1 = refresh): */
	int		main_window_refreshF;

	/*-----------------------------------*/
	/* Main window drawing mode index:   */
	/*-----------------------------------*/
	/* 0             structure           */
	/* 1             Ramachandran plot   */
	/* 2             helical wheel       */
	/* 3             Venn diagram        */
	/* 4             plots (hydrophob.)  */
	/* 5             sequence comparison */
	/*-----------------------------------*/
	int		main_window_modeI;

	/* Docking flag (0 = docking mode off, 1 = docking mode on) */
	int		dockingF;

	/* Display data: */
	char		*display_nameP;
	char		display_nameA[STRINGSIZE];
	Display		*displaySP;

	/* Screen data: */
	int		screenID;
	unsigned int	screen_width;
	unsigned int	screen_height;

	/* Flags reserved for keyboard modifiers (0 = not pressed): */
	int		shift_pressedF;
	int		control_pressedF;
	int		alt_pressedF;

	/* Data describing display color capabilities: */
	int		depth;
	XVisualInfo	visual_infoS;
	Visual		*visualSP;
	Colormap	colormapID;

	/* Data for fast TrueColor color allocation: */
	int		red_right_shift;
	int		red_left_shift;
	int		green_right_shift;
	int		green_left_shift;
	int		blue_right_shift;
	int		blue_left_shift;

	/* The main hidden pixmap.  Drawing is done to a hidden pixmap */
	/* which resides on the server side.  Another approach will be */
	/* to use an XImage on the client side. An experiment was made */
	/* to compare two approaches,  using XFree86 implementation of */
	/* X11 window system. The XImage-based method was twice faster */
	/* if both client and server run on the same machine. However, */
	/* the hidden pixmap method was choosen because it offers much */
	/* larger set of  drawing routines  (rectangles,  arcs  etc.). */
	int		main_hidden_pixmapF;
	Pixmap		main_hidden_pixmapID;

	/* The hidden pixmap with small blue arrows. These arrows are used */
	/* to show the main chain direction,  projected to the  XY  plane. */
	int		small_arrows_pixmapF;
	unsigned int	small_arrows_width;
	unsigned int	small_arrows_height;
	Pixmap		small_arrows_pixmapID;

	/* Array of graphics contexts: */
	GC		theGCA[10];
	int		gca_createdF;

	/* Window manager hints: */
	XWMHints	wm_hintsS;

	/* Main window data: */
	WindowS		main_winS;
	unsigned int	main_win_free_area_width;
	unsigned int	main_win_free_area_height;

	/* Icon data: */
	WindowS		icon_winS;

	/* Data required for control window: */
	WindowS		control_winS;

	/* Data required for text input window: */
	WindowS		input_winS;

	/* Data required for text output window: */
	WindowS		output_winS;

	/* Data required for docking window: */
	int		docking_window_createdF;
	int		docking_window_mappedF;
	WindowS		docking_winS;

	/* Data required for docking icon window: */
	WindowS		docking_icon_winS;

	/* Atoms related to window manager: */
	Atom		delete_window_atom;
	Atom		protocols_atom;

	/* Eighteen auxiliary colors: */
	unsigned long	black_colorID;
	unsigned long	gray_colorID;
	unsigned long	dark_gray_colorID;
	unsigned long	white_colorID;
	unsigned long	red_colorID;
	unsigned long	dark_red_colorID;
	unsigned long	green_colorID;
	unsigned long	dark_green_colorID;
	unsigned long	blue_colorID;
	unsigned long	dark_blue_colorID;
	unsigned long	yellow_colorID;
	unsigned long	dark_yellow_colorID;
	unsigned long	cyan_colorID;
	unsigned long	dark_cyan_colorID;
	unsigned long	magenta_colorID;
	unsigned long	dark_magenta_colorID;
	unsigned long	orange_colorID;
	unsigned long	dark_orange_colorID;

	} GUIS;


/* Raw atomic data structure:  it stores atomic data read from PDB file. */
/* Each  molecular complex is  a set  of atoms;  the atomic  information */
/* is the most  important information about  molecular complex.  Residue */
/* information is  not considered to be of such  importance to group all */
/* atoms beloging to the same  residue into a special  structure, though */
/* this approach is  also possible.  As amino acids,  residues,  nucleic */
/* bases, detergents and other molecules  (commonly called residues) are */
/* quite different in size (number of atoms),  grouping AtomS structures */
/* into residue structures will lead to inefficient usage of the memory. */

typedef struct		/* RawAtomS */
	{
	/* Hetero-flag (0 = ATOM, 1 = HETATM): */
	int		heteroF;

	/* Data contained in PDB ATOM and HETATM records: */
	int		serialI;
	char		atom_nameA[ATOMNAMESIZE];	  /* With spaces */
	char		pure_atom_nameA[ATOMNAMESIZE]; /* Without spaces */
	char		chemical_symbolA[SYMBOLSIZE]; /* Right justified */
	char		remoteness_indicator;		   /* Alphabetic */
	char		branch_designator;		      /* Numeric */
	char		alt_location;
	char		residue_nameA[RESNAMESIZE];       /* With spaces */
	char		pure_residue_nameA[RESNAMESIZE];   /* Without s. */
	char		chainID;
	int		residue_sequenceI;
	char		residue_insertion_code;
	double		x[2];		/* Stereo: x[0] left, x[1] right */
	double		y;		/* x[0] is  read  from  PDB file */
	double		z[2];		/* x[1] is calculated  from x[0] */
	int		screen_x[2];	     /* Projection to the screen */
	int		screen_y;
	double		occupancy;
	double		temperature_factor;

	/* Be careful with the remaining fields: older PDB files */
	/* contain some other data after the temperature factor: */
	char		segmentA[SEGNAMESIZE];
	char		elementA[ELEMNAMESIZE];           /* With spaces */
	char		pure_elementA[ELEMNAMESIZE];   /* Without spaces */
	char		chargeA[CHARGESIZE];

	/* SIGATM, ANISOU and SIGUIJ fields not implemented at present! */
	/* Maybe some time later ... */

	/* Model identifier (important for NMR structures): */
	int		model_serialI;

	/* Data below this line are not covered by PDB: */

	/* Style index: */
	int		atom_styleI;

	/* Atomic radius: */
	double		radius;

	/* Covalent radius: */
	double		covalent_radius;

	/* Small radius: */
	double		small_radius;

	/* van der Waals radius: */
	double		van_der_Waals_radius;

	/* Ball radius, used to draw atoms as balls. */
	double		ball_radius;

	/* Additional data: */
	double		hydrophobicity;
	double		special_value;         /* Since version 1.2 */

	} RawAtomS;


/* True bond structure; it contains data about real chemical */
/* bond. Atomic pair ID's may be found in check_dist.c file. */

typedef struct		/* TrueBondS */
	{
	/* Style index (0 = nothing to draw): */
	int		bond_styleI;

	/* Bond ID (see check_dist.c for detailed information): */
	int		pairID;

	/* Bond type:     */
	/*  1 = covalent  */
	/*  0 = hydrogen  */
	/*  2 = disulfide */ 
	/* 11 = pseudo    */
	int		bond_typeI;

	/* Neighboring atom specification: */
	int		neighbor_mol_complexI;
	size_t		neighbor_arrayI;

	/* Bond length: */
	double		bond_length;

	} TrueBondS;


/* Atomic data structure; it contains raw atomic data and data */
/* about bonds.  The same basic colors are used both for given */
/* atom and  all bonds  associated with  this atom.  If one or */
/* more bonds should be colored differently,  try some special */
/* tricks.  For example,  it may be interesting to color bonds */
/* according to  bond  length  (red too close,  blue too far). */
/* Normally,  hydrogen  bonds should be  drawn using  the same */
/* basic colors and  the same  drawing style  for all hydrogen */
/* bonds.  These colors should be stored elsewhere,  not here. */
/*-------------------------------------------------------------*/
/* Flags:                                                      */
/*-------------------------------------------------------------*/
/* selectedF        Equal to 1 if atom is currently  selected. */
/* hiddenF          Equal to 1 if atom is hideen, 0 otherwise. */
/* inside_slabF     Depends on slab: visible atoms are inside. */
/* in_windowF       Some atoms may  fall outside  main window. */
/*-------------------------------------------------------------*/

typedef struct		/* AtomS */
	{
	/* Selection flag (current and previous): */
	int		selectedF;
	int		previous_selectedF;

	/* Visibility flags (read Note 4 in main_expose.c): */
	int		hiddenF;
	int		inside_slabF;
	int		inside_windowF;

	/* Additional visibility flag, used only in ControlRefresh_ (): */
	int		inside_projected_slabF;

	/* Label flag (1 = write label): */
	int		labelF;

	/* Auxiliary index (used to recognize rings, for example). */
	/* Do not forget  to reset  this index  before  every use! */
	int		auxiliaryI;

	/* The number of color fading surfaces: */
	int		surfacesN;

	/* Basic colors, used both for atom and its bonds. */
	/* Used to prepare the  atom_colorID  for drawing. */
	RGBS		left_rgbSA[MAXCOLORSURFACES];
	RGBS		middle_rgbSA[MAXCOLORSURFACES];
	RGBS		right_rgbSA[MAXCOLORSURFACES];

	/* Atom color ID's (used for drawing): */
	unsigned long	left_colorID;
	unsigned long	middle_colorID;
	unsigned long	right_colorID;

	/* Array index of the residue to which this atom belongs: */
	size_t		residue_arrayI;

	/* Raw atomic data (atom without bonds): */
	RawAtomS	raw_atomS;

	/* Bonds: */
	int		bondsN;
	TrueBondS	true_bondSA[MAXBONDS];

	} AtomS;


/* Structure for  macromolecular  complex  header data (text). */
/* Header is divided into six subsets:  header, title, compnd, */
/* source, expdta, author.  Each subset is  defined by  offset */
/* and number of lines. A total number of lines is stored too. */

typedef struct		/* HeaderS */
	{
	char		*dataP;
	int		total_linesN;
	int		header_linesN;
	int		header_offset;
	int		title_linesN;
	int		title_offset;
	int		compnd_linesN;
	int		compnd_offset;
	int		source_linesN;
	int		source_offset;
	int		expdta_linesN;
	int		expdta_offset;
	int		author_linesN;
	int		author_offset;

	} HeaderS;


/* Backbone structure, used to store information about CA atoms. */

typedef struct		/* BackboneS */
	{
	int		hiddenF;
	size_t		c_alphaI;
	int		backbone_styleI;
	int		previous_c_alphaF;
	size_t		previous_c_alphaI;
	int		next_c_alphaF;
	size_t		next_c_alphaI;

	} BackboneS;


/* Residue structure, used to store sequence */
/* info, dihedral angles and cis-trans flag. */

typedef struct		/* ResidueS */
	{
	size_t		residue_startI;
	size_t		residue_endI;
	double		phi;
	double		psi;
	double		omega;
	int		cis_transF; /* 0 = bad/undefined, 1 = trans, 2 = cis */
	double		chi1;
	double		chi2;
	double		chi3;
	double		chi4;
	double		chi5;

	} ResidueS;


/* Plane structure defines  the plane associated  with a given macromol. */
/* complex.  The plane is represented  by a circle and  defined by plane */
/* center,  normal vector  and  circle radius.  Projected to the screen, */
/* plane looks like a tilted ellipse.  The angle normal_phi is the angle */
/* between x axis and  the projection of  the normal vector to xy plane. */
/* It is equal to zero if the projection is parallel to x axis. Positive */
/* direction is clockwise.  The angle normal_theta  is the angle between */
/* the normal vector and z axis. The value is zero if they are parallel. */
/* Half axis  a and  b are given in screen units  but stored as doubles. */

typedef struct		/* PlaneS */
	{
	int		hiddenF;           /* 0 = visible, 1 = unvisible */
	int		visible_sideI[2];  /* 0 = top, 1 = bottom */
	double		center_x[2];       /* Atomic units */
	double		center_y;
	double		center_z[2];
	int		center_screen_x[2];
	int		center_screen_y;
	double		normal_x[2];
	double		normal_y;
	double		normal_z[2];
	double		circle_radius;     /* Circle radius  in atomic units */
	double		screen_a;          /* Large half-axis of the ellipse */
	double		screen_b[2];       /* Small half-axis of the ellipse */
	double		normal_theta[2];   /* See definition above. */
	double		normal_phi[2];     /* See definition above. */
	RGBS		top_near_rgbS;     /* Top side  near color */
	RGBS		top_far_rgbS;      /* Top side  far  color */
	RGBS		bottom_near_rgbS;  /* Bottom s. near color */
	RGBS		bottom_far_rgbS;   /* Bottom s. far  color */
	unsigned long	top_near_colorID;
	unsigned long	top_far_colorID;
	unsigned long	bottom_near_colorID;
	unsigned long	bottom_far_colorID;
	double		transparency;     /* 0.0 = opaque, 1.0 = transparent */

	} PlaneS;


/* Membrane structure defines  the membrane associated with a given */
/* macromolecular complex.  Due to  the thermal  motion,  it is not */
/* possible to define the inner and the outer edge of the membrane. */
/* However,  the representation which uses two planes to define the */
/* membrane is very popular. Do not forget that this is a bad idea. */

typedef struct		/* MembraneS */
	{
	int		definedF;       /* 0 = missing, 1 = defined */
	int		hiddenF;      /* 0 = visible, 1 = unvisible */
	double		center_x;                   /* Atomic units */
	double		center_y;
	double		center_z;
	double		thickness;                 /* In angstroms. */
	PlaneS		plane1S;
	PlaneS		plane2S;

	} MembraneS;


/* Dot structure is used  to define the surface.  The surfaces are */
/* treated as sets of dots, with some common properties.  A single */
/* dot on  a given surface is  defined by  the radius vector,  the */
/* unit (normal) vector, two flags and a set of twelve colors. Six */
/* outside colors are used if the projection of  the normal vector */
/* to z axis is negative, i.e. if the outer side of the surface at */
/* the given point is on observer's side. Six inside (rear) colors */
/* are used if the normal vector has a positive z component,  i.e. */
/* if the observer sees  the inner side of  the surface.  A single */
/* transparency value  is used for  all dots at  the same surface. */

typedef struct		/* DotS */
	{
	/* Radius vector and unit vector normal to the surface: */
	VectorS		radius_vectorS;
	VectorS		normal_vectorS;

	/* Six colors for the outer side of the surface: */
	RGBS		out_left_near_rgbS;
	RGBS		out_middle_near_rgbS;
	RGBS		out_right_near_rgbS;
	RGBS		out_left_far_rgbS;
	RGBS		out_middle_far_rgbS;
	RGBS		out_right_far_rgbS;

	/* Six colors for the inner side of the surface: */
	RGBS		in_left_near_rgbS;
	RGBS		in_middle_near_rgbS;
	RGBS		in_right_near_rgbS;
	RGBS		in_left_far_rgbS;
	RGBS		in_middle_far_rgbS;
	RGBS		in_right_far_rgbS;

	/* Selection flag (0 = dot is not selected, 1 = dot is selected): */
	char		selectedF;

	/* Hide flag (0 = dot is visible, 1 = dot is hidden): */
	char		hiddenF;

	/* The array index of  the macromolecular complex  and  the */
	/* array index of the atom to which this dot belongs (These */ 
	/* data are required if using  CPK color scheme  for dots): */
	int		mol_complexI;
	size_t		atomI;

	} DotS;


/* Surface structure is used to define the molecular surface. It is */
/* possible to associate  more than one surface with a given molec. */
/* structure (the maximal number is MAXMOLSURFACES, see defines.h). */
/* Each surface is a finite set of dots (see DotS structure above), */
/* with a number of common properties, like transparency and color. */
/* If the number of dots for a given surface  (dotsN) is zero,  the */
/* surface is not defined (used) and  the storage is not allocated. */
/* Take care about  dotsN and storage when discarding some surface! */

typedef struct		/* SurfaceS */
	{
	/* Number of dots and pointer to the storage: */
	int		dotsN;
	DotS		*dotSP;

	int		hiddenF; /* 0 = visible, 1 = hidden; this flag */
				 /* overrides flags of individual dots */

	/* Surface transparency mode index.        */
	/*-----------------------------------------*/
	/* 0 = Default mode (uniform transparency) */
	/* 1 = Transparency depends on the angle   */
	/*     between the unit vector and z axis  */
	/*-----------------------------------------*/
	int		transparency_modeI;

	/* Surface transparency value: */
	double		transparency;

	/* The surface probe radius. This probe radius is used */
	/* to generate  the surface.  Do not  confuse it  with */
	/* the probe radius  which is used to draw nice bonds! */
	double		surface_probe_radius;

	} SurfaceS;


/* Structure which contains molecular complex data (proteins, DNA). */
/* Each  molecular complex is  a set  of atoms.  The fact that each */
/* molecular complex is made of building blocks called  residues is */
/* not very important for this program,  as atoms are considered to */
/* be  the  basic  building  blocks.  Atomic data  are stored  into */
/* dynamically  allocated  memory.  Reallocation  mechanism must be */
/* ensured  as the  molecular complex size  is not known in advance */
/* and large insertions into the structure are allowed  at runtime. */

typedef struct		/* MolComplexS */
	{
	/* Macromolecular complex identifier: */
	int		mol_complexID;

	/* Unique PDB identifier: */
	char		unique_PDB_codeA[PDBCODESIZE];

	/* Tag associated with the complex: */
	int		tagF;
	char		tagA[TAGSIZE];

	/* File name: */
	char		file_nameA[STRINGSIZE];

	/* Catch flag; if equal to one, the movement, slab and */
	/* fading controls affect  the macromolecular complex: */
	int		catchF;

	/* Move bits;  structure,  plane and */
	/* envelope may be moved separately. */
	/* By default,  everything is moved. */
	/*-----------------------------------*/
	/* Bit:               If set to one: */
	/*-----------------------------------*/
	/* 1 (LSB)            Move structure */
	/* 2                  Move plane     */
	/* 3                  Move membrane  */
	/* 4                  Move envelope  */
	/*-----------------------------------*/
	int		move_bits;

	/* Slab mode flag (each macromol. complex has its own): */
	int		slab_modeI;

	/* Color fading mode flag (each complex has its own): */
	int		fading_modeI;

	/* Flag used to signal that position or orientation changed: */
	int		position_changedF;

	/* Geometric center position and spacial extent of the object. */
	/* Note: spacial extent is very approximate, calculated at the */
	/* very beginning.  It is used to set  the initial slab width. */
	VectorS		geometric_center_vectorS;
	VectorS		left_top_near_vectorS;
	VectorS		right_bottom_far_vectorS;

	/* The maximal extent (the largest dimension): */
	double		max_extent;

	/* Rotation center position: */
	VectorS		rotation_center_vectorS;

	/* The storage which is used  to backup the rotation */
	/* center position if  the macromolecular complex is */
	/* taking part  in a group of complexes.  The stored */
	/* value is copied back when the group is dismissed. */
	VectorS		backup_vectorS;

	/* Slab center position. It is used as the reference point for */
	/* planar slab, cylindrical slab and spherical slab.  The sym- */
	/* metry axis of  the cylindrical slab  is parallel to y-axis. */
	VectorS		slab_center_vectorS;

	/* Parameters which define the relative positions */
	/* of slab surfaces  with respect to slab center. */
	double		slab_front_relative_position;
	double		slab_back_relative_position;

	/* Color fading center.  Used in a similar way as slab center. */
	VectorS		fading_center_vectorS;

	/* Parameters  which define  the relative  positions of */
	/* color fading surfaces with respect to fading center. */
	double		fading_front_relative_position;
	double		fading_back_relative_position;

	/* Hydrophobicity scale index and reference */
	/* hydrophobicity values for a given scale: */
	int		hydrophobicity_scaleI;
	double		min_hydrophobicity;    /* Extreme values for current */
	double		max_hydrophobicity;    /* scale, not for the complex */
	double		average_hydrophobicity;
	double		threshold_hydrophobicity;

	/* Information header: */
	HeaderS		headerS;    /* Contains pointer to alloc. storage! */

	/* Atoms: */
	size_t		atomsN;     /* Number of atoms in a complex */
	size_t		max_atomsN;
	AtomS		*atomSP;

	/* Backbone: */
	size_t		c_alphaN;
	BackboneS	*backboneSP;

	/* Sequence, dihedral angles,  cis-trans */
	/* flags and sec. structure information: */
	size_t		residuesN;
	ResidueS	*residueSP;
	char		*secondary_structureP;

	/* Plane: */
	PlaneS		planeS;

	/* Membrane: */
	MembraneS	membraneS;

	/* A number of surfaces associated with the structure: */
	SurfaceS	surfaceSA[MAXMOLSURFACES];

	/* Two flags related to  hydrogen bonds.  The first flag gives the */
	/* status of hydrogen bonds  (0 = missing or obsolete, 1 = valid). */
	/* The second flag is equal to zero if hydrogen bonds are visible. */
	int		hydrogen_bondsF;
	int		hydrogen_bonds_hiddenF;

	/* Bond probe radius.  Each structure has  its own  probe radius. */
	/* Do not confuse this probe with the probe used for the surface. */
	double		bond_probe_radius;

	/* Stick radius, used to draw bonds as sticks. Each structure has */
	/* its own stick radius.  A single radius is used  for all bonds. */
	double		stick_radius;

	/* Group member flag. Zero for independent structures, */
	/* one for all structures which are forming the group. */
	int		group_memberF;

	} MolComplexS;


/* NearestAtomS is used to store data about the nearest atom for a given */
/* pixel of the main window  (excluding control, input and output win.). */
/* Each time  the macromolecular complexes are drawn to the main window, */
/* the fact that some atoms are closer to  the observer than other atoms */
/* must be taken into account. Both complex array index and  atom serial */
/* number are stored.  If some atoms are  inserted or deleted in  one or */
/* more macromolecular complexes,  NearestAtomS has to be reinitialized. */
/* Unvisible atoms are not taken into account while comparing distances. */
/* Because NearestAtomS is used, there is no need to sort atoms by dist. */
/* The value of the refreshI must be in range between 1 and MAXREFRESHI. */
/* If maximal value is exceeded,  reinitialize the  NearestAtomS  array. */
/* The size of  NearestAtomS  should be checked after the main window is */
/* resized.  The array should be reallocated if main window is enlarged. */
/* Always check the column and  row index of a  given pixel,  as well as */
/* the combined (array) index before using  NearestAtomS array elements. */
/* In addition,  the NearestAtomS is also used  for sequence comparison. */
/* The member bondI is zero or positive if the specified pixel is filled */
/* by certain true bond and negative  if it is filled by something else. */

typedef struct		/* NearestAtomS */
	{
	unsigned int	last_refreshI;
	int		mol_complexI;
	size_t		atomI;
	int		bondI;
	double		z;
	int		styleI;
	unsigned long	colorID;
	int		auxiliaryI;

	} NearestAtomS;


/* Aux1S structure,  used to store some data and pointers required to */
/* draw simple bonds. Bond drawing functions require many parameters. */
/* Aux1S is used to reduce the number of arguments in function calls. */

typedef struct		/* Aux1S */
	{
	int		imageI;
	int		screen_x0;
	int		screen_x1;
	int		screen_delta_x;
	int		screen_y0;
	int		screen_y1;
	int		screen_delta_y;
	double		atomic_z0;
	double		atomic_z1;
	double		atomic_delta_z;
	ConfigS		*configSP;
	GUIS		*guiSP;
	NearestAtomS	*nearest_atomSP;
	size_t		pixelsN;
	unsigned int	refreshI;
	int		mol_complexI;
	size_t		atomI;
	unsigned long	colorIDA[10];

	} Aux1S;


/* Aux2S structure,  used to store  some data and */
/* pointers required to draw nice (curved) bonds. */

typedef struct		/* Aux2S */
	{
	int		imageI;
	int		mol_complexI;
	size_t		atomI;
	int		bondI;
	ConfigS		*configSP;
	GUIS		*guiSP;
	NearestAtomS	*nearest_atomSP;
	size_t		pixelsN;
	unsigned int	refreshI;
	AtomS		*atom1SP;
	AtomS		*atom2SP;
	TrueBondS	*curr_bondSP;
	double		bond_probe_radius;
	double		stick_radius;

	} Aux2S;


/* ExposedResidueS structure,  with data about  exposed polar residue. */
/* If excludedF is equal to one the exposed residue should be ignored. */

typedef struct		/* ExposedResidueS */
	{
	int		excludedF;
	int		donorI;   /* 0 = acceptor, 1 = donor, 2 = both */
	size_t		representative_atomI;
	char		pure_residue_nameA[RESNAMESIZE];
	char		chainID;
	int		residue_sequenceI;
	char		residue_insertion_code;

	} ExposedResidueS;


/* BetaCellS structure, used to find the proper membrane position */
/* and orientation  with respect to a given  beta barrel protein. */
typedef struct		/* BetaCellS */
	{
	int		cell_usedF;
	double		distance;
	double		hydrophobicity;
	int		cells_usedN;
	int		average_calculatedF;
	double		average_hydrophobicity;
	} BetaCellS;


/*---------------------------------------------------*/
/* RuntimeS structure: a lot of run-time data.       */
/*---------------------------------------------------*/
/* click_modeI:                                      */
/*---------------------------------------------------*/
/*   0 = use clicks for distances and angles.        */
/*   1 = use clicks to pick bond for editing.        */
/*---------------------------------------------------*/
/* edit_modeI:                                       */
/*---------------------------------------------------*/
/*   0 = default mode (no editing).                  */
/*   1 = edit atom(s).                               */
/*   2 = edit phi.                                   */
/*   3 = edit psi.                                   */
/*   4 = edit omega.                                 */
/*   5 = edit main chain (edit phi and psi).         */
/*   6 = edit bond.                                  */
/*   7 = edit side chain (chi1 and chi2).            */
/* 101 = edit dimensions of selected portion.        */
/*---------------------------------------------------*/
/* rama_selectionF:                                  */
/*---------------------------------------------------*/
/* 0 = draw Ramachandran plot for all residues.      */
/* 1 = draw Ramachandran plot for selected residues. */
/*---------------------------------------------------*/
/* *disulfideFP:                                     */
/*---------------------------------------------------*/
/* 0 = not involved in disulfide bond.               */
/* 1 = involved in disulfide bond.                   */
/*---------------------------------------------------*/
/* wheel_clockwiseF:                                 */
/*---------------------------------------------------*/
/* 0 = draw helical wheel anticlockwise (top view).  */
/* 1 = draw helical wheel clockwise (bottom view).   */
/*---------------------------------------------------*/
/* groupF:                                           */
/*---------------------------------------------------*/
/* 0 = only 1 struct. is caught,  there is no group. */
/* 1 = one, two or more structures are grouped.      */
/*---------------------------------------------------*/

typedef struct		/* RuntimeS */
	{
	int		click_modeI;
	int		edit_modeI;
	int		edit_single_bondF;
	int		default_complexI;
	int		scriptF;
	int		pauseF;
	int		rama_selectionF;
	int		wheel_clockwiseF;
	int		groupF;

	char		*commandsP;                 /* History buffer */
	int		next_commandI;
	int		old_commandI;
	int		highest_commandI;
	char		curr_commandA[COMMSTRINGSIZE];
	int		command_length;
	int		carriage_position;
	int		left_part_widthA[COMMSTRINGSIZE];
	char		messageA[COMMSTRINGSIZE];
	int		message_length;

	int		title_hiddenF[MAXTITLES];
	int		title_screen_x[MAXTITLES];
	int		title_screen_y[MAXTITLES];
	char		*titlesP;                   /* Buffer for titles     */

	int		atom1_serialI;              /* Serial numbers of two */
	int		atom2_serialI;              /* atoms which form bond */
	size_t		atom1_arrayI;               /* Array indices of      */
	size_t		atom2_arrayI;               /* these two atoms       */

	int		hydrophobicity_scaleI;
	double		helix_step_angle;         /* degrees   */
	double		arc_angle;                /* degrees   */
	double		sphere_radius;            /* angstroms */
	int		sliding_window_width;     /* residues  */
	int		average_hydrophobicityF;  /* Average hydrophobicity  */
	int		weighted_hydrophobicityF; /* Weighted hydrophobicity */
	int		hydrophobic_momentF;      /* Hydrophobic moment      */
	int		sided_hydrophobicityF;    /* Two sided-h. functions  */
	int		function1F;               /* 1 = draw function F1    */
	int		function2F;               /* 1 = draw function F2    */
	int		function3F;               /* 1 = draw function F3    */
	int		function4F;               /* 1 = draw function F4    */
	int		function5F;               /* 1 = draw function F5    */
	int		function6F;               /* 1 = draw function F6    */
	int		function7F;               /* 1 = draw function F7    */

	size_t		sequence_buffer_size;       /* Size of seq. buffer   */
	size_t		residuesN;                  /* Sequence length       */
	char		*sequenceP;                 /* Main sequence buffer  */
	char		*sec_structureP;            /* Secondary structure   */
	int		sec_structure_length;       /* Number of codes       */
	int		*disulfideFP;               /* S-S bond flags        */
	int		*serialIP;                  /* Serial numbers        */
	int		*residue_codeIP;            /* Numeric residue codes */
	double		*hydrophobicityP;           /* Hydrophobicity values */
	double		*weighted_hydrophobicityP;  /* Weighted hyd. values  */
	double		*average_hydrophobicityP;   /* For given  win. width */
	double		*hydrophobic_momentP;       /* Absolute value        */
	double		*larger_sided_hyphobP;      /* Sided h.,  upper line */
	double		*smaller_sided_hyphobP;     /* Sided h., bottom line */
	double		*function1P;		    /* Hydroph. function F1  */
	double		*function2P;                /* Hydroph. function F2  */
	double		*function3P;                /* Hydroph. function F3  */
	double		*function4P;                /* Hydroph. function F4  */
	double		*function5P;                /* Hydroph. function F5  */
	double		*function6P;                /* Hydroph. function F6  */
	double		*function7P;                /* Hydroph. function F7  */

	int		*auxiliaryIP;               /* Aux. integer storage  */
	double		*aux_doubleP;               /* Aux. double storage   */

	size_t		reference_residuesN;        /* Reference seq. length */
	char		*reference_sequenceP;       /* Reference seq. buffer */
	char		*reference_sec_structureP;  /* Refer. sec. structure */
	int		reference_sec_str_length;   /* Number of codes       */
	int		*reference_serialIP;        /* Ref. seq. ser. number */
	int		*reference_residue_codeIP;  /* Numeric residue codes */
	double		*reference_hydrophobicityP; /* Hydrophobicity values */

	int		zoom_factor;                /* Zoom factor: 1, 2 ... */
	int		sequence_offset;            /* Sequence offset       */
	int		reference_offset;           /* Reference seq. offset */
	int		segment_width;              /* Use to compare 2 seq. */
	int		minimal_score;              /* Use to compare 2 seq. */

	int		range_startI;             /* Residue range start     */
	int		range_endI;               /* Residue range end       */

	size_t		pattern_buffer_size;      /* Size of pattern buffer  */
	size_t		pattern_length;           /* Number of positions     */
	char		*patternP;                /* Pattern buffer          */
	int		namesNA[MAX_PATT_LENGTH]; /* Residue names per pos.  */
	int		residues_in_patternN;	  /* Total number of resid.  */
	int		pattern_tolerance;	  /* Numb. of allowed errors */

	MolComplexS	*mol_complex1SP;          /* Involved in docking     */
	MolComplexS	*mol_complex2SP;          /* Involved in docking     */
	double		docking_area_width;       /* angstroms               */
	double		docking_cell_width;       /* angstroms               */
	int		docking_matrix_width;     /* Cells across width      */
	int		*exposed_atom1IP;         /* Involved in docking     */
	int		*exposed_atom2IP;         /* Involved in docking     */
	ExposedResidueS	*exposed_polar1SP;        /* Exposed polar, bottom   */
	int		exposed_polar1N;          /* Number of e.p.r. bottom */
	ExposedResidueS	*exposed_polar2SP;        /* Exposed polar, top      */
	int		exposed_polar2N;          /* Number of e.p.r. top    */

	int		template_atomsN;          /* Numb. of template atoms */
	int		max_template_atomsN;      /* Max. n. of template at. */
	AtomS		*template_atomSP;         /* Used to create peptides */
	size_t		template_residuesN;       /* N. of template residues */
	ResidueS	*template_residueSP;      /* Information  about res. */

	int		blurF;			  /* 0 = blur off, 1 = on    */
	int		blur_width;		  /* Width of blur rectangle */
	int		blur_height;		  /* Height of blur rect.    */

	int		beta_cellsN;		  /* Auxiliary cells used to */
	BetaCellS	*beta_cellSP;		  /* find  membrane position */
						  /* relative to beta barrel */

	int		active_surfaceI;          /* For default  m. complex */

	} RuntimeS;


/* SelectS structure, with the selection criteria. */

typedef struct		/* SelectS */
	{
	int		all_chainsF;
	int		chainsN;
	char		chainIDA[MAXFIELDS];
	int		all_residue_serialF;
	int		residue_serial_rangesN;
	int		residue_serial_start[MAXFIELDS];
	int		residue_serial_end[MAXFIELDS];
	int		all_residue_namesF;
	int		residue_namesN;
	char		residue_nameAA[MAXFIELDS][RESNAMESIZE];
	int		all_atom_namesF;
	int		atom_namesN;
	char		atom_nameAA[MAXFIELDS][ATOMNAMESIZE];

	} SelectS;


/* ButtonS structure, defines positions */
/* of dummy buttons  in control window. */

typedef struct		/* ButtonS */
	{
	int		left_edge;
	int		right_edge;
	int		top_edge;
	int		bottom_edge;

	} ButtonS;


/* DockingS contains data required for docking. */

typedef struct		/* DockingS */
	{
	MolComplexS	*mol_complex1SP;
	MolComplexS	*mol_complex2SP;
	double		docking_area_width;
	} DockingS;


/* That's all! */


