/* Copyright (C) 2001-2003 Damir Zucic */

/*=============================================================================

				draw_squid.c

Purpose:
	Draw the squid at the position of the first selected atom. Write
	a small hint to the top right corner of the main window.


Input:
	(1) Pointer to AtomS structure.
	(2) Pointer to ConfigS structure.
	(3) Pointer to GUIS structure.
	(4) Pointer to NearestAtomS structure.
	(5) The number of pixels in the main window free area.
	(6) The refreshI index.

Output:
	(1) Squid drawn to the main pixmap.
	(2) Return value.

Return value:
	(1) Positive on success.
	(2) Negative on failure.

=============================================================================*/

#include <stdio.h>

#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include "defines.h"
#include "typedefs.h"
#include "squid.xpm"

/*======function prototypes:=================================================*/

int		ParseColor_ (RGBS *, unsigned long *, GUIS *, char *, char *);

/*======draw squid:==========================================================*/

int DrawSquid_ (AtomS *selected_atomSP,
		ConfigS *configSP, GUIS *guiSP,
		NearestAtomS *nearest_atomSP, size_t pixelsN,
		unsigned int refreshI)
{
static int		called_beforeF;
int			imageI, imagesN;
int			left_edge[2], right_edge[2];
int			colorsN = 10, colorI;
int			stringI;
char			*color_stringP;
RGBS			rgbS;
static unsigned long	colorIDA[10];
int			screen_x0, screen_y0, screen_x, screen_y;
double			z;
int			width = 211, height = 43, columnI, rowI;
int			current_char;
size_t			pixelI;
NearestAtomS		*curr_pixelSP;
char			stringA[SHORTSTRINGSIZE];
int			text_width;
unsigned int		hint_width, hint_height;

/*------draw the squid:------------------------------------------------------*/

/* Number of images: */
if (configSP->stereoF) imagesN = 2;
else imagesN = 1;

/* Left and right image edge (in stereo mode there are two images): */
left_edge[0]  = configSP->image_screen_x0[0];
right_edge[0] = configSP->image_screen_x1[0];
left_edge[1]  = configSP->image_screen_x0[1];
right_edge[1] = configSP->image_screen_x1[1];

/* Prepare colors if this function is called for the first time: */
if (!called_beforeF)
	{
	for (colorI = 0; colorI < colorsN; colorI++)
		{
		/* The third  string  specifies  the first */
		/* color. The index of this string is two: */
		stringI = colorI + 2;

		/* The color string follows the substring "c ": */
		color_stringP = strstr (squid_xpm[stringI], "c ") + 2;

		/* Parse color: */
		ParseColor_ (&rgbS, &colorIDA[colorI],
			     guiSP, color_stringP, "white");
		}
	}

/* Set the flag which signals that this function was used at least once: */
called_beforeF = 1;

/* Copy colors to GC's: */
for (colorI = 0; colorI < colorsN; colorI++)
	{
	XSetForeground (guiSP->displaySP, guiSP->theGCA[colorI],
			colorIDA[colorI]);
	}

/* Draw one (mono) or two pixels (stereo): */
for (imageI = 0; imageI < imagesN; imageI++)
	{
	/* Screen coordinates of the selected atom: */
	screen_x0 = selected_atomSP->raw_atomS.screen_x[imageI];
	screen_y0 = selected_atomSP->raw_atomS.screen_y;

	/* z coordinate: */
	z = selected_atomSP->raw_atomS.z[imageI];

	/* Now scan the pixmap. */

	/* Vertical scan (top to bottom): */
	for (rowI = 0; rowI < height; rowI++)
		{
		/* Prepare the string index: */
		stringI = rowI + 12;

		/* Horizontal scan (left to right): */
		for (columnI = 0; columnI < width; columnI++)
			{
			/* Prepare the current character: */
			current_char = squid_xpm[stringI][columnI];

			/* Check is it this pixel transparent: */
			if (current_char == ' ') continue;

			/* Prepare the color index: */
			switch (current_char)
				{
				case '.':
					colorI = 0;
					break;
				case '+':
					colorI = 1;
					break;
				case '@':
					colorI = 2;
					break;
				case '#':
					colorI = 3;
					break;
				case '$':
					colorI = 4;
					break;
				case '%':
					colorI = 5;
					break;
				case '&':
					colorI = 6;
					break;
				case '*':
					colorI = 7;
					break;
				case '=':
					colorI = 8;
					break;
				case '-':
					colorI = 9;
					break;
				default:
					;
				}

			/* Prepare and check the screen coordinates: */
			screen_x = screen_x0 + columnI -  1;
			screen_y = screen_y0 + rowI    - 28;
			if (screen_x <  left_edge[imageI])  break;
			if (screen_x >= right_edge[imageI]) break;

			/* Prepare index  to the array */
			/* of NearestAtomS structures: */
			pixelI = guiSP->main_win_free_area_width * screen_y +
				 screen_x;

			/* Check the pixel index: */
			if (pixelI >= pixelsN) break;

			/* Pointer to  NearestAtomS struct. */
			/* assigned to current coordinates: */
			curr_pixelSP = nearest_atomSP + pixelI;

			/* Check was  this pixel used  already in */
			/* this drawing step;  if it was, compare */
			/* the z value of the current atom with z */
			/* value previously stored to this pixel: */
			if (refreshI == curr_pixelSP->last_refreshI)
				{
				if (z >= curr_pixelSP->z) continue;
				}

			/* Draw this pixel: */
			XDrawPoint (guiSP->displaySP,
				    guiSP->main_hidden_pixmapID,
				    guiSP->theGCA[colorI],
				    screen_x, screen_y);
			}
		}
	}   /* imageI loop */

/*------draw the hint:-------------------------------------------------------*/

/* Prepare the text: */
strcpy (stringA, "Use 0, 5, /, *, + and -  on numeric keypad");

/* Draw the red filled rectangle and the yellow text inside this rectangle: */
text_width = XTextWidth (guiSP->main_winS.fontSP, stringA, strlen (stringA));
screen_x0 = guiSP->main_win_free_area_width - text_width -
	    guiSP->main_winS.half_font_height -
	    2 * guiSP->main_winS.border_width;
screen_y0 = guiSP->main_winS.font_height + 9;
hint_width  = text_width + guiSP->main_winS.half_font_height;
hint_height = guiSP->main_winS.font_height + 4;
XSetForeground (guiSP->displaySP, guiSP->theGCA[0], guiSP->red_colorID);
XFillRectangle (guiSP->displaySP,
		guiSP->main_hidden_pixmapID,
		guiSP->theGCA[0],
		screen_x0, screen_y0, hint_width, hint_height);
XSetForeground (guiSP->displaySP, guiSP->theGCA[0], guiSP->yellow_colorID);
screen_x0 += guiSP->main_winS.quarter_font_height;
screen_y0 += guiSP->main_winS.font_height - 2;
XDrawString (guiSP->displaySP, guiSP->main_hidden_pixmapID, guiSP->theGCA[0],
             screen_x0, screen_y0, stringA, strlen (stringA));

/*---------------------------------------------------------------------------*/

/* Return positive value on success: */
return 1;
}

/*===========================================================================*/


